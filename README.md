# Importante

O projeto está com a pasta Pods/ para facilitar o processo final de subir na loja.
O app possui um problema: ele foi feito com a lib EVReflection na camada de Network/Models. Essa biblioteca está depreciada e utiliza uma versão antiga fixa do RxCocoa, que por sua vez utiliza o UIWebView, o qual foi depreciado pela Apple. Ou seja, quando a gente dá um pod install, indiretamente o projeto está utilizando o UIWebView e a Apple recusa a subida do app na loja.

Para resolver o problema de forma correta o app deveria ser refeito praticamente por inteiro, pois ele utiliza esses models por completo no app. Como isso não é uma possibilidade para o cliente, a solução foi remover na mão tudo o que é UIWebView do projeto, comentando algumas linhas e arquivos do Pods/.

Para não ter que fazer isso toda hora, o projeto está com a pasta Pods/ no git. Caso um dia haja algum problema com UIWebView, muito provavelmente algo se perdeu e esse processo de "apagar" a UIWebView do projeto deverá ser refeito.
