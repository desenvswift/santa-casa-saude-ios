//
//  MainButton.swift
//  Base
//
//  Created by Victor Catão on 29/04/19.
//  Copyright © 2019 Catao. All rights reserved.
//

import UIKit

class MainButton: CataoUIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = Colors.MAIN_COLOR
        self.borderWidth = 3.0
        self.borderColor = .white
        self.cornerRadius = self.frame.height/2
        self.setTitleColor(.white, for: .normal)
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
    }
    
}
