//
//  CataoModel.swift
//  Catao
//
//  Created by Catao on 02/05/2018.
//  Copyright © 2018 Catao. All rights reserved.
//

import Foundation
import EVReflection

class CataoModel: EVNetworkingObject {
    
    var token: String? = UserDefaults.standard.string(forKey: KEY_USER_TOKEN)
    
    
    var codAcao: NSNumber?
    var statusVersao: NSNumber?
    var msgExterna: String?
    var msgInterna: String?
    var msgExternaExibir: NSNumber?
    
    override func skipPropertyValue(_ value: Any, key: String) -> Bool {
        if let value = value as? String, value.count == 0 || value == "null" {
            return true
        } else if let value = value as? NSArray, value.count == 0 {
            return true
        } else if value is NSNull {
            return true
        }
        return false
    }
    
    func getStatus() -> Int {
        return self.codAcao?.intValue ?? -1
    }
    
    func getMsgExterna() -> String? {
        return self.msgExternaExibir?.boolValue == true ? self.msgExterna : nil
    }
    
}
