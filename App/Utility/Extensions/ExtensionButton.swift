//
//  ExntensionButton.swift
//  Catao
//
//  Created by Victor Catão on 05/11/18.
//  Copyright © 2018 Catao. All rights reserved.
//

import UIKit

extension UIButton: XIBLocalizable {
    @IBInspectable var xibLocKey: String? {
        get { return nil }
        set(key) {
            setTitle(key?.localized, for: .normal)
        }
    }
}

extension UIButton{
    func putBadge(count: Int){
        // your badge view (use UIView instead of UIButton)
        let badgeSize: CGFloat = 13
        let badgeView = UIView()
        badgeView.backgroundColor = UIColor(red: 235/255, green: 68/255, blue: 90/255, alpha: 1)
        badgeView.layer.cornerRadius = badgeSize / 2
        self.addSubview(badgeView)

        // set constraints
        badgeView.translatesAutoresizingMaskIntoConstraints = false
        let constraints: [NSLayoutConstraint] = [
            badgeView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 1),
            badgeView.topAnchor.constraint(equalTo: self.topAnchor, constant: -5),
            badgeView.widthAnchor.constraint(equalToConstant: badgeSize),
            badgeView.heightAnchor.constraint(equalToConstant: badgeSize)
        ]
        NSLayoutConstraint.activate(constraints)
        
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 11)
        label.text = String(count)
        label.backgroundColor = .clear
        label.textAlignment = .center
        
        badgeView.addSubview(label)

        // set constraints
        label.translatesAutoresizingMaskIntoConstraints = false
        let constraints1: [NSLayoutConstraint] = [
            label.leadingAnchor.constraint(equalTo: badgeView.leadingAnchor, constant: 0),
            label.topAnchor.constraint(equalTo: badgeView.topAnchor, constant: 0),
            label.bottomAnchor.constraint(equalTo: badgeView.bottomAnchor, constant: 0),
            label.trailingAnchor.constraint(equalTo: badgeView.trailingAnchor, constant: 0),
        ]
        NSLayoutConstraint.activate(constraints1)
        
        
    }
}
