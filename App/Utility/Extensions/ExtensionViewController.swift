//
//  ViewControllerCatao.swift
//  Catao
//
//  Created by Catao on 18/12/2017.
//  Copyright © 2017 Catao. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MobileCoreServices
import MapKit

private var loaderViewAssociationKey: NVActivityIndicatorView?

enum VersionError: Error {
    case invalidResponse, invalidBundleInfo
}

extension UIViewController {
    
    var loaderView: NVActivityIndicatorView! {
        get { return objc_getAssociatedObject(self, &loaderViewAssociationKey) as? NVActivityIndicatorView }
        set(newValue) { objc_setAssociatedObject(self, &loaderViewAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN) }
    }
    
    
    // MARK: - AutoScrollWhenKeyboardShowsUp
    /**
     Sobe a scrollview quando o teclado aparece. Normalmente utilizado em formulários
     O TextField deve estar dentro de uma scrollView
     Não se esqueça de dar override no setScrollViewContentInset e dar removeObservers() no viewDidDisappear
     */
    
    func setupAutoScrollWhenKeyboardShowsUp() {
        closeKeyboardOnTouch()
        addObservers()
    }
    
    
    func closeKeyboardOnTouch(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapView(gesture:)))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func didTapView(gesture: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    func addObservers(){
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillShow, object: nil, queue: nil) { (notification) in
            self.keyboardWillShow(notification: notification)
        }
        
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillHide, object: nil, queue: nil) { (notification) in
            self.keyboardWillHide(notification: notification)
        }
    }
    
    func keyboardWillShow(notification: Notification) {
//        guard let userInfo = notification.userInfo,
//            let frame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
//                return
//        }
//        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: frame.height, right: 0)
//        setScrollViewContentInset(contentInset)
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: Notification) {
//        setScrollViewContentInset(UIEdgeInsets.zero)
        if view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }
    }
    
    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    // OVERRIDE IT!
    @objc func setScrollViewContentInset(_ inset: UIEdgeInsets) {
        
    }
    
    // MARK: - ShowModal
    func showModal(viewController controller: UIViewController) {
        controller.modalPresentationStyle = .overFullScreen
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    
    func showModalGeneric(titulo: String?, desc: String?, exibirConfirmar: Bool, exibirCancelar: Bool, acaoConfirmar: @escaping ()->(), acaoCancelar: @escaping ()->()){
        let modal = GenericModalVC()
        modal.titulo = titulo
        modal.desc = desc
        modal.actionConfirm = acaoConfirmar
        modal.actionCancel = acaoCancelar
        modal.showConfirmar = exibirConfirmar
        modal.showCancelar = exibirCancelar
        self.showModal(viewController: modal)
    }
    
    func showModalGeneric(titulo: String?, desc: String?,  acaoConfirmar: @escaping ()->(), acaoCancelar: @escaping ()->()){
        let modal = GenericModalVC()
        modal.titulo = titulo
        modal.desc = desc
        modal.actionConfirm = acaoConfirmar
        modal.actionCancel = acaoCancelar
        self.showModal(viewController: modal)
    }
    
    func showModalNotification(notficacao: PainelBoleto?){
        if notficacao == nil { return }
        
        let modal = NotificationModal()
        modal.notficacao = notficacao
        self.showModal(viewController: modal)
    }
    
    // MARK: - Alert
    
    func showNotImplementedAlert(){
        showAlert(title: "Alerta", message: "Função não implementada!", okBlock: nil, cancelBlock: nil)
    }
    
    func showSuccessAlert(message: String, okBlock:(() -> Void)?, cancelBlock: (() -> Void)?){
        showAlert(title: "Sucesso", message: message, okBlock: okBlock, cancelBlock: cancelBlock)
    }
    
    func showSuccessAlert(message: String){
        showAlert(title: "Sucesso", message: message, okBlock: nil, cancelBlock: nil)
    }
    
    func showErrorAlert(message: String){
        showAlert(title: "Oops!", message: message, okBlock: nil, cancelBlock: nil)
    }
    
    func showErrorAlert(message: String, okBlock:(() -> Void)?, cancelBlock: (() -> Void)?){
        showAlert(title: "Erro", message: message, okBlock: okBlock, cancelBlock: cancelBlock)
    }
    
    func verifyUserLoggedAlert(loggedBlock: (()->Void)?) {
        if isUserLogged() {
            loggedBlock?()
        } else {
            self.showAlert(title: "Deseja fazer login agora?", message: "Para acesso a esta funcionalidade, você deve ser cliente e efetuar seu login.", okBlock: {
                let loginVC = LoginViewController()
                let nav = UINavigationController(rootViewController: loginVC)
                nav.navigationBar.tintColor = UIColor.white
                UIApplication.shared.windows.first?.rootViewController = nav
            }) {
                // do nothing
            }
        }
    }
    
    func showAlert(title: String, message: String, okBlock:(() -> Void)?, cancelBlock: (() -> Void)?){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let titleOkAction = "Ok"
        let titleCancelAction = "Cancelar"
        
//        if (cancelBlock != nil) {
//            titleOkAction = "Sim"
//            titleCancelAction = "Não"
//        }
        
        
        let ok = UIAlertAction(title: titleOkAction, style: .default) { (action) in
            if let okBl = okBlock {
                okBl()
            }
            alert.dismiss(animated: true, completion: nil);
        }
        
        alert.addAction(ok)
        
        if let cancelBl = cancelBlock {
            let cancel = UIAlertAction(title: titleCancelAction, style: .cancel) { (action) in
                cancelBl()
            }
            alert.addAction(cancel)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - Loader
    /**
     Github: https://github.com/ninjaprox/NVActivityIndicatorView
     Mostra o loader na tela. Cor default = MAIN_COLOR
     Escolha um estilo (listados no github)
     */
    func showLoader() {
        
        if(loaderView == nil) {
            loaderView = NVActivityIndicatorView(frame: CGRect(x: (UIScreen.main.bounds.size.width/2) - 15, y: UIScreen.main.bounds.size.height/2, width: 35, height: 35) , type: NVActivityIndicatorType.circleStrokeSpin, color: Colors.MAIN_COLOR, padding: 0)
        }
        
        
        
        if(loaderView.isAnimating == false){
            loaderView.startAnimating()
        }
        
        let v = UIView(frame: UIScreen.main.bounds)
        v.backgroundColor = .black
        v.alpha = 0.7
        v.tag = 992
        
        if self.navigationController ==  nil {
            self.lockView(self.view)
            self.view.addSubview(v)
            self.view.addSubview(loaderView)
        }
        else {
            self.lockView(self.navigationController!.view)
            self.navigationController?.view.addSubview(v)
            self.navigationController?.view.addSubview(loaderView)
        }
        
    }
    
    func hideLoader () {
        if(loaderView != nil){
            self.view.viewWithTag(992)?.removeFromSuperview()
            self.navigationController?.view.viewWithTag(992)?.removeFromSuperview()
            if self.navigationController ==  nil {
                self.unlockView(self.view)
            } else {
                self.unlockView(self.navigationController!.view)
            }
            loaderView.stopAnimating()
            loaderView.removeFromSuperview()
        }
    }
    
    //show loader over modal
    func showModalLoader() {
        
        if(loaderView == nil) {
            loaderView = NVActivityIndicatorView(frame: CGRect(x: (UIScreen.main.bounds.size.width/2) - 15, y: UIScreen.main.bounds.size.height/2, width: 35, height: 35) , type: NVActivityIndicatorType.ballTrianglePath, color: Colors.MAIN_COLOR, padding: 0)
        }
        
        loaderView.startAnimating()
        self.view.addSubview(loaderView)
    }
    
    // MARK: - navigation
    /**
     Retira o texto do back no navigationItem. Fica apenas a seta "<"
     */
    func setupBack() {
        let backItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backItem
    }
    
    
    // MARK: - Views
    /**
     Desativa a interação do usuário naquela view
     */
    func lockView(_ view: UIView){
        view.isUserInteractionEnabled = false
    }
    
    func unlockView(_ view: UIView) {
        view.isUserInteractionEnabled = true
    }
    
    // MARK: - RegisterNib
    /**
     Registra a nib na tableView
     */
    func registerNibInTableView(_ tableView: UITableView, nibName: String){
        tableView.register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: nibName)
    }
    
    /**
     Registra a nib na collectionView
     */
    func registerNibInCollectionView(_ collectionView: UICollectionView, nibName: String){
        collectionView.register(UINib(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: nibName)
    }
    
    
    
    func route(locationSelected: CLLocationCoordinate2D) {
        let alert = UIAlertController(title: "Alerta", message: "Onde deseja abrir o endereço?", preferredStyle: .alert)
        var list = [String]()
        if (UIApplication.shared.canOpenURL(NSURL(string:"http://maps.apple.com")! as URL)){
            list.append("Apple Maps")
        }
        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
            list.append("Google Maps")
        }
        if (UIApplication.shared.canOpenURL(NSURL(string:"waze://")! as URL)) {
            list.append("Waze")
        }
        //Retirar comentario para usar o Uber. Instalar o pod
        
        //        if (UIApplication.shared.canOpenURL(NSURL(string:"uber://")! as URL)){
        
        //        list.append("Uber")
        //        }
        list.append("Cancelar")
        for i in list {
            alert.addAction(UIAlertAction(title: i, style: .default, handler: { action in
                self.openDeepLink(action: action, locationSelected: locationSelected)
            }))
        }
        // alerta para quando nao tiver opcoes para abrir // TESTAR
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    func openDeepLink(action: UIAlertAction, locationSelected: CLLocationCoordinate2D) {
        //Use action.title
        if(action.title == "Apple Maps") {
            let string = "http://maps.apple.com/?q=" + String(locationSelected.latitude) + "," + String(locationSelected.longitude)
            UIApplication.shared.openURL(NSURL(string:string)! as URL)
        } else if (action.title == "Google Maps") {
            let string = "comgooglemaps://?q=" + String(locationSelected.latitude) + "," + String(locationSelected.longitude)
            UIApplication.shared.open(URL(string:string)!)
        } else if (action.title == "Waze") {
            let string = "waze://?ll=" + String(locationSelected.latitude) + "," + String(locationSelected.longitude) + "&navigate=yes"
            UIApplication.shared.open(URL(string:string)!)
            //        } else if (action.title == "Uber"){
            //            let dropoffLocation = CLLocation(latitude: locationSelected.latitude, longitude: locationSelected.longitude)
            ////            let builder = RideParametersBuilder()
            ////            builder.dropoffLocation = dropoffLocation
            ////            builder.dropoffNickname = "Revenda/Filial"
            ////            let rideParameters = builder.build()
            //            let deeplink = RequestDeeplink(rideParameters: rideParameters, fallbackType: .appStore)
            //            deeplink.execute()
        }
    }
    
    
}

extension UIViewController{
    func setNavBar(close: Bool = false, transparent: Bool = false){
        UINavigationBar.appearance().backgroundColor = UIColor(named: "LightGray")
        UINavigationBar.appearance().barTintColor = UIColor(named: "LightGray")
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor(named: "LightGray")
            if transparent{
                appearance.backgroundColor = .clear
            }
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.navigationBar.standardAppearance = appearance
            navigationController?.navigationBar.scrollEdgeAppearance = navigationController?.navigationBar.standardAppearance
        } else {
            self.navigationController?.navigationBar.barTintColor = UIColor(named: "LightGray")
            if transparent{
                self.navigationController?.navigationBar.barTintColor = .clear
            }
        }
        
        if close{
            addCloseButton()
        }else{
            addBackButton()
        }
        addNavBarImage()
        removeBorder()
    }
    
    func removeBorder(){
        if #available(iOS 13.0, *) {
            let navigationBar = navigationController?.navigationBar
            let navigationBarAppearance = UINavigationBarAppearance()
            navigationBarAppearance.shadowColor = .clear
            navigationBar?.scrollEdgeAppearance = navigationBarAppearance
        }
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
    }
    
    func addCloseButton(){
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        backButton.setImage(UIImage(named: "Back")?.resize(maxWidthHeight: 20), for: .normal)
        backButton.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func closeAction () {
        self.dismiss(animated: true)
    }
    
    func addBackButton(){
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        backButton.setImage(UIImage(named: "Back")?.resize(maxWidthHeight: 20), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction () {
        self.navigationController?.popViewController(animated: true)
    }
    
    func addNavBarImage() {
        let imageView = UIImageView(image: UIImage(named: "Logo_dark"))
        imageView.frame = CGRect(x: 0, y: 0, width: 170, height: 30)
        imageView.contentMode = .scaleAspectFit

        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 170, height: 30))

        titleView.addSubview(imageView)
        titleView.backgroundColor = .clear
        self.navigationItem.titleView = titleView
    }
    
    func setNavBar(withColor color: UIColor){
        
        UINavigationBar.appearance().backgroundColor = color
        UINavigationBar.appearance().barTintColor = color
//        if #available(iOS 13.0, *) {
//            let appearance = UINavigationBarAppearance()
//            appearance.configureWithOpaqueBackground()
//            appearance.backgroundColor = color
//            appearance.backgroundColor?.withAlphaComponent(1)
//            self.navigationController?.navigationBar.isTranslucent = false
//            self.navigationController?.navigationBar.barTintColor = color
//            self.navigationController?.navigationBar.standardAppearance = appearance
//            navigationController?.navigationBar.scrollEdgeAppearance = navigationController?.navigationBar.standardAppearance
//        } else {
//            self.navigationController?.navigationBar.barTintColor = color
//        }
        
        addBackButton()
        addNavBarImage()
        removeBorder()
    }
}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
