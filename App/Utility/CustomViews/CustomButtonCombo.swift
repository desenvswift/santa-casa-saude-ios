//
//  CustomEditText.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 09/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CustomButtomCombo : UIView, UITextFieldDelegate {
    
    var errorMessage = "Campo inválido"
    lazy var emptyMessage = "Campo vazio"
    
    var editText: UITextField = UITextField() {
        didSet {
            //Default editText
            self.editText.textColor = Colors.DarkGray
            self.editText.font = UIFont.systemFont(ofSize: 16)
        }
    }
    var stack: UIStackView = UIStackView()
    var viewBorda: UIView = UIView()
    var lbl: UILabel = UILabel()
    var lblErro: UILabel = UILabel()
    var icon: UIImageView = UIImageView(image: UIImage(named: "Down") ?? UIImage())
    var botao: UIButton = UIButton()
    var altura: CGFloat = 63
    // Constants
    
    let borderWidth : CGFloat = 1
    let fontSize : CGFloat = 12
    let cornerRadius : CGFloat = 5
    
    
    var action: (() -> Void)?
    var actionFuncao: ()?
    
    @IBInspectable
    var titulo : String = "" {
        didSet {
            self.lbl.text = titulo
            self.lbl.font = UIFont.systemFont(ofSize: 16)
            self.lbl.textColor = Colors.DarkGray
        }
    }
    
    @IBInspectable
    var placeholder : String = "" {
        didSet {
            self.editText.placeholder = placeholder
        }
    }
    
    @IBInspectable
    var texto : String = "" {
        didSet {
            self.editText.text = texto
            self.editText.font = UIFont.systemFont(ofSize: 16)
            self.editText.textColor = Colors.DarkGray
        }
    }
    
    @IBInspectable
    var arredondado : Bool = true {
        didSet {
            refreshCorner()
        }
    }
    
    @IBInspectable
    var senha : Bool = false {
        didSet {
            if senha{
                self.editText.isSecureTextEntry = true
                
                let button = UIButton(type: .custom)
                let image = getImageEye()
                
                button.setImage(image, for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 5, right: 10)
                button.frame = CGRect(x: self.frame.size.width , y: 5, width: 30, height: 30)
                button.addTarget(self, action: #selector(verCampo), for: .touchUpInside)
                self.editText.rightView = button
                self.editText.rightViewMode = .always
            }
        }
    }
    
    func getImageEye(name: String = "Show") -> UIImage? {
        let image = UIImage(named: name)
        return image?.resize(maxWidthHeight: 25)
    }
    
    @objc func verCampo(_ sender: UIButton) {
        
        self.editText.isSecureTextEntry = !self.editText.isSecureTextEntry
        
        if self.editText.isSecureTextEntry {
            let image = getImageEye(name: "Show")
            sender.setImage(image, for: .normal)
        } else {
            let image = getImageEye(name: "Hide")
            sender.setImage(image, for: .normal)
        }
    }
    
    @IBInspectable
    var autocapitalizationType : UITextAutocapitalizationType = .words {
        didSet {
            if senha{
                self.editText.autocapitalizationType = autocapitalizationType
            }
        }
    }
    
    @IBInspectable
    var tipoTeclado : UIKeyboardType = .decimalPad {
        didSet {
            self.editText.keyboardType = tipoTeclado
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        self.editText.becomeFirstResponder()
    }
    
    private func setup() {
        setLblColor()
        setBorderColor()
        setBackgroundColor()
        refreshCorner()
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
        self.addGestureRecognizer(gesture)

        
        self.lblErro.numberOfLines = 0
        self.lblErro.font = UIFont.systemFont(ofSize: 12)
        self.lbl.numberOfLines = 0
        self.editText.delegate = self
        
        
        stack.alignment = .fill
        stack.distribution = .equalSpacing
        stack.axis = .vertical
        stack.spacing = 12
        
        viewBorda.addSubview(editText)
        viewBorda.addSubview(icon)
        
//        self.lbl.translatesAutoresizingMaskIntoConstraints = false
//        self.lbl.leadingAnchor.constraint(equalTo: self.viewBorda.leadingAnchor, constant: 8).isActive = true
//        self.lbl.trailingAnchor.constraint(equalTo: self.viewBorda.trailingAnchor, constant: -5).isActive = true
//        self.lbl.topAnchor.constraint(equalTo: self.viewBorda.topAnchor, constant: 8).isActive = true
        
        self.editText.translatesAutoresizingMaskIntoConstraints = false
        self.editText.leadingAnchor.constraint(equalTo: self.viewBorda.leadingAnchor, constant: 15).isActive = true
        self.editText.trailingAnchor.constraint(equalTo: self.viewBorda.trailingAnchor, constant: -15).isActive = true
        self.editText.topAnchor.constraint(equalTo: self.viewBorda.topAnchor, constant: 15).isActive = true
        self.editText.bottomAnchor.constraint(equalTo: self.viewBorda.bottomAnchor, constant: -15).isActive = true
        
        self.icon.translatesAutoresizingMaskIntoConstraints = false
        self.icon.trailingAnchor.constraint(equalTo: self.viewBorda.trailingAnchor, constant: -15).isActive = true
        self.icon.topAnchor.constraint(equalTo: self.viewBorda.topAnchor, constant: 15).isActive = true
        self.icon.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.icon.widthAnchor.constraint(equalToConstant: 25).isActive = true
        
        stack.addArrangedSubview(lbl)
        stack.addArrangedSubview(viewBorda)
        stack.addArrangedSubview(lblErro)
        
        addSubview(stack)
        
        
        self.stack.translatesAutoresizingMaskIntoConstraints = false
        self.stack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        self.stack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        self.stack.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        self.stack.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5).isActive = true
        
        botao.setTitle(" ", for: .normal)
        botao.backgroundColor = .clear
        botao.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        addSubview(botao)
        self.botao.translatesAutoresizingMaskIntoConstraints = false
        self.botao.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        self.botao.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        self.botao.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        self.botao.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5).isActive = true
        

        self.lblErro.isHidden = true
    }
    
    @objc func buttonAction(sender: UIButton!) {
        if let acao = self.action{
            acao()
            return
        }
        if let acao = self.actionFuncao{
            acao
            return
        }
       }
    
    func setErro(erro: String){
        lblErro.isHidden = erro.isEmpty
        lblErro.text = erro
        
        self.lblErro.textColor = Colors.Error
        self.viewBorda.layer.borderColor = Colors.Error?.cgColor
        self.viewBorda.layer.borderWidth = 1
        
    }
    
    func setOK(){
        lblErro.isHidden = true
        self.viewBorda.layer.borderWidth = 0
        setLblColor()
        setBorderColor()
        refreshCorner()
        
        
    }
    
    // Set up view
    
    func refreshCorner() {
        if (arredondado) {
            self.viewBorda.layer.cornerRadius = cornerRadius
        } else {
            self.viewBorda.layer.cornerRadius = 0
        }
    }
    
    func setLblColor() {
        self.lbl.textColor = Colors.DarkGray ?? .black
        self.editText.textColor = Colors.DarkGray
        self.editText.font = UIFont.systemFont(ofSize: 16)
    }
    
    func setBorderColor() {
        self.viewBorda.layer.borderColor = UIColor.clear.cgColor
    }
    
    func setBackgroundColor() {
        self.backgroundColor = .clear
        self.stack.backgroundColor = .clear
        self.viewBorda.backgroundColor = Colors.White
            
    }
    
}

