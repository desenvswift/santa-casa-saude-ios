//
//  CustomButton.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 09/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CustomButton : UIButton {
    
    // Constants
    let cornerRadius : CGFloat = 8
    let inset : CGFloat = 16
    
    // Interface Builder Fields
    
    @IBInspectable
    var texto : String = "" {
        didSet {
            setTitle(texto.uppercased(), for: .normal)
            titleLabel?.font = UIFont.boldSystemFont(ofSize:fontSize)
        }
    }
    
    @IBInspectable
    lazy var preenchimento : Bool = true {
    
        didSet {
            refreshBackground()
        }
    }
    
    @IBInspectable
    var arredondado : Bool = true {
        didSet {
            refreshCorner()
        }
    }
    
    @IBInspectable
    var fontSize : CGFloat = 16 {
        didSet {
            titleLabel?.font = UIFont.boldSystemFont(ofSize:fontSize)
        }
    }
    
    @IBInspectable
    var cor : UIColor = Colors.Primary ?? .blue{
        didSet {
            refreshBackground()
        }
    }
    
    @IBInspectable
    var tamanhoBorda : Int = 2 {
        didSet {
            refreshBackground()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        refreshBackground()
    }
    
    private func setup() {
        refreshBackground()
        refreshCorner()
        titleLabel?.textAlignment = .center
        contentEdgeInsets = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
        titleLabel?.font = UIFont.boldSystemFont(ofSize:fontSize)
//        titleLabel?.adjustsFontSizeToFitWidth = true
    }
    
    public func ativar(){
        self.isEnabled = true
        cor = Colors.Primary ?? .blue
        refreshBackground()
    }
    
    public func desativar(){
        self.isEnabled = true
        cor = Colors.ButtonDisable ?? .blue
        refreshBackground()
    }
    
    // Set up view
    
    func refreshCorner() {
        if (arredondado) {
            layer.cornerRadius = cornerRadius
        } else {
            layer.cornerRadius = 0
        }
    }
    
    func refreshBackground() {
        
        let mainColor = cor
        
        if (preenchimento) {
            layer.backgroundColor = mainColor.cgColor
            layer.borderWidth = 0
            titleLabel?.tintColor = Colors.PrimaryButton
            titleLabel?.textColor = Colors.PrimaryButton
            self.setTitleColor(Colors.PrimaryButton, for: .normal)
            self.titleLabel?.tintColor = Colors.PrimaryButton
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
//            }
        } else {
            layer.backgroundColor = UIColor.clear.cgColor
            layer.borderColor = mainColor.cgColor
            titleLabel?.tintColor = mainColor
            titleLabel?.textColor = mainColor
            setTitleColor(mainColor, for: .normal)
            layer.borderWidth = CGFloat(tamanhoBorda)
            self.setTitleColor(mainColor, for: .normal)
            self.titleLabel?.tintColor = mainColor
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//            }
        }
    }
    
}
