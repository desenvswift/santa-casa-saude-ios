//
//  ViewArredondado.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 15/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import Foundation
import UIKit

class ViewArredondado: UIView{
    
    @IBInspectable
    var radius : CGFloat = 8 {
        didSet {
            self.layer.cornerRadius = radius
        }
    }
}
