//
//  CustomTitulo.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 09/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import Foundation
import UIKit

class CustomViewTituloGrande : UIView {
    
    var lblTitulo: UILabel = UILabel()
    var lblSubTiulo: UILabel = UILabel()
    // Constants
    
    let borderWidth : CGFloat = 1
    let fontSize : CGFloat = 12
    let cornerRadius : CGFloat = 5
    
    // Interface Builder Fields
    
    @IBInspectable
    var titulo : String = "" {
        didSet {
            self.lblTitulo.text = titulo
            self.lblTitulo.font = UIFont.boldSystemFont(ofSize: 24)
            if (titulo.isEmpty){
                self.lblTitulo.isHidden = true
            }
        }
    }
    
    @IBInspectable
    var corTitulo : UIColor = .black {
        didSet {
            self.lblTitulo.textColor = corTitulo
        }
    }
    
    @IBInspectable
    var subTitulo : String = "" {
        didSet {
            self.lblSubTiulo.text = subTitulo
            self.lblSubTiulo.font = UIFont.systemFont(ofSize: 16)
            if (subTitulo.isEmpty){
                self.lblSubTiulo.isHidden = true
            }
        }
    }
    
    @IBInspectable
    var corSubTitulo : UIColor = .black {
        didSet {
            self.lblSubTiulo.textColor = corSubTitulo
        }
    }
    
    @IBInspectable
    var centralizado : Bool = false {
        didSet {
            if centralizado{
                lblTitulo.textAlignment = .center
                lblSubTiulo.textAlignment = .center
            }
            else{
                lblTitulo.textAlignment = .justified
                lblSubTiulo.textAlignment = .justified
            }
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        //self.lbl.font = UIFont.boldSystemFont(ofSize:fontSize)
        self.lblTitulo.numberOfLines = 0
        self.lblSubTiulo.numberOfLines = 0
        
        
        addSubview(lblTitulo)
        addSubview(lblSubTiulo)
        
        self.lblTitulo.translatesAutoresizingMaskIntoConstraints = false
        self.lblTitulo.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        self.lblTitulo.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        self.lblTitulo.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true

        self.lblSubTiulo.translatesAutoresizingMaskIntoConstraints = false
        self.lblSubTiulo.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        self.lblSubTiulo.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        self.lblSubTiulo.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -15).isActive = true
        self.lblSubTiulo.topAnchor.constraint(equalTo: self.lblTitulo.bottomAnchor, constant: 5).isActive = true
        
        self.backgroundColor = .clear
        
    }
}
