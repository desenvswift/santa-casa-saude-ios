//
//  CustomEditText.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 09/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import Foundation
import UIKit
import TLCustomMask

@IBDesignable
class CustomEditText : UIView, UITextFieldDelegate {
    
    
    private let MIN_LENGTH_PASSWORD = 6
    private let MASK_CPF = "$$$.$$$.$$$-$$"
    private let MASK_CNPJ = "$$.$$$.$$$/$$$$-$$"
    private let MASK_CEP = "$$$$$-$$$"
    private let MASK_CELLPHONE = "($$) $$$$$-$$$$"
    private let MASK_PHONE = "($$) $$$$-$$$$"
    private let MASK_CREDIT_CARD_NUMBER = "$$$$ $$$$ $$$$ $$$$ $$$$ $$$$"
    private let MASK_CREDIT_CARD_DATE = "$$/$$"
    private let MASK_DATE = "$$/$$/$$$$"
    
    lazy var maxLength: Int = 100
    var minAge: Int?
    var maxAge: Int?
    private lazy var customMask = TLCustomMask()
    var validateBlock: (() -> Bool)?
    private var maskString: String? {
        didSet {
            if let m = maskString {
                self.customMask.formattingPattern = m
            }
        }
    }
    
    var errorMessage = "Campo inválido"
    lazy var emptyMessage = "Campo vazio"
    
    
    var textFieldType: TextFieldType? {
        didSet {
            self.didSetType()
        }
    }
    
    
    var editText: UITextField = UITextField() {
        didSet {
            //Default editText
            self.editText.textColor = Colors.DarkGray
            self.editText.font = UIFont.systemFont(ofSize: 16)
            self.editText.delegate = self
        }
    }
    var stack: UIStackView = UIStackView()
    var viewBorda: UIView = UIView()
    var viewBlock: UIView = UIView()
    var lbl: UILabel = UILabel()
    var lblErro: UILabel = UILabel()
    var altura: CGFloat = 63
    // Constants
    
    let borderWidth : CGFloat = 1
    let fontSize : CGFloat = 12
    let cornerRadius : CGFloat = 5
    
    
    
    @IBInspectable
    var titulo : String = "" {
        didSet {
            self.lbl.text = titulo
            self.lbl.font = UIFont.systemFont(ofSize: 16)
            self.lbl.textColor = Colors.DarkGray
        }
    }
    
    @IBInspectable
    var placeholder : String = "" {
        didSet {
            self.editText.placeholder = placeholder
        }
    }
    
    @IBInspectable
    var texto : String = "" {
        didSet {
            self.editText.text = texto
            self.editText.font = UIFont.systemFont(ofSize: 16)
            self.editText.textColor = Colors.DarkGray
        }
    }
    
    @IBInspectable
    var arredondado : Bool = true {
        didSet {
            refreshCorner()
        }
    }
    
    @IBInspectable
    var senha : Bool = false {
        didSet {
            if senha{
                self.editText.isSecureTextEntry = true
                
                let button = UIButton(type: .custom)
                let image = getImageEye()
                
                button.setImage(image, for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 5, right: 10)
                button.frame = CGRect(x: self.frame.size.width , y: 5, width: 30, height: 30)
                button.addTarget(self, action: #selector(verCampo), for: .touchUpInside)
                self.editText.rightView = button
                self.editText.rightViewMode = .always
            }
        }
    }
    
    
    @IBInspectable
    var enable : Bool = true {
        didSet {
            self.viewBlock.isHidden = enable
            self.editText.isEnabled = enable
            if !enable{
                self.editText.textColor = Colors.MediumGray
            }
        }
    }
    
    func getImageEye(name: String = "Show") -> UIImage? {
        let image = UIImage(named: name)
        return image?.resize(maxWidthHeight: 25)
    }
    
    @objc func verCampo(_ sender: UIButton) {
        
        self.editText.isSecureTextEntry = !self.editText.isSecureTextEntry
        
        if self.editText.isSecureTextEntry {
            let image = getImageEye(name: "Show")
            sender.setImage(image, for: .normal)
        } else {
            let image = getImageEye(name: "Hide")
            sender.setImage(image, for: .normal)
        }
    }
    
    @IBInspectable
    var autocapitalizationType : UITextAutocapitalizationType = .words {
        didSet {
            if senha{
                self.editText.autocapitalizationType = autocapitalizationType
            }
        }
    }
    
    @IBInspectable
    var tipoTeclado : UIKeyboardType = .decimalPad {
        didSet {
            self.editText.keyboardType = tipoTeclado
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        self.editText.becomeFirstResponder()
    }
    
    private func setup() {
        setLblColor()
        setBorderColor()
        setBackgroundColor()
        refreshCorner()
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
        self.addGestureRecognizer(gesture)

        
        self.lblErro.numberOfLines = 0
        self.lblErro.font = UIFont.systemFont(ofSize: 12)
        self.lbl.numberOfLines = 0
        self.editText.delegate = self
        
        
        stack.alignment = .fill
        stack.distribution = .equalSpacing
        stack.axis = .vertical
        stack.spacing = 12
        
        viewBorda.addSubview(viewBlock)
        viewBorda.addSubview(editText)
        
//        self.lbl.translatesAutoresizingMaskIntoConstraints = false
//        self.lbl.leadingAnchor.constraint(equalTo: self.viewBorda.leadingAnchor, constant: 8).isActive = true
//        self.lbl.trailingAnchor.constraint(equalTo: self.viewBorda.trailingAnchor, constant: -5).isActive = true
//        self.lbl.topAnchor.constraint(equalTo: self.viewBorda.topAnchor, constant: 8).isActive = true
        
        self.editText.translatesAutoresizingMaskIntoConstraints = false
        self.editText.leadingAnchor.constraint(equalTo: self.viewBorda.leadingAnchor, constant: 15).isActive = true
        self.editText.trailingAnchor.constraint(equalTo: self.viewBorda.trailingAnchor, constant: -15).isActive = true
        self.editText.topAnchor.constraint(equalTo: self.viewBorda.topAnchor, constant: 15).isActive = true
        self.editText.bottomAnchor.constraint(equalTo: self.viewBorda.bottomAnchor, constant: -15).isActive = true
        
        self.viewBlock.translatesAutoresizingMaskIntoConstraints = false
        self.viewBlock.leadingAnchor.constraint(equalTo: self.viewBorda.leadingAnchor, constant: 0).isActive = true
        self.viewBlock.trailingAnchor.constraint(equalTo: self.viewBorda.trailingAnchor, constant: 0).isActive = true
        self.viewBlock.topAnchor.constraint(equalTo: self.viewBorda.topAnchor, constant: 0).isActive = true
        self.viewBlock.bottomAnchor.constraint(equalTo: self.viewBorda.bottomAnchor, constant: 0).isActive = true
        viewBlock.isHidden = true
        viewBlock.backgroundColor = Colors.Gray?.withAlphaComponent(0.7)
        
        stack.addArrangedSubview(lbl)
        stack.addArrangedSubview(viewBorda)
        stack.addArrangedSubview(lblErro)
        
        addSubview(stack)
        
        
        self.stack.translatesAutoresizingMaskIntoConstraints = false
        self.stack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        self.stack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        self.stack.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        self.stack.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5).isActive = true

        self.lblErro.isHidden = true
    }
    
    func setErro(erro: String){
        lblErro.isHidden = erro.isEmpty
        lblErro.text = erro
        
        self.lblErro.textColor = Colors.Error
        self.viewBorda.layer.borderColor = Colors.Error?.cgColor
        self.viewBorda.layer.borderWidth = 1
        
    }
    
    func setOK(){
        lblErro.isHidden = true
        self.viewBorda.layer.borderWidth = 0
        setLblColor()
        setBorderColor()
        refreshCorner()
        
        
    }
    
    // Set up view
    
    func refreshCorner() {
        if (arredondado) {
            self.viewBorda.layer.cornerRadius = cornerRadius
            self.viewBlock.layer.cornerRadius = cornerRadius
        } else {
            self.viewBorda.layer.cornerRadius = 0
            self.viewBlock.layer.cornerRadius = 0
        }
    }
    
    func setLblColor() {
        self.lbl.textColor = Colors.DarkGray ?? .black
        self.editText.textColor = Colors.DarkGray
        self.editText.font = UIFont.systemFont(ofSize: 16)
    }
    
    func setBorderColor() {
        self.viewBorda.layer.borderColor = UIColor.clear.cgColor
    }
    
    func setBackgroundColor() {
        self.backgroundColor = .clear
        self.stack.backgroundColor = .clear
        self.viewBorda.backgroundColor = Colors.White
            
    }
    
}

extension CustomEditText{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing(true)
    }
    
    func configure(textFieldType: TextFieldType, erroMessage: String? = nil){
        if let mensagem = erroMessage{
            self.errorMessage = mensagem
        }
        self.textFieldType = textFieldType
    }
    
    func validate(){
        self.setOK()
        guard let texto = self.editText.text, texto != "" else{
            self.setErro(erro: emptyMessage)
            return
        }
        if !isValid(){
            self.setErro(erro: self.errorMessage)
        }
        
    }
    
    func isValid() -> Bool {
        if let customValidationBlock = validateBlock {
            return customValidationBlock()
        }
        
        guard let type = self.textFieldType else { return true }
        
        switch type {
        case .CPF:
            return self.editText.text?.isValidCPF() == true
            
        case .CNPJ:
            return self.editText.text?.isValidCNPJ() == true
            
        case .NAME:
            return self.editText.text?.isValidName() == true
            
        case .CEP:
            return self.editText.text?.isValidCEP() == true
            
        case .PHONE:
            return self.editText.text?.isValidPhone() == true
            
        case .DATE:
            return self.editText.text?.isValidDate() == true
            
        case .EMAIL:
            return self.editText.text?.isValidEmail() == true
            
        case .BIRTHDAY:
            return self.editText.text?.isValidBirthday(minAge: nil, maxAge: nil) == true
            
        case .BIRTHDAY_AGE_ADULT:
            return self.editText.text?.isValidBirthday(minAge: 18, maxAge: nil) == true
            
        case .BIRTHDAY_MAX_AGE_AND_MINAGE:
            return self.editText.text?.isValidBirthday(minAge: self.minAge, maxAge: self.maxAge) == true
            
        case .BIRTHDAY_MIN_AGE:
            return self.editText.text?.isValidBirthday(minAge: self.minAge, maxAge: nil) == true
            
        case .BIRTHDAY_MAX_AGE:
            return self.editText.text?.isValidBirthday(minAge: nil, maxAge: self.maxAge) == true
            
        case .FUTURE_DATE:
            return self.editText.text?.isValidFutureDate(minAge: nil, maxAge: nil) == true
            
        case .PASSWORD:
            return (self.editText.text != nil) && self.editText.text!.count >= MIN_LENGTH_PASSWORD
            
        case .CREDIT_CARD_DATE:
            return self.editText.text?.isValidCreditCardDate() == true
            
        case .CREDIT_CARD_NUMBER:
            return (self.editText.text != nil) && (self.editText.text!.count >= (16+3)) // 16 numeros + 3 espacos
            
        case .MONEY:
            return true
            
        case .NUMERIC:
            return true
            
        default:
            return true
            
        }
        
    }
    
    
    private func didSetType(){
        guard let type = self.textFieldType else { return }
        
        switch type {
        case .CPF:
            self.maskString = MASK_CPF
            self.editText.keyboardType = .numberPad
            break
            
        case .CNPJ:
            self.maskString = MASK_CNPJ
            self.editText.keyboardType = .numberPad
            break
            
        case .CEP:
            self.maskString = MASK_CEP
            self.editText.keyboardType = .numberPad
            break
            
        case .PHONE:
            self.maskString = MASK_CELLPHONE
            self.editText.keyboardType = .numberPad
            break
            
        case .CREDIT_CARD_NUMBER:
            self.maskString = MASK_CREDIT_CARD_NUMBER
            self.editText.keyboardType = .numberPad
            break
            
        case .CREDIT_CARD_DATE:
            self.maskString = MASK_CREDIT_CARD_DATE
            self.editText.keyboardType = .numberPad
            break
            
        case .DATE:
            fallthrough
        case .BIRTHDAY:
            fallthrough
        case .BIRTHDAY_AGE_ADULT:
            fallthrough
        case .BIRTHDAY_MAX_AGE_AND_MINAGE:
            fallthrough
        case .BIRTHDAY_MIN_AGE:
            fallthrough
        case .BIRTHDAY_MAX_AGE:
            fallthrough
        case .FUTURE_DATE:
            self.maskString = MASK_DATE
            self.editText.keyboardType = .numberPad
            break
            
        case .EMAIL:
            self.editText.keyboardType = .emailAddress
            break
            
        case .MONEY:
            self.editText.keyboardType = .numberPad
            self.editText.addTarget(self, action: #selector(moneyTextFieldDidChange), for: .editingChanged)
            break
        case .NUMERIC:
            self.editText.keyboardType = .numberPad
            break
        case .NAME:
            self.editText.keyboardType = .default
            break
            
        default:
            break
        }
    }
    
    
    @objc private func moneyTextFieldDidChange(_ textField: UITextField) {
        if let amountString = self.editText.text?.currencyInputFormatting() {
            if(amountString == "") {
                self.editText.text = "R$0,00"
            } else {
                self.editText.text = amountString
            }
            self.editText.sendActions(for: .valueChanged)
        }
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if self.textFieldType != nil && self.textFieldType! == .PHONE && textField.text != nil && textField.text!.count == 6 {
            if (textField.text!.charAt(index: 5) == "9") {
                self.maskString = MASK_CELLPHONE
            } else {
                self.maskString = MASK_PHONE
            }
        }
        
        if textField.text != nil && textField.text!.count >= maxLength && string != "" {
            return false
        }
        
        if self.maskString != nil {
            self.editText.text = customMask.formatStringWithRange(range: range, string: string)
            self.editText.sendActions(for: .valueChanged)
        } else {
            return true
        }
        
        return false
        
    }
    
    
    
    // MARK: - Gets
    
    func getTexto() -> String?{
        return self.editText.text
    }
    func getDDD() -> String? {
        if self.textFieldType != .PHONE || self.isValid() == false {
            return nil
        }
        return "\(self.editText.text!.split(separator: " ")[0])".replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "")
    }
    
    func getPhone() -> String? {
        if self.textFieldType != .PHONE || self.isValid() == false {
            return nil
        }
        return self.editText.text!.split(separator: " ")[1].replacingOccurrences(of: "-", with: "")
    }
}
