//
//  UserHeader.swift
//  Base
//
//  Created by Victor Catão on 06/05/19.
//  Copyright © 2019 Catao. All rights reserved.
//

import UIKit

protocol UserHeaderDelegate: class {
    func didTapUserHeader()
}

class UserHeader: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var loggedInfosStackView: UIStackView!
    @IBOutlet weak var registrationNumberLabel: UILabel!
    @IBOutlet weak var planNameLabel: UILabel!
    
    @IBOutlet weak var notLoggedUserStackView: UIStackView!
    @IBOutlet weak var notLoggedUserLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var signinButton: MainButton!
    
    weak var delegate: UserHeaderDelegate?
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("UserHeader", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setup()
    }
    
    func setup() {
        
        self.arrowImageView.isHidden = true
        self.signinButton.isHidden = true
        
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapUserHeader)))
        
        self.notLoggedUserStackView.isHidden = isUserLogged()
        self.loggedInfosStackView.isHidden = !isUserLogged()
        
        
        if isUserLogged() {
            self.registrationNumberLabel.text = UserSingleton.shared.user?.beneficiario?.matBeneficiario
            self.planNameLabel.text = UserSingleton.shared.user?.beneficiario?.descricaoPlano
        }
        
    }
    
    func showHomeItems() {
        if isUserLogged() {
            self.arrowImageView.isHidden = false
        } else {
            self.signinButton.isHidden = false
        }
    }
    
    @objc func didTapUserHeader() {
        self.delegate?.didTapUserHeader()
    }
}
