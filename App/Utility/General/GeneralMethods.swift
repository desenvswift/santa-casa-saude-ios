//
//  GeneralMethods.swift
//  Catao
//
//  Created by Victor Catão on 30/12/18.
//  Copyright © 2018 Catao. All rights reserved.
//

import Foundation

func isUserLogged() -> Bool {
    return UserSingleton.shared.user != nil
}

func getLink(forLocation: WebViewCodes, success: @escaping(String?)->(), error: @escaping(String)->()) {
    SCSManager.getLink(location: forLocation.rawValue, success: { (response) in
        success(response?.trimmingCharacters(in: .whitespaces))
    }) { (msg, status) in
        error(msg)
    }
}
