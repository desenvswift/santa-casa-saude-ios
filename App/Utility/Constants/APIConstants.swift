//
//  APIConstants.swift
//  Catao
//
//  Created by Catao on 18/12/2017.
//  Copyright © 2017 Catao. All rights reserved.
//

import Foundation

let API_URL = "http://200.220.137.230:65004/app_v2/api"
let API_URL_V3 = "http://200.220.137.230:65004/app_v3/api"

// Login
let POST_LOGIN = "/scsapp_login"
let POST_SIGNUP = "/SCSApp_CadastroBeneficiario"
let POST_SIGNUP_SAC = "/SCSApp_CadastroBeneficiarioSAC"

let GET_USER = "/SCSApp_RetornaBeneficiario"
let GET_PLANS = "/SCSApp_RetornaBeneficiariosPlanos"
let UPDATE_USER_PLANS = "/SCSApp_MudarPlanoBeneficiario"
let RECOVER_PASSWORD = "/SCSApp_RecuperarSenha"

let GET_TUTORIAL = "/SCSApp_Tutorial"
let GET_BENEFICIOS = "/SCSApp_MeusBeneficios"

let GET_MEDICAL_GUIDE_FILTER = "/SCSApp_GuiaMedicoFiltro"
let GET_MEDICAL_GUIDE_FILTER_ADVANCED = "/SCSApp_GuiaMedicoFiltroAvancado"
let GET_MEDICAL_GUIDE = "/SCSApp_GuiaMedico"

let POST_FAVORITE = "/SCSApp_ManutencaoFavoritos"
let GET_PLANS_ATENDIDOS = "/SCSApp_GuiaMedicoPlanosAtendidos"

let POST_TALK_WITH_US = "/SCSApp_CadastroFaleConosco"

let POST_TALK_WITH_US_RESOURCES = "/SCSApp_CadastroFaleConoscoInsumo"

let GET_FAVORITES = "/SCSApp_RetornaFavoritos"

let GET_HOME = "/SCSApp_Home"
let VERIFY_ACCESS = "/SCSApp_ValidaAcesso"

let UPDATE_MAIL = "/SCSApp_AtualizarEmail"
let UPDATE_PHONE = "/SCSApp_AtualizarTelefone"
let UPDATE_PASSWORD = "/SCSApp_AtualizarSenha"

let GET_AUTHORIZATION_GUIDES = "/SCSApp_AutorizacaoGuia"
let GET_AUTHORIZATION_HISTORY = "/SCSApp_HistoricoGuia"
let GET_AUTHORIZATION_GUIDE_INSUMO = "/SCSApp_CadastroAutorizacaoGuiaInsumo"
let CREATE_AUTHORIZATION_GUIDE = "/SCSApp_CadastroAutorizacaoGuia"
let GET_AUTHORIZATION_GUIDE_DETAIL = "/SCSApp_AutorizacaoGuiaDetalhes"

let GET_FINANCEIRO = "/SCSApp_Financeiro"
let GET_FINANCEIRO_INSUMO = "/SCSApp_FinanceiroInsumos"

let CLICK_HOME = "/SCSApp_HomeClick"

let GET_LINKS = "/SCSApp_Links"

let GET_CARTEIRINHAS = "/SCSApp_Carteirinha"

let GET_CONTACT = "/SCSApp_Contatos"
let DELETE_USER = "/SCSApp_excluirCadastroApp"
let WEB_LINKS = "/SCSApp_WebviewLinks"
