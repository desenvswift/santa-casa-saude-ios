//
//  AppConstants.swift
//  Catao
//
//  Created by Catao on 18/12/2017.
//  Copyright © 2017 Catao. All rights reserved.
//

import Foundation

let KEY_USER_INFOS = "keyuserinfos"
let KEY_USER_TOKEN = "usertokenkey"
let KEY_USER_NAME = "keyusername"
let KEY_USER_PLAN = "keyuserplan"
let LAST_ASK_UPDATE = "lastaskupdate"
let KEY_USER_PICTURE = "keyuserpicture"
let KEY_USER_OPEN_TUTORIAL = "KEY_USER_OPEN_TUTORIAL"
let KEY_USER_CARTEIRINHAS = "KEY_USER_CARTEIRINHASKEY_USER_CARTEIRINHAS"
let NO_RESULTS_IMAGE = "no-results-icon"
let NAME_PLACEHOLDER_NO_PHOTO = "placeholder-no-photo"

let ERROR_NOT_CONNECTED = -1

let ERROR_SOMETHING_WENT_WRONG = "Erro desconhecido. Tente novamente."

let KEY_PREFERENCE_NOTIFICATION = "KEY_USER_PREFERENCE_NOTIFICATION"
let KEY_PREFERENCE_LOCATION = "KEY_USER_PREFERENCE_LOCATION"
