//
//  Colors.swift
//  Catao
//
//  Created by Catao on 18/12/2017.
//  Copyright © 2017 Catao. All rights reserved.
//

import UIKit

class Colors {
    
    //https://stackoverflow.com/questions/24074257/how-to-use-uicolorfromrgb-value-in-swift
    static let MAIN_COLOR = UIColorFromRGB(rgbValue: 0x0060AE)
    static let BACKGROUND_COLOR = UIColorFromRGB(rgbValue: 0xEFEFEF)
    static let MODAL_SHADOW_COLOR = UIColorFromRGBWithAlpha(rgbValue: 0x000000, alpha: UInt(0.1))
    
    static let Primary = UIColor(named: "Primary")
    static let ButtonDisable = UIColor(named: "ButtonDisable")
    static let DarkGray = UIColor(named: "DarkGray")
    static let Error = UIColor(named: "Error")
    static let Gray = UIColor(named: "Gray")
    static let LightGray = UIColor(named: "LightGray")
    static let MediumGray = UIColor(named: "MediumGray")
    static let PrimaryButton = UIColor(named: "PrimaryButton")
    static let Success = UIColor(named: "Success")
    static let Warning = UIColor(named: "Warning")
    static let White = UIColor(named: "White")
    
    static func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func UIColorFromRGBWithAlpha(rgbValue: UInt, alpha: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alpha)
        )
    }
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

