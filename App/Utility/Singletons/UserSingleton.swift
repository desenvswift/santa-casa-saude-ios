//
//  UserSingleton.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 25/05/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import Foundation

class UserSingleton {
    var user : SignInResponse?
    static let shared = UserSingleton()
    
    private init() {
        
    }
    
    func loadUser(){
        if let json = UserDefaults.standard.string(forKey: KEY_USER_INFOS) {
            user = SignInResponse(json: json)
        }
    }
    
    func invalidateUser() {
        self.user = nil
    }
    
}
