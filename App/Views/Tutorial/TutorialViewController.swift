//
//  TutorialViewController.swift
//  Project
//
//  Created by Victor Catão on 03/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {
    
    let CELL_IDENTIFIER = "TutorialCollectionViewCell"
    
    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    // MARK: - Variables
    private let viewModel = TutorialViewModel()
    var isFromMenu = false
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.downloadData()
    }
    
    // MARK: - Download
    func downloadData() {
        self.showLoader()
        self.viewModel.fetchTutorial(success: {
            self.hideLoader()
            self.setupPageControl()
            self.collectionView.reloadData()
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    // MARK: - Setups
    private func setup() {
        UserDefaults.standard.set(true, forKey: KEY_USER_OPEN_TUTORIAL)
        setupCollectionView()
        setupPageControl()
    }
    
    func setupCollectionView() {
        self.collectionView.registerNib(named: CELL_IDENTIFIER)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    func setupPageControl() {
        self.pageControl.numberOfPages = self.viewModel.getNumberOfItems()
        self.pageControl.currentPage = 0
    }
    
    // MARK: - Actions
    @IBAction func didTapClose(_ sender: Any) {
        if isFromMenu {
            self.dismiss(animated: true, completion: nil)
        } else {
            AppDelegate.setupRootWindow()
        }
    }
}

// MARK: - CollectionView
extension TutorialViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.getNumberOfItems()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CELL_IDENTIFIER, for: indexPath) as! TutorialCollectionViewCell
        cell.setup(tutorial: self.viewModel.getTutorial(at: indexPath.row))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.item
    }
}
