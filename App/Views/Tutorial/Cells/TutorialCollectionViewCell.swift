//
//  TutorialCollectionViewCell.swift
//  Base
//
//  Created by Victor Catão on 03/05/19.
//  Copyright © 2019 Catao. All rights reserved.
//

import UIKit

class TutorialCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var tutorialImageView: UIImageView!
//    @IBOutlet weak var tutorialTextLabel: UILabel!
    
    func setup(tutorial: TutorialResponse) {
        self.tutorialImageView.setBlobImage(string: tutorial.imagem?.replacingOccurrences(of: "data:image/png;base64,", with: ""))
//        self.tutorialTextLabel.text = tutorial.texto
    }

}
