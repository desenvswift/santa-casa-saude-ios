//
//  TutorialViewModel.swift
//  Project
//
//  Created by Victor Catão on 03/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class TutorialViewModel {
    
    private var list: [TutorialResponse] = []
    
    
    func fetchTutorial(success: @escaping ()->(), error: @escaping (String)->()) {
        SCSManager.getTutorial(success: { (response) in
            self.list = response.lista ?? []
            success()
        }) { (msg, _) in
            error(msg)
        }
    }
    
    func getNumberOfItems() -> Int {
        return list.count
    }
    
    func getTutorial(at index: Int) -> TutorialResponse {
        return list[index]
    }
    
}
