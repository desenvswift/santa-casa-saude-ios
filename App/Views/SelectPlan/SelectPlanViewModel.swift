//
//  SelectPlanViewModel.swift
//  Project
//
//  Created by Victor Catão on 03/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class SelectPlanViewModel {
    
    var plans: [Plano] = []
    
    func getNumberOfRows() -> Int {
        return plans.count
    }
    
    func getPlan(at: Int) -> Plano {
        return plans[at]
    }
    
    func selectPlan(at index: Int, success: @escaping (CataoModel)->(), error: @escaping (String)->()) {
        let request = UpdateUserPlanRequest()
        request.matTitular = plans[index].MatTitular
        
        SCSManager.updateUserPlan(request: request, success: { (response) in
            success(response)
        }) { (msg, _) in
            error(msg)
        }
    }
    
    
}
