//
//  PlanTableViewCell.swift
//  Base
//
//  Created by Victor Catão on 03/05/19.
//  Copyright © 2019 Catao. All rights reserved.
//

import UIKit

class PlanTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setup(name: String?) {
        self.nameLabel.text = name
    }
    
}
