//
//  SelectPlanViewController.swift
//  Project
//
//  Created by Victor Catão on 03/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

protocol SelectPlanViewControllerDelegate: class {
    func didSelectPlan()
}

class SelectPlanViewController: UIViewController {
    
    convenience required init(plans: [Plano]) {
        self.init()
        self.viewModel.plans = plans
    }
    
    let CELL_IDENTIFIER = "PlanTableViewCell"
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variables
    private let viewModel = SelectPlanViewModel()
    weak var delegate: SelectPlanViewControllerDelegate?
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
//        self.downloadData()
    }
    
    // MARK: - Setups
    private func setup() {
        setupTableView()
    }
    
    func setupTableView() {
        self.tableView.registerNib(named: CELL_IDENTIFIER)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    // MARK: - DownloadData
    func downloadData() {
//        self.viewModel.fetchPlans(success: { (response) in
//            
//            let status = response.getStatus()
//            let msg = response.getMsgExterna()
//            
//            if status == 1 {
//                self.tableView.reloadData()
//            } else {
//                self.showErrorAlert(message: msg ?? ERROR_SOMETHING_WENT_WRONG)
//            }
//            
//        }) { (msg) in
//            self.showErrorAlert(message: msg)
//        }
    }
    
}

// MARK: - TableView
extension SelectPlanViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as! PlanTableViewCell
        cell.selectionStyle = .none
        cell.setup(name: self.viewModel.getPlan(at: indexPath.row).PlanoDsc)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.selectPlan(at: indexPath.row, success: { (response) in
            let status = response.getStatus()
            let msg = response.getMsgExterna()
            
            if status == 1 {
                UserDefaults.standard.set(self.viewModel.getPlan(at: indexPath.row).PlanoDsc, forKey: KEY_USER_PLAN)
                
                self.dismiss(animated: true, completion: {
                    self.delegate?.didSelectPlan()
                })
            } else {
                self.showErrorAlert(message: msg ?? ERROR_SOMETHING_WENT_WRONG)
            }
        }) { (msg) in
            self.showErrorAlert(message: msg)
        }
    }
}
