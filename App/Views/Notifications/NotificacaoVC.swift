//
//  NotificacaoVC.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 26/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class NotificacaoVC: UIViewController {

    let CELL_IDENTIFIER = "NotificationTableViewCell"
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variables
    private let viewModel = NotificacaoViewModel()
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.downloadData()
//        setupTabBarCollor()
        setNavBar(close: true)
    }
    
    // MARK: - Setups
    private func setup() {
        setupTableView()
    }
    
    private func setupTableView() {
        self.tableView.registerNib(named: CELL_IDENTIFIER)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorColor = .clear
    }
    
    private func downloadData() {
        self.showLoader()
        self.viewModel.fetchData(success: {
            self.hideLoader()
            self.tableView.reloadData()
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    func openNotificacao(at index: Int) {
        if let link = self.viewModel.homeResponse?[index].link, let url = URL(string: link) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
}

// MARK: - TableView
extension NotificacaoVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows = self.viewModel.getNumberOfRows()
//        emptyView.isHidden = numberOfRows != 0
        return numberOfRows
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as! NotificationTableViewCell
        cell.setup(item: self.viewModel.homeResponse?[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.openNotificacao(at: indexPath.row)
    }
    
}
