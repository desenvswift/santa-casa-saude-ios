//
//  NotificacaoViewModel.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 26/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import Foundation

class NotificacaoViewModel{
    
    var homeResponse: [PainelBoleto]?
    
    func fetchData(success: @escaping()->(), error: @escaping(String)->()) {
        SCSManager.getHome(success: { (response) in
//            var exibir = response.painelNotificacoes
//            for not in NotificationName.allCases{
//                exibir = exibir?.filter({ $0.titulo != not.rawValue})
//            }
//            self.homeResponse = exibir
            self.homeResponse = response.painelNotificacoes
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    
    func getNumberOfRows() -> Int {
        return self.homeResponse?.count ?? 0
    }
    
}
