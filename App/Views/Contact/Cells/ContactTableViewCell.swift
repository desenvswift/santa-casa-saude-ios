//
//  ContactTableViewCell.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 04/07/20.
//  Copyright © 2020 Mobile2You. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setup(contact: SubGroup) {
        titleLabel.text = contact.titulo
        descriptionLabel.text = contact.valor
    }
    
}
