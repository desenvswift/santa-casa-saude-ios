//
//  ContactViewModel.swift
//  Project
//
//  Created by Victor Catão on 29/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class ContactViewModel {
    
    var responseContacts: [ContactList] = []
    
    func numberOfSections() -> Int {
        return responseContacts.count
    }
    
    func numberOfRows(at section: Int) -> Int {
        return responseContacts[section].subgrupo?.count ?? 0
    }
    
    func getContact(at ip: IndexPath) -> SubGroup {
        return responseContacts[ip.section].subgrupo?[ip.item] ?? SubGroup()
    }
    
    func getHeader(for section: Int) -> String {
        return responseContacts[section].nome ?? ""
    }
    
    func fetchContacts(success: @escaping()->(), error: @escaping(String)->()) {
        SCSManager.getContact(success: { (response) in
            self.responseContacts = response.lista ?? []
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
}
