//
//  ContactViewController.swift
//  Project
//
//  Created by Victor Catão on 29/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variables
    private let viewModel = ContactViewModel()
    
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        fetchData()
    }
    
    // MARK: - Setups
    private func setup() {
        setNavBar()
        tableView.registerNib(named: "ContactTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func fetchData() {
        self.showLoader()
        viewModel.fetchContacts(success: {
            self.hideLoader()
            self.tableView.reloadData()
        }) { (errormsg) in
            self.hideLoader()
            self.showErrorAlert(message: errormsg)
        }
    }
    
}

extension ContactViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        v.backgroundColor = .clear
        
        let label = UILabel(frame: CGRect(x: 20, y: 20, width: UIScreen.main.bounds.width, height: 40))
        label.font = .systemFont(ofSize: 16, weight: .bold)
        label.textColor = Colors.DarkGray
        label.text = viewModel.getHeader(for: section)
        
        v.addSubview(label)
        return v
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(at: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell") as! ContactTableViewCell
        cell.selectionStyle = .none
        cell.setup(contact: viewModel.getContact(at: indexPath))
        return cell
    }
}
