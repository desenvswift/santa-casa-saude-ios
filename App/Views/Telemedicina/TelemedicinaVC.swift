//
//  TelemedicinaVC.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 16/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class TelemedicinaVC: UIViewController {

    @IBOutlet weak var viewEspecialidades: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavBar(close: true)
        self.showModalNotification(notficacao: HomeResponse.verifyNotificationFor(location: .telemedicina))
    }

    @IBAction func gotoTeleCOnsulta(_ sender: Any) {
        viewEspecialidades.isHidden = false
    }
    
    @IBAction func gotoAgendar(_ sender: Any) {
        gotoLink(forLocation: .MnTelemedicinaAgenda)
    }
    
    
    @IBAction func gotoPA(_ sender: Any) {
        gotoLink(forLocation: .MnTelemedicinaPAdigi)
    }
    
    @IBAction func fecharEspecialidades(_ sender: Any) {
        viewEspecialidades.isHidden = true
    }
    
    @IBAction func gotoTutorial(_ sender: Any) {
        gotoLink(forLocation: .MnTelemedicinaTutorial)
    }
    
    
    func gotoLink(forLocation: WebViewCodes){
        showLoader()
        getLink(forLocation: forLocation, success: { link in
            self.hideLoader()
            if let url = URL(string: link ?? "") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else{
                self.showErrorAlert(message: "Erro ao carregar URL")
            }
        }) {msg in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
}
