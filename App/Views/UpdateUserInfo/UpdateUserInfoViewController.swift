//
//  UpdateUserInfoViewController.swift
//  Project
//
//  Created by Victor Catão on 19/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

enum UpdateUserType {
    case mail
    case phone
    case password
}

class UpdateUserInfoViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var viewConfirmacao: CustomEditText!
    @IBOutlet weak var viewSenha: CustomEditText!
    
    // MARK: - Variables
    private let viewModel = UpdateUserInfoViewModel()
    var viewType: UpdateUserType!
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.closeKeyboardOnTouch()
    }
    
    // MARK: - Setups
    private func setup() {
        setNavBar()
        setupTextFields()
    }
    
    private func setupTextFields() {
        viewSenha.configure(textFieldType: .PASSWORD)
        viewConfirmacao.configure(textFieldType: .PASSWORD)
        self.viewConfirmacao.validateBlock = {
            return self.viewSenha.getTexto() == self.viewConfirmacao.getTexto()
        }
    }
    
    @IBAction func didTapSave(_ sender: Any) {
        viewConfirmacao.validate()
        viewSenha.validate()
        if !viewConfirmacao.isValid() {
            self.showErrorAlert(message: "Senha e confirmação de senha não coincidem.")
            return
        }
        
        self.showLoader()
        self.viewModel.updatePassword(password: self.viewSenha.getTexto() ?? "", success: {
            self.showSuccessMessage()
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    private func showSuccessMessage() {
        self.showSuccessAlert(message: "Atualizado com sucesso", okBlock: {
            self.navigationController?.popViewController(animated: true)
        }, cancelBlock: nil)
        self.hideLoader()
    }
}
