//
//  UpdateUserInfoViewModel.swift
//  Project
//
//  Created by Victor Catão on 19/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class UpdateUserInfoViewModel {
    
    func updateMail(email: String, success: @escaping ()->(), error: @escaping (String)->()){
        SCSManager.updateMail(email: email, success: {
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func updatePhone(ddd: String, phone: String, success: @escaping ()->(), error: @escaping (String)->()){
        SCSManager.updatePhone(ddd: ddd, phone: phone, success: {
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func updatePassword(password: String, success: @escaping ()->(), error: @escaping (String)->()){
        SCSManager.updatePassword(password: password, success: {
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    
    
    
}
