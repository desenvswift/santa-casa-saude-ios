//
//  BoletoTableViewCell.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 16/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class BoletoTableViewCell: UITableViewCell {

    @IBOutlet weak var imgBoleto: UIImageView!
    @IBOutlet weak var lblData: UILabel!
    @IBOutlet weak var lblValor: UILabel!
    @IBOutlet weak var viewArredondar: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewArredondar.layer.cornerRadius = 8
        // Initialization code
    }

    func setup(boleto: BoletoModel) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/yyyy"
        let date = dateFormatter.date(from: boleto.descricao ?? "01/0001")
        dateFormatter.dateFormat = "MMM yy"
        let data = dateFormatter.string(from: date ?? Date())
        self.lblData.text = data.uppercased().replacingOccurrences(of: ".", with: "")
//        self.lblValor.text = boleto.valor?.currencyInputFormatting()
        self.lblValor.text = boleto.valor
        if boleto.status == "ABERTO"{
            self.imgBoleto.image = UIImage(named: "Boleto_aberto")
        }
        else if boleto.status == "PAGO" || boleto.status == "Repactuada"{
            self.imgBoleto.image = UIImage(named: "Boleto")
        }
        else{
            self.imgBoleto.image = UIImage(named: "Boleto_vencido")
        }
//        self.imgBoleto.image = boleto.estaPago?.boolValue == true ? UIImage(named: "Boleto") : UIImage(named: "Boleto_vencido")
        }
    
}
