//
//  BoletoViewModel.swift
//  Project
//
//  Created by Victor Catão on 28/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class BoletoViewModel {
    
    private var boletos: [BoletoModel] = []
    private var anosFiltro: [String] = []
    
    func fetchBoletos(ano: String, success: @escaping()->(), error: @escaping(String)->()) {
        SCSManager.getboletos(ano: ano, success: { (boletos) in
            self.boletos = boletos
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func getYearsFilter(success: @escaping([String])->(), error: @escaping(String)->()) {
        if anosFiltro.isEmpty{
            SCSManager.getBoletosFiltros(success: { (response) in
                for item in response{
                    self.anosFiltro.append(item.chave ?? "")
                }
                success(self.anosFiltro)
            }) { (msg, status) in
                error(msg)
            }
        }
        else{
            success(anosFiltro)
        }
    }
    
    func setFilter(ano: Int){
        print(ano)
    }
    
    func getNumberOfRows() -> Int {
        return self.boletos.count
    }
    
    func getBoleto(at index: Int) -> BoletoModel {
        return self.boletos[index]
    }
    
}
