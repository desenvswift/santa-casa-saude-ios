//
//  BoletoViewController.swift
//  Project
//
//  Created by Victor Catão on 28/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class BoletoViewController: UIViewController {
    
    let CELL_IDENTIFIER = "BoletoTableViewCell"
    
    @IBOutlet weak var tableView: UITableView!
    var ano_filtro = String(Calendar.current.component(.year, from: Date()))
    
    // MARK: - Variables
    private let viewModel = BoletoViewModel()
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.downloadData()
//        setupTabBarCollor()
        setNavBar()
    }
    
    // MARK: - Setups
    private func setup() {
        setupTableView()
        setupNav()
    }
    
    func setupNav() {
        let sidemenuBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        sidemenuBtn.setImage(UIImage(named: "Filter")?.resize(maxWidthHeight: 20), for: .normal)
        sidemenuBtn.addTarget(self, action: #selector(self.didTapShare), for: .touchUpInside)
        sidemenuBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let sideMenuBarBtn = UIBarButtonItem(customView: sidemenuBtn)

        self.navigationItem.setRightBarButtonItems([sideMenuBarBtn], animated: false)
    }
    
    @objc func didTapShare() {
        self.viewModel.getYearsFilter(success: { arr in
            ActionSheetStringPicker.show(withTitle: "Selecione um ano", rows: arr , initialSelection: 0, doneBlock: { (picker, index, strings) in
                self.ano_filtro = strings as! String
                self.downloadData()
            }, cancel: { (picker) in
                // do nothing
            }, origin: self.view)
        }, error: { error in
            print("Erro")
            
        })
            
    }
    
    private func setupTableView() {
        self.tableView.registerNib(named: CELL_IDENTIFIER)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorColor = .clear
    }
    
    private func downloadData() {
        self.showLoader()
        self.viewModel.fetchBoletos(ano: ano_filtro, success: {
            self.hideLoader()
            self.tableView.reloadData()
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    func openBoleto(at index: Int) {
        let vc = BoletoDetailViewController(boleto: self.viewModel.getBoleto(at: index))
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

// MARK: - TableView
extension BoletoViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows = self.viewModel.getNumberOfRows()
//        emptyView.isHidden = numberOfRows != 0
        return numberOfRows
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as! BoletoTableViewCell
        cell.setup(boleto: self.viewModel.getBoleto(at: indexPath.row))
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.openBoleto(at: indexPath.row)
    }
    
}
