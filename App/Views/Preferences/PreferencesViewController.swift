//
//  PreferencesViewController.swift
//  Project
//
//  Created by Victor Catão on 22/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class PreferencesViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var notificationSwitch: UISwitch!
    @IBOutlet weak var locationSwitch: UISwitch!
    @IBOutlet weak var darkModeSwitch: UISwitch!
    @IBOutlet weak var viewModoEscuro: UIView!
    
    // MARK: - Variables
    private let viewModel = PreferencesViewModel()
    
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    // MARK: - Setups
    private func setup() {
        setNavBar()
        setupView()
    }
    
    private func setupView() {
        self.notificationSwitch.isOn = UserDefaults.standard.bool(forKey: KEY_PREFERENCE_NOTIFICATION)
        self.locationSwitch.isOn = UserDefaults.standard.bool(forKey: KEY_PREFERENCE_LOCATION)
        
        if #available(iOS 12.0, *) {
            self.darkModeSwitch.isOn = self.traitCollection.userInterfaceStyle == .dark
        } else {
            self.viewModoEscuro.isHidden = true
        }
    }
    
    @IBAction func didSwitchNotification(_ sender: Any) {
        let value = !UserDefaults.standard.bool(forKey: KEY_PREFERENCE_NOTIFICATION)
        UserDefaults.standard.set(value, forKey: KEY_PREFERENCE_NOTIFICATION)
    }
    
    @IBAction func didSwitchLocation(_ sender: Any) {
        let value = !UserDefaults.standard.bool(forKey: KEY_PREFERENCE_LOCATION)
        UserDefaults.standard.set(value, forKey: KEY_PREFERENCE_LOCATION)
    }
    
    @IBAction func darkMode(_ sender: Any) {
        AppDelegate.setDarkMode(darkModeSwitch.isOn)
    }
}
