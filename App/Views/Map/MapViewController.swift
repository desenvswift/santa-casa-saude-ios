//
//  MapViewController.swift
//  Project
//
//  Created by Victor Catão on 01/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController {
    
    convenience required init(places: [PlaceResponse]) {
        self.init()
        self.viewModel.places = places
    }
    
    var markers: [GMSMarker] = []
    // MARK: - Variables
    private let viewModel = MapViewModel()
    @IBOutlet weak var lblNome: UILabel!
    @IBOutlet weak var viewMapa: UIView!
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblEspecialidade: UILabel!
    @IBOutlet weak var lblEndereco: UILabel!
    @IBOutlet weak var lblDistancia: UILabel!
    @IBOutlet weak var viewInfos: UIView!
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    // MARK: - Setups
    private func setup() {
        setNavBar(withColor: Colors.Primary ?? .blue)
        setupMap()
        viewInfos.layer.cornerRadius = 8
        viewInfos.isHidden = true
    }
    
    func setupMap() {
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        let (mainLat, mainLng) = self.viewModel.getMainPlaceLatLng()
        let camera = GMSCameraPosition.camera(withLatitude: mainLat, longitude: mainLng, zoom: 14.0)
        let mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.viewMapa.frame.width, height: self.viewMapa.frame.height), camera: camera)
        mapView.delegate = self
        viewMapa.addSubview(mapView)
        
        // Creates a marker in the center of the map.
        for place in self.viewModel.places {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: place.prestadorLatitude?.replacingOccurrences(of: ",", with: ".").doubleValue ?? 0,
                                                     longitude: place.prestadorLongitude?.replacingOccurrences(of: ",", with: ".").doubleValue ?? 0)
//            marker.title = place.prestadorNome
//            marker.snippet = "Australia"
            marker.map = mapView
            marker.icon = UIImage(named: "nem_pin_map")
            markers.append(marker)
        }
        
    }
    
    func preencherInfos(place: PlaceResponse?){
        lblNome.text = place?.prestadorNome
        lblEspecialidade.text = "\(place?.especialidadeDsc ?? "") | CRM \(place?.prestadorNumero ?? "")"
        var endereco = "\(place?.prestadorLogradouro ?? ""), \(place?.prestadorNumero ?? "") - \(place?.prestadorBairro ?? "")"
        endereco += "\n\(place?.cidadeDsc ?? "") - \(place?.prestadorUF ?? "")"
        lblEndereco.text = endereco
        self.lblDistancia.text = String.init(format: "%.2f", place?.prestadorDistancia?.floatValue ?? 0.0)
        viewInfos.isHidden = false
    }
    
}


// extension for GMSMapViewDelegate
extension MapViewController: GMSMapViewDelegate {
    
    // tap map marker
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("didTap marker \(marker.title ?? "")")
        
        // remove color from currently selected marker
        if let selectedMarker = mapView.selectedMarker {
            selectedMarker.icon = UIImage(named: "nem_pin_map")
        }
        
        // select new marker and make green
        mapView.selectedMarker = marker
        marker.icon = UIImage(named: "new_pin_map_selected")
        
        if let index = self.markers.firstIndex(of: marker){
            self.preencherInfos(place: self.viewModel.places[index])
        }
        
        // tap event handled by delegate
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        for mark in self.markers{
            mark.icon = UIImage(named: "nem_pin_map")
        }
        self.viewInfos.isHidden = true
    }
}
    
