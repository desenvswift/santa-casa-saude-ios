//
//  MapViewModel.swift
//  Project
//
//  Created by Victor Catão on 01/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class MapViewModel {
    
    var places: [PlaceResponse] = []
    
    func getMainPlaceLatLng() -> (Double, Double) {
        return (self.places.first?.prestadorLatitude?.replacingOccurrences(of: ",", with: ".").doubleValue ?? 0, self.places.first?.prestadorLongitude?.replacingOccurrences(of: ",", with: ".").doubleValue ?? 0)
    }
    
}
