//
//  ProfileViewModel.swift
//  Project
//
//  Created by Victor Catão on 01/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class ProfileViewModel {
    
    func fetchUser(success: @escaping ()->(), error: @escaping (String)->()){
        SCSManager.getUser(success: { (user) in
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func updateMail(email: String, success: @escaping ()->(), error: @escaping (String)->()){
        SCSManager.updateMail(email: email, success: {
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func updatePhone(ddd: String, phone: String, success: @escaping ()->(), error: @escaping (String)->()){
        SCSManager.updatePhone(ddd: ddd, phone: phone, success: {
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
}
