//
//  ProfileViewController.swift
//  Project
//
//  Created by Victor Catão on 01/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var viewCelular: CustomEditText!
    @IBOutlet weak var viewEmail: CustomEditText!
    @IBOutlet weak var viewCpf: CustomEditText!
    @IBOutlet weak var viewNascimento: CustomEditText!
    @IBOutlet weak var viewInclusao: CustomEditText!
    
    
    // MARK: - Variables
    private let viewModel = ProfileViewModel()
    
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.downloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Setups
    private func setup() {
        self.setNavBar()
        setupView()
        self.closeKeyboardOnTouch()
//        self.setupAutoScrollWhenKeyboardShowsUp()
        viewCelular.editText.delegate = self
        viewEmail.editText.delegate = self
    }
    
    
    // MARK: - Data
    private func downloadData() {
        self.showLoader()
        self.viewModel.fetchUser(success: {
            self.setupView()
            self.hideLoader()
        }) { (msg) in
            self.showErrorAlert(message: msg)
            self.hideLoader()
        }
        
    }
    
    // MARK: - Actions
    @objc func didTapemailView(){
        let vc = UpdateUserInfoViewController()
        vc.viewType = .mail
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func didTapcellphoneView(){
        let vc = UpdateUserInfoViewController()
        vc.viewType = .phone
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func didTappasswordView(){
        let vc = UpdateUserInfoViewController()
        vc.viewType = .password
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func setupView() {
        viewCelular.configure(textFieldType: .PHONE)
        viewEmail.configure(textFieldType: .EMAIL)
        let user = UserSingleton.shared.user?.beneficiario
        
        self.viewEmail.texto = user?.email ?? ""
        self.viewCelular.texto = "\(user?.ddd ?? "") \(user?.celular ?? "")"
        self.viewCpf.texto = user?.cpfBeneficiario ?? ""
        self.viewCpf.enable = false
        self.viewNascimento.texto = user?.dtaNasc ?? ""
        self.viewNascimento.enable = false
        self.viewInclusao.texto = user?.data_inclusao ?? ""
        self.viewInclusao.enable = false
        
    }
    
    func updatePhone(){
        guard let ddd = viewCelular.getDDD(), let phone = viewCelular.getPhone() else{ return }
        self.viewModel.updatePhone(ddd: ddd, phone: phone, success: {})
        { (msg) in
            self.showErrorAlert(message: msg)
        }
    }
    
    func updateEmail(){
        guard let email = viewEmail.getTexto() else{ return }
        self.viewModel.updateMail(email: email, success: {
            self.showSuccessMessage()
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    private func showSuccessMessage() {
        self.hideLoader()
        self.showModalGeneric(titulo: "", desc: "Atualizado com sucesso", exibirConfirmar: true, exibirCancelar: false, acaoConfirmar: {
            self.navigationController?.popViewController(animated: true)
        }, acaoCancelar: {} )
    }
    
    
    @IBAction func salvar(_ sender: Any) {
        self.showLoader()
        viewCelular.validate()
        viewEmail.validate()
        if viewCelular.isValid() && viewEmail.isValid(){
            updatePhone()
            updateEmail()
        }
    }
}
