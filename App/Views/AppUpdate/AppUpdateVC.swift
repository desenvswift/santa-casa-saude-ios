//
//  AppUpdateVC.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 01/06/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class AppUpdateVC: UIViewController {
    
    var exibeFechar = true
    @IBOutlet weak var btnFechar: CustomButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnFechar.isHidden = !exibeFechar
    }

    @IBAction func atualizar(_ sender: Any) {
//        let urlApp = "https://apps.apple.com/br/app/plano-santa-casa-sa%C3%BAde/id1473191236"
        if let url = URL(string: "itms-apps://apple.com/app/id1473191236") {
            UIApplication.shared.open(url)
        }
    }
    

    @IBAction func fechar(_ sender: Any) {
        self.dismiss(animated: true)
    }
}
