//
//  TalkWithUsViewModel.swift
//  Project
//
//  Created by Victor Catão on 19/05/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit
import FirebaseMessaging

class TalkWithUsViewModel {
    
    var subjects: [SubjectResponse] = []
    var selectedSubject: SubjectResponse?
    
    func sendMessage( name: String?, city: String?, mail: String?, phone: String?, cardNumber: String?, cpf: String?, subject: String?, message: String?, success: @escaping ()->(), error: @escaping (String)->()){
        
        let req = TalkWithUsRequest()
        req.idDispositivo = Messaging.messaging().fcmToken ?? UIDevice.current.identifierForVendor?.uuidString
        req.nome = name
        req.cidade = city
        req.email = mail
        req.telefone = phone
        req.celular = phone
        req.numeroCarterinha = cardNumber
        req.cpf = cpf
        req.codAssunto = selectedSubject?.valor
        req.msg = message
        
        SCSManager.sendMessage(request: req, success: {
            success()
        }) { (msg, _) in
            error(msg)
        }
    }
    
    func fetchSubjects(success: @escaping ()->(), error: @escaping (String)->()) {
        SCSManager.getSubsjects(success: { (response) in
            self.subjects = response.comboAssunto ?? []
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func selectSubject(_ desc: String) {
        self.selectedSubject = self.subjects.first(where: {$0.descricao == desc})
    }
    
}
