//
//  TalkWithUsViewController.swift
//  Project
//
//  Created by Victor Catão on 19/05/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class TalkWithUsViewController: UIViewController {
    
    let MAX_CHRACTERES = 180
    
    // MARK: - ScrollView
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameTextField: CataoTextFieldClass!
    @IBOutlet weak var cityTextField: CataoTextFieldClass!
    @IBOutlet weak var mailTextField: CataoTextFieldClass!
    @IBOutlet weak var phoneTextField: CataoTextFieldClass!
    @IBOutlet weak var cardNumberTextField: CataoTextFieldClass!
    @IBOutlet weak var cpfTextField: CataoTextFieldClass!
    @IBOutlet weak var subjectTextField: CataoTextFieldClass!
    @IBOutlet weak var messageTextView: UITextView!
    
    @IBOutlet weak var charactersCountLabel: UILabel!
    
    // MARK: - Variables
    private let viewModel = TalkWithUsViewModel()
    
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
        self.downloadData()
    }
    
    // MARK: - Setups
    private func setup() {
        title = "Fale Conosco"
        setupAutoScrollWhenKeyboardShowsUp()
        setupTextFields()
        setupBack()
    }
    
    func setupTextFields() {
        nameTextField.setup(textFieldType: .NAME)
        cityTextField.validateBlock = { return (self.cityTextField.text?.count ?? 0) > 3 }
        mailTextField.setup(textFieldType: .EMAIL)
        phoneTextField.setup(textFieldType: .PHONE)
        cardNumberTextField.keyboardType = .numberPad
        cpfTextField.setup(textFieldType: .CPF)
        subjectTextField.validateBlock = { return (self.subjectTextField.text?.count ?? 0) > 3 }
        messageTextView.delegate = self
        subjectTextField.delegate = self
        
        if isUserLogged() {
            self.nameTextField.isHidden = true
            self.cityTextField.isHidden = true
            self.mailTextField.isHidden = true
            self.phoneTextField.isHidden = true
            self.cardNumberTextField.isHidden = true
            self.cpfTextField.isHidden = true
        }
    }
    
    func downloadData() {
        self.showLoader()
        self.viewModel.fetchSubjects(success: {
            self.hideLoader()
        }) { (msg) in
            self.hideLoader()
        }
    }
    
    func openSubjects() {
        ActionSheetStringPicker.show(withTitle: "Selecione o Plano", rows: self.viewModel.subjects.map({$0.descricao ?? ""}), initialSelection: 0, doneBlock: { (picker, index, str) in
            self.subjectTextField.text = str as? String
            self.viewModel.selectSubject(str as! String)
        }, cancel: { (picker) in
            
        }, origin: self.view)
    }
    
    // MARK: - ScrollView
    override func setScrollViewContentInset(_ inset: UIEdgeInsets) {
        scrollView.contentInset = inset
    }
    
    // MARK: - Actions
    @IBAction func didTapSend(_ sender: Any) {
        
        if(messageTextView.text == "") {
            self.showErrorAlert(message: "Preencha a mensagem")
            return
        }
        
        if(!subjectTextField.isValid()) {
            self.showErrorAlert(message: "Preencha o assunto")
            return
        }
        
        if isUserLogged() {
            sendMessage()
        } else {
            if(!nameTextField.isValid()){
                self.showErrorAlert(message: "Preencha o nome corretamente")
                return
            }
            
            if(!cityTextField.isValid()){
                self.showErrorAlert(message: "Preencha a cidade corretamente")
                return
            }
            
            if(!mailTextField.isValid()){
                self.showErrorAlert(message: "Preencha o e-mail corretamente")
                return
            }
            
            if(!phoneTextField.isValid()){
                self.showErrorAlert(message: "Preencha o telefone corretamente")
                return
            }
            
            if(!cpfTextField.isValid()){
                self.showErrorAlert(message: "Preencha o cpf corretamente")
                return
            }
            sendMessage()
        }
    }
    
    func sendMessage() {
        self.showLoader()
        self.viewModel.sendMessage(name: nameTextField.text, city: cityTextField.text, mail: mailTextField.text, phone: phoneTextField.text, cardNumber: cardNumberTextField.text, cpf: cpfTextField.text, subject: subjectTextField.text, message: messageTextView.text, success: {
            self.hideLoader()
            self.showSuccessAlert(message: "Mensagem enviada com sucesso!", okBlock: {
                self.navigationController?.popViewController(animated: true)
            }, cancelBlock: nil)
        }, error: { (msg) in
            self.hideLoader()
        })
    }
}

// MARK: - UITextViewDelegate
extension TalkWithUsViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "" { return true }
        return (self.messageTextView.text?.count ?? 0) < MAX_CHRACTERES
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.charactersCountLabel.text = "\(MAX_CHRACTERES - textView.text.count) caracteres restantes"
    }
    
}

// MARK: - UITextFieldDelegate
extension TalkWithUsViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        openSubjects()
        return false
    }
}
