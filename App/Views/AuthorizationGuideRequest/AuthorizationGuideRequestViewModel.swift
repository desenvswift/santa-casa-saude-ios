//
//  AuthorizationGuideRequestViewModel.swift
//  Project
//
//  Created by Victor Catão on 14/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit
import RxSwift
import GooglePlaces

class AuthorizationGuideRequestViewModel {
    
    let bag = DisposeBag()
    let selectedCardNumber = Variable<SubjectResponse?>(nil)
    let selectedSolicitationType = Variable<SubjectResponse?>(nil)
    let selectedAddress = Variable<String?>(nil)
    let selectedDate = Variable<String?>(nil)
    
    let photo1 = Variable<UIImage?>(nil)
    let photo2 = Variable<UIImage?>(nil)
    let photo3 = Variable<UIImage?>(nil)
    
    let request = CreateAuthorizationGuideRequest()
    
    var place:GMSPlace?
    
    var currentPhotoSelection = 0
    
    private var inputs: AuthorizationGuideInput?
    var carteirinhas: [String] {
        get {
            return inputs?.comboCarterinha?.map({$0.descricao ?? "-"}) ?? []
        }
    }
    var soliciatacoes: [String] {
        get {
            return inputs?.combotipoSolicitacao?.map({$0.descricao ?? "-"}) ?? []
        }
    }
    
    func fetchInputs(success: @escaping ()->(), error: @escaping (String)->()) {
        SCSManager.getAuthorizationGuideInput(success: { (response) in
            self.inputs = response
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    
    func deletePhoto() {
        switch currentPhotoSelection {
        case 1:
            self.photo1.value = nil
        case 2:
            self.photo2.value = nil
        default:
            self.photo3.value = nil
        }
    }
    
    func setPhoto(_ image: UIImage?) {
        switch currentPhotoSelection {
        case 1:
            self.photo1.value = image
        case 2:
            self.photo2.value = image
        default:
            self.photo3.value = image
        }
    }
    
    func setCardNumber(atIndex: Int) {
        self.selectedCardNumber.value = self.inputs?.comboCarterinha?[atIndex]
        request.matBen = self.selectedCardNumber.value?.valor
    }
    
    func setSoliciationType(atIndex: Int) {
        self.selectedSolicitationType.value = self.inputs?.combotipoSolicitacao?[atIndex]
        request.tipSol = self.selectedSolicitationType.value?.valor
    }
    
    func setPlace(place: GMSPlace) {
        self.place = place
        self.selectedAddress.value = place.name
        self.request.locExame = self.selectedAddress.value
    }
    
    func selectDate(_ dateBR: String, date: Date){
        self.selectedDate.value = dateBR
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        request.dtaExame = dateFormatter.string(from: date)
    }
    
    func isValidFields() -> Bool {
        return self.selectedCardNumber.value != nil && self.selectedSolicitationType.value != nil && (self.photo1.value != nil || self.photo2.value != nil || self.photo3.value != nil)
    }
    
    func createAuthorizationGuide(place: String?, success: @escaping ()->(), error: @escaping (String)->()) {
        if let local = place {
            self.request.locExame = local
        }
        if let image1 = photo1.value {
            self.request.fotoGuiaMedico = UIImageJPEGRepresentation(image1, 0.05)?.base64EncodedString()
        }
        
        if let image2 = photo2.value {
            self.request.fotoGuiaMedico2 = UIImageJPEGRepresentation(image2, 0.05)?.base64EncodedString()
        }
        
        if let image3 = photo3.value {
            self.request.fotoGuiaMedico3 = UIImageJPEGRepresentation(image3, 0.05)?.base64EncodedString()
        }
        
        SCSManager.createAuthorizationGuide(request: request, success: {
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
}
