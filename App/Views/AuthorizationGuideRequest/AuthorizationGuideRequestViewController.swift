//
//  AuthorizationGuideRequestViewController.swift
//  Project
//
//  Created by Victor Catão on 14/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import GooglePlaces

class AuthorizationGuideRequestViewController: UIViewController {
    
    @IBOutlet weak var viewCarteirinha: CustomButtomCombo!
    @IBOutlet weak var viewSolicitacao: CustomButtomCombo!
    @IBOutlet weak var viewData: CustomButtomCombo!
    @IBOutlet weak var viewLocal: CustomEditText!
    
    @IBOutlet weak var photo1ImageView: UIImageView!
    @IBOutlet weak var photo2ImageView: UIImageView!
    @IBOutlet weak var photo3ImageView: UIImageView!
    
    @IBOutlet weak var photo1View: CataoUIView!
    @IBOutlet weak var photo2View: CataoUIView!
    @IBOutlet weak var photo3View: CataoUIView!
    
    // MARK: - Variables
    private let viewModel = AuthorizationGuideRequestViewModel()
    
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.downloadData()
        self.closeKeyboardOnTouch()
        setupAutoScrollWhenKeyboardShowsUp()
    }
    
    // MARK: - Setups
    private func setup() {
        setNavBar()
        setupRx()
        setupTaps()
    }
    
    private func setupTaps() {
        viewCarteirinha.action = didTapCardNumberView
        viewSolicitacao.action = didTapSolicitationTypeView
        viewData.action = didTapDateView
        
        
        // PHOTOS
        self.photo1View.isUserInteractionEnabled = true
        self.photo2View.isUserInteractionEnabled = true
        self.photo3View.isUserInteractionEnabled = true
        
        self.photo1View.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapPhoto1)))
        self.photo2View.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapPhoto2)))
        self.photo3View.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapPhoto3)))
    }
    
    private func setupRx() {
        // LABELS
        self.viewModel.selectedDate.asObservable().subscribe(onNext: {value in
            self.viewData.texto = value ?? "Selecione a data"
        }).disposed(by: self.viewModel.bag)
        
        self.viewModel.selectedCardNumber.asObservable().subscribe(onNext: {value in
            self.viewCarteirinha.texto = value?.descricao ?? "Selecione a carteirinha"
        }).disposed(by: self.viewModel.bag)
        
        self.viewModel.selectedSolicitationType.asObservable().subscribe(onNext: {value in
            self.viewSolicitacao.texto = value?.descricao ?? "Selecione o tipo"
        }).disposed(by: self.viewModel.bag)
        
        
        
        // Photos
        self.viewModel.photo1.asObservable().subscribe(onNext: { photo in
            self.photo1ImageView.image = photo
            self.photo1ImageView.isHidden = photo == nil
        }).disposed(by: self.viewModel.bag)
        
        self.viewModel.photo2.asObservable().subscribe(onNext: { photo in
            self.photo2ImageView.image = photo
            self.photo2ImageView.isHidden = photo == nil
        }).disposed(by: self.viewModel.bag)
        
        self.viewModel.photo3.asObservable().subscribe(onNext: { photo in
            self.photo3ImageView.image = photo
            self.photo3ImageView.isHidden = photo == nil
        }).disposed(by: self.viewModel.bag)
    }
    
    // MARK: - Data
    func downloadData() {
        self.showLoader()
        self.viewModel.fetchInputs(success: {
            self.hideLoader()
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    
    // MARK: - Action
    
    @objc func didTapCardNumberView() -> Void{
        ActionSheetStringPicker.show(withTitle: "Carteirinha", rows: self.viewModel.carteirinhas, initialSelection: 0, doneBlock: { (picker, index, strings) in
            self.viewModel.setCardNumber(atIndex: index)
        }, cancel: { (picker) in
            // do nothing
        }, origin: self.view)
    }
    
    @objc func didTapSolicitationTypeView() -> Void{
        ActionSheetStringPicker.show(withTitle: "Tipo de Solicitação", rows: self.viewModel.soliciatacoes, initialSelection: 0, doneBlock: { (picker, index, strings) in
            self.viewModel.setSoliciationType(atIndex: index)
        }, cancel: { (picker) in
            // do nothing
        }, origin: self.view)
    }
    
    @objc func didTapAddressView() -> Void{
        openPlacesAutocomplete()
    }
    
    @objc func didTapDateView() -> Void{
        ActionSheetDatePicker.show(withTitle: "Data do Exame", datePickerMode: .date, selectedDate: Date(), doneBlock: { (picker, any1, any2) in
            if let date = any1 as? Date {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy"
                let data = dateFormatter.string(from: date)
                self.viewModel.selectDate(data, date: date)
            }
        }, cancel: { (picker) in
            
        }, origin: self.view)
    }
    
    
    private func openPlacesAutocomplete() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
//        let fields = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
//            UInt(GMSPlaceField.placeID.rawValue))!
//        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.country = "BR"
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    
    
    @objc func didTapPhoto1(){
        self.viewModel.currentPhotoSelection = 1
        self.openPhotoSheet(canDelete: self.viewModel.photo1.value != nil)
    }
    
    @objc func didTapPhoto2(){
        self.viewModel.currentPhotoSelection = 2
        self.openPhotoSheet(canDelete: self.viewModel.photo2.value != nil)
    }
    
    @objc func didTapPhoto3(){
        self.viewModel.currentPhotoSelection = 3
        self.openPhotoSheet(canDelete: self.viewModel.photo3.value != nil)
    }
    
    @IBAction func didTapSend(_ sender: Any) {
//        if self.viewModel.isValidFields() {
            self.showLoader()
        self.viewModel.createAuthorizationGuide(place: viewLocal.editText.text,success: {
                self.hideLoader()
                self.showSuccessAlert(message: "Solicitação enviada com sucesso!", okBlock: {
                    self.navigationController?.popViewController(animated: true)
                }, cancelBlock: nil)
            }) { (msg) in
                self.hideLoader()
                self.showErrorAlert(message: msg)
            }
//        } else {
//            self.showErrorAlert(message: "É necessário preencher a carteirinha, o tipo de solicitação e mandar ao menos uma foto")
//        }
    }
}

// MARK: - GMSAutocompleteViewControllerDelegate
extension AuthorizationGuideRequestViewController: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        print("Place name: \(place.name)")
//        print("Place ID: \(place.placeID)")
//        print("Place attributions: \(place.attributions)")
        self.viewModel.setPlace(place: place)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension AuthorizationGuideRequestViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // Camera and Gallery are the options to open, video and image are the media types it will pick.
    // It needs at least one from camera and gallery to be true. The same applies to video and image
    func openPhotoSheet(canDelete: Bool){
        var pickerTitle = "Você esqueceu de passar algum true no parametro"
        var galleryTitle = "Você esqueceu de passar algum true no parametro"
        var mediaTypes = [String]()
        
//        if withImage && withVideo {
//            pickerTitle = "Foto e Vídeo"
//            galleryTitle = "Foto ou Vídeo da galeria"
//            mediaTypes.append("public.movie")
//            mediaTypes.append("public.image")
//        } else if withImage {
            pickerTitle = "Foto"
            galleryTitle = "Foto da galeria"
            mediaTypes.append("public.image")
//        } else if withVideo {
//            pickerTitle = "Vídeo"
//            galleryTitle = "Vídeo da galeria"
//            mediaTypes.append("public.movie")
//        }
        
//        if withCamera && withGallery {
        
            let actionSheet = UIAlertController(title: pickerTitle, message: "O que deseja fazer?", preferredStyle: .actionSheet)
            actionSheet.addAction(UIAlertAction(title: "Tirar foto", style: .default, handler: { (action) in
                self.openCamera()
            }))
            
            actionSheet.addAction(UIAlertAction(title: galleryTitle, style: .default, handler: { (action) in
                self.openPhotoLibrary(with: mediaTypes)
            }))
        
        if canDelete {
            actionSheet.addAction(UIAlertAction(title: "Deletar Imagem", style: .default, handler: { (action) in
                self.viewModel.deletePhoto()
            }))
        }
        
            
            actionSheet.addAction(UIAlertAction(title: "Cancelar", style: .destructive, handler: nil))
            present(actionSheet, animated: true, completion: nil)
//        } else if withCamera {
//            openCamera()
//        } else if withGallery {
//            openPhotoLibrary(with: mediaTypes)
//        }
        
    }
    
    private func openCamera(){
        let picker = UIImagePickerController()
        picker.sourceType = .camera
        picker.allowsEditing = false
        picker.delegate = self
        picker.modalPresentationStyle = .overFullScreen
        present(picker, animated: true, completion: nil)
    }
    
    private func openPhotoLibrary(with mediaTypes: [String]){
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.allowsEditing = false
        picker.mediaTypes = mediaTypes
        picker.delegate = self
        picker.modalPresentationStyle = .overFullScreen
        present(picker, animated: true, completion: nil)
    }
    
    // MARK - ImagePicker Delegate
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        let image = info[UIImagePickerControllerEditedImage] as? UIImage
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.viewModel.setPhoto(image)
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
