//
//  BoletoDetailViewController.swift
//  Project
//
//  Created by Victor Catão on 29/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class BoletoDetailViewController: UIViewController {
    
    // MARK: - init
    convenience required init(boleto: BoletoModel) {
        self.init()
        self.viewModel.boleto = boleto
    }
    
    // MARK: - Outlets
    
    @IBOutlet weak var lblObs: UILabel!
    @IBOutlet weak var viewBarCode: UIView!
    @IBOutlet weak var lblValor: UILabel!
    @IBOutlet weak var lblData: UILabel!
    @IBOutlet weak var lblBarCode: UILabel!
    @IBOutlet weak var btnBoleto: CustomButton!
    @IBOutlet weak var btnCoparticipacao: CustomButton!
    
    // MARK: - Variables
    private let viewModel = BoletoDetailViewModel()
    
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    // MARK: - Setups
    private func setup() {
        setupView()
        setNavBar()
        setupNav()
    }
    
    func setupNav() {
        let sidemenuBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        sidemenuBtn.setImage(UIImage(named: "Share")?.resize(maxWidthHeight: 20), for: .normal)
        sidemenuBtn.addTarget(self, action: #selector(self.didTapShare), for: .touchUpInside)
        sidemenuBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let sideMenuBarBtn = UIBarButtonItem(customView: sidemenuBtn)

        self.navigationItem.setRightBarButtonItems([sideMenuBarBtn], animated: false)
    }
    
    @objc func didTapShare() {
        let shareVC: UIActivityViewController = UIActivityViewController(activityItems: [(lblBarCode.text) as Any], applicationActivities: nil)
        self.present(shareVC, animated: true, completion: nil)
    }
    
    private func setupView() {
        self.lblData.text = self.viewModel.dueDate
        self.lblBarCode.text = self.viewModel.codeNumber
        self.lblValor.text = self.viewModel.price
        self.viewBarCode.isHidden = self.viewModel.codeNumber == nil || self.viewModel.codeNumber == ""
        self.lblObs.text = self.viewModel.status
        if self.viewModel.status == "ABERTO"{
            self.lblObs.textColor = Colors.Warning
        }
        else if self.viewModel.status == "PAGO"{
            self.lblObs.textColor = Colors.Success
        }
        else if self.viewModel.status == "VENCIDO"{
            self.lblObs.textColor = Colors.Error
        }
        else{
            self.lblObs.textColor = Colors.hexStringToUIColor(hex: "#D6D971")
        }
        
        let existe = (self.viewModel.boleto?.existeBoleto?.boolValue ?? false)
        let pago = (self.viewModel.boleto?.estaPago?.boolValue ?? false)
        let linkVazio = (self.viewModel.boleto?.linkBoleto?.isEmpty() ?? true)
        let exibeBoleto = existe && !pago && !linkVazio
        self.btnBoleto.isHidden = !exibeBoleto
        
        let existeCopart = (self.viewModel.boleto?.existeCoparticipacao?.boolValue ?? false)
        let linkCopartVazio = (self.viewModel.boleto?.linkCoparticipacao?.isEmpty ?? true)
        let exibeCopart = existeCopart && !linkCopartVazio
        self.btnCoparticipacao.isHidden = !existeCopart
    }
    
    @IBAction func didTapGenerateButton(_ sender: Any) {
        if let link = self.viewModel.boleto?.linkBoleto, let url = URL(string: link) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            self.showErrorAlert(message: "Não foi possível gerar o boleto")
        }
    }
    
    // MARK: - Actions
    @IBAction func didTapCopy(_ sender: Any) {
        guard let codeNumber = self.viewModel.codeNumber else { return }
        let pasteBoard = UIPasteboard.general
        pasteBoard.string = codeNumber
        
        self.showAlert(title: "", message: "Código copiado com sucesso!", okBlock: {
            
        }, cancelBlock: nil)
    }
    
    @IBAction func gotoCoparticipacao(_ sender: Any) {
        if let link = self.viewModel.boleto?.linkCoparticipacao, let url = URL(string: link) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            self.showErrorAlert(message: "Não foi possível gerar o boleto")
        }
//        self.navigationController?.pushViewController(CoparticipacaoVC(), animated: true)
    }
}
