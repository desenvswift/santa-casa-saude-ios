//
//  BoletoDetailViewModel.swift
//  Project
//
//  Created by Victor Catão on 29/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class BoletoDetailViewModel {
    
    var boleto: BoletoModel?
    
    var month: String? {
        get {
            return boleto?.mes
        }
    }
    
    var codeNumber: String? {
        get {
            return boleto?.linhaDigitavel
        }
    }
    
    var mensagem: String? {
        get {
            return boleto?.mensagemBeneficiario
        }
    }
    
    var status: String? {
        get {
            return boleto?.status
        }
    }
    
    var price: String? {
        get {
//            return boleto?.valor?.currencyInputFormatting()
            return boleto?.valor
        }
    }
    
    var dueDate: String? {
        get {
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "dd/MM/yyyy"
//            let date = dateFormatter.date(from: boleto?.dataVencimento ?? "01/01/0001")
//            dateFormatter.dateFormat = "d MMM"
//            let data = dateFormatter.string(from: date ?? Date())
//            return "Venc. \(data.uppercased().replacingOccurrences(of: ".", with: ""))"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            if let date = dateFormatter.date(from: boleto?.dataVencimento ?? "") {
                dateFormatter.dateFormat = "dd/MM/yyyy"
                let date =  dateFormatter.string(from: date)
                return "Venc. \(date)"
            }
            return ""
        }
    }
    
    
}
