//
//  BeneficioCell.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 15/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class BeneficioCell: UICollectionViewCell {

    @IBOutlet weak var tutorialImageView: UIImageView!
    var link = ""
//    @IBOutlet weak var tutorialTextLabel: UILabel!
    
    func setup(tutorial: BeneficiosResponse) {
        let imagem = tutorial.imagemBase64?.replacingOccurrences(of: "data:image/jpeg;base64,", with: "")
        self.tutorialImageView.setBlobImage(string: tutorial.imagemBase64?.replacingOccurrences(of: "data:image/jpeg;base64,", with: ""))
//        self.tutorialTextLabel.text = tutorial.texto
        
        self.link = tutorial.link ?? ""
        if !self.link.isEmpty{
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            self.tutorialImageView.isUserInteractionEnabled = true
            self.tutorialImageView.addGestureRecognizer(tapGestureRecognizer)

        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        if let url = URL(string: link) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}
