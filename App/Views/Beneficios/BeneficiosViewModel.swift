//
//  BeneficiosViewModel.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 31/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import Foundation
import UIKit

class BeneficiosViewModel {
    
    private var list: [BeneficiosResponse] = []
    
    
    func fetchTutorial(success: @escaping ()->(), error: @escaping (String)->()) {
        SCSManager.getBeneficios(success: { (response) in
            self.list = response.lista ?? []
            success()
        }) { (msg, _) in
            error(msg)
        }
    }
    
    func getNumberOfItems() -> Int {
        print(list.count)
        return list.count
    }
    
    func getTutorial(at index: Int) -> BeneficiosResponse {
        return list[index]
    }
    
}
