//
//  BeneficiosVC.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 15/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class BeneficiosVC: UIViewController {

    let CELL_IDENTIFIER = "BeneficioCell"
    
    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    // MARK: - Variables
    private let viewModel = BeneficiosViewModel()
    var isFromMenu = false
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.downloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    // MARK: - Download
    func downloadData() {
        self.showLoader()
        self.viewModel.fetchTutorial(success: {
            self.hideLoader()
            self.setupPageControl()
            self.collectionView.reloadData()
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    // MARK: - Setups
    private func setup() {
        setupCollectionView()
        setupPageControl()
        //setNavBar(close: true)
    }
    
    func setupCollectionView() {
        self.collectionView.registerNib(named: CELL_IDENTIFIER)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    func setupPageControl() {
        self.pageControl.numberOfPages = self.viewModel.getNumberOfItems()
        self.pageControl.currentPage = 0
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true)
    }
    // MARK: - Actions
    @IBAction func didTapClose(_ sender: Any) {
        if isFromMenu {
            self.dismiss(animated: true, completion: nil)
        } else {
            AppDelegate.setupRootWindow()
        }
    }
}

// MARK: - CollectionView
extension BeneficiosVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.getNumberOfItems()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CELL_IDENTIFIER, for: indexPath) as! BeneficioCell
        cell.setup(tutorial: self.viewModel.getTutorial(at: indexPath.row))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.item
    }
}
