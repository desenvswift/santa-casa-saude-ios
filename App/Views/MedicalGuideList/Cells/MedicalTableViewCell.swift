//
//  MedicalTableViewCell.swift
//  Base
//
//  Created by Victor Catão on 05/05/19.
//  Copyright © 2019 Catao. All rights reserved.
//

import UIKit

class MedicalTableViewCell: UITableViewCell {
    
    let CELL_IDENTIFIER = "QualificationTableViewCell"
    let CELL_QUALIFICATIONS_HEIGHT: CGFloat = 20.0

    @IBOutlet weak var viewArredondar: UIView!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var crmLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var lblCidadeEstado: UILabel!
    @IBOutlet weak var stackQualificacoes: UIStackView!
    @IBOutlet weak var imgFavorito: UIImageView!
    
    var qualifications: [QualificationModel] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewArredondar.layer.cornerRadius = 8
    }
    
    func setup(place: PlaceResponse) {
        self.imgFavorito.image = (place.favorito?.boolValue ?? false) ? UIImage(named: "StarFilled") : UIImage(named: "favorito")
        doctorNameLabel.text = place.prestadorNome
        crmLabel.text = "\(place.especialidadeDsc ?? "") | CRM \(place.prestadorNumero ?? "")"
        addressLabel.text = "\(place.prestadorLogradouro ?? ""), \(place.prestadorNumero ?? "") - \(place.prestadorBairro ?? "")"
        lblCidadeEstado.text = "\(place.cidadeDsc ?? "") - \(place.prestadorUF ?? "")"
        
//        distanceViews.forEach({$0.isHidden = (place.prestadorDistancia == nil || place.prestadorDistancia?.intValue == 0 )})
        distanceLabel.text = String.init(format: "%.2f", place.prestadorDistancia?.floatValue ?? 0.0)
        
        self.qualifications = place.prestadorQualificacoes ?? []
        stackQualificacoes.isHidden = self.qualifications.isEmpty
    }
    
}
