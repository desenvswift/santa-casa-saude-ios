//
//  MedicalGuideListViewController.swift
//  Project
//
//  Created by Victor Catão on 05/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class MedicalGuideListViewController: UIViewController {
    
    convenience required init(places: [PlaceResponse], request: MedicalGuideRequest) {
        self.init()
        self.viewModel.places = places
        self.viewModel.request = request
    }
    
    let CELL_IDENTIFIER = "MedicalTableViewCell"
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewTitulo: CustomViewTituloGrande!
    
    // MARK: - Variables
    private let viewModel = MedicalGuideListViewModel()
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    // MARK: - Setups
    private func setup() {
        setNavBar()
        setupTableView()
        setupView()
        setupNavButtons()
    }
    
    func setupNavButtons() {
        let sidemenuBtn1 = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        sidemenuBtn1.setImage(UIImage(named: "Filter")?.resize(maxWidthHeight: 20), for: .normal)
        sidemenuBtn1.addTarget(self, action: #selector(self.didTapFilter), for: .touchUpInside)
        sidemenuBtn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let sideMenuBarBtn1 = UIBarButtonItem(customView: sidemenuBtn1)
        
        let sidemenuBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        sidemenuBtn.setImage(UIImage(named: "Map")?.resize(maxWidthHeight: 20), for: .normal)
        sidemenuBtn.addTarget(self, action: #selector(self.didTapMap), for: .touchUpInside)
        sidemenuBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let sideMenuBarBtn = UIBarButtonItem(customView: sidemenuBtn)

        self.navigationItem.setRightBarButtonItems([sideMenuBarBtn, sideMenuBarBtn1], animated: false)
    }
    
    func setupView() {
        self.viewTitulo.subTitulo = "Pronto! Encontramos \(self.viewModel.getNumberOfRows()) resultados"
    }
    
    func setupTableView() {
        self.tableView.registerNib(named: CELL_IDENTIFIER)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    // MARK: - Actions
    @objc func didTapFilter(_ sender: Any) {
//        let modal = FiltersModalViewController(request: self.viewModel.request)
//        self.showModal(viewController: modal)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func didTapMap() {
        let places = self.viewModel.getPlacesForMap()
        self.navigationController?.pushViewController(MapViewController(places: places), animated: true)
    }
    
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension MedicalGuideListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as! MedicalTableViewCell
        cell.setup(place: self.viewModel.getPlace(at: indexPath.row))
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(MedicalDetailViewController(place: self.viewModel.getPlace(at: indexPath.row)), animated: true)
    }
}
