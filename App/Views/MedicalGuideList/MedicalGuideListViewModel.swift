//
//  MedicalGuideListViewModel.swift
//  Project
//
//  Created by Victor Catão on 05/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class MedicalGuideListViewModel {
    
    var places: [PlaceResponse] = []
    var request: MedicalGuideRequest!
    
    func getNumberOfRows() -> Int { return places.count }
    
    func getPlace(at index: Int) -> PlaceResponse {
        return places[index]
    }
    
    func getPlacesForMap() -> [PlaceResponse] {
        return self.places.filter({$0.prestadorLatitude != nil && $0.prestadorLongitude != nil && $0.prestadorLatitude?.doubleValue != 0 && $0.prestadorLongitude?.doubleValue != 0})
    }
    
}
