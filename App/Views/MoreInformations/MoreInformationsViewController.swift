//
//  MoreInformationsViewController.swift
//  Project
//
//  Created by Victor Catão on 24/02/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class MoreInformationsViewController: UIViewController {
    
    
    @IBOutlet weak var lblNome: UILabel!
    @IBOutlet weak var lblPlano: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    
    // MARK: - Variables
    private let viewModel = MoreInformationsViewModel()
    let gradientLayer = CAGradientLayer()
    
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    // MARK: - Setups
    private func setup() {
        setupView()
        setupTabBarCollor()
        verificarBackgroud()
    }
    
    func verificarBackgroud(){
        if #available(iOS 12.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                self.view.backgroundColor = .black
                if self.view.layer.sublayers?[0] == gradientLayer{
                    self.view.layer.sublayers?[0].removeFromSuperlayer()
                }
            } else {
                setGradientBackground()
            }
        } else {
            setGradientBackground()
        }
    }


    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        verificarBackgroud()
    }
    
    func setGradientBackground() {
        let colorTop = UIColor(red: 0, green: 0.686, blue: 0.796, alpha: 1).cgColor
        let colorBottom = UIColor(red: 0.137, green: 0.376, blue: 0.659, alpha: 1).cgColor
                    
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width + 20, height: 2000)

        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
                
        self.view.layer.insertSublayer(gradientLayer, at:0)
    }
    
    func setupView() {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
//        let build = dictionary["CFBundleVersion"] as! String
        self.versionLabel.text = "Versão: \(version)"
        
//        if !isUserLogged() {
//            for v in [userInfosView, favoritesView, preferencesView, guideAuthorizationView] {
////                for v in [userInfosView, guideAuthorizationView, favoritesView, preferencesView] {
//                v?.isHidden = true
//            }
//        } else {
            self.lblPlano.text = UserSingleton.shared.user?.beneficiario?.descricaoPlano
            self.lblNome.text = UserSingleton.shared.user?.beneficiario?.nome
//        }
    }
    
    // MARK: - Taps
    @objc func didTapAgendamentoConsultaExame() {
        guard let url = URL(string: "https://app-1-18.agenda.globalhealth.mv/agendar/?key=scsjc") else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }

    @objc func didTapcontactView() {
        self.navigationController?.pushViewController(ContactViewController(), animated: true)
    }
    
    @objc func didTapuserInfosView() {
        self.navigationController?.pushViewController(ProfileViewController(), animated: true)
    }
    
    @objc func didTapguideAuthorizationView() {
        self.navigationController?.pushViewController(AuthorizationGuideViewController(), animated: true)
    }
    
    @objc func didTapfavoritesView() {
        self.navigationController?.pushViewController(FavoritesViewController(), animated: true)
    }
    
    @objc func didTappreferencesView() {
        let vc = PreferencesViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func didTaptalkWithUsView() {
        if isUserLogged() {
            showLoader()
            getLink(forLocation: .PerfilMeajude, success: { link in
                self.hideLoader()
                if let url = URL(string: link ?? "") {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
                else{
                    self.showErrorAlert(message: "Erro ao carregar URL")
                }
            }) {msg in
                self.hideLoader()
                self.showErrorAlert(message: msg)
            }
        } else {
            if let url = URL(string: "https://www.santacasasaude-planos.com/") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    @objc func didTaptutorialView() {
        let vc = TutorialViewController()
        vc.isFromMenu = true
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func didTaplinksView() {
//        self.openViewController(LinksViewController())
    }
    
    @objc func didTaptermsView() {
        showLoader()
        getLink(forLocation: .TermoPerfil, success: { link in
            self.hideLoader()
            if let url = URL(string: link ?? "") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else{
                self.showErrorAlert(message: "Erro ao carregar URL")
            }
        }) {msg in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    @objc func didTapexitView() {
        self.showAlert(title: "Atenção", message: "Tem certeza que deseja sair?", okBlock: {
            UserDefaults.standard.set(nil, forKey: KEY_USER_TOKEN)
            UserDefaults.standard.set(nil, forKey: KEY_USER_INFOS)
            UserSingleton.shared.invalidateUser()
            AppDelegate.setupRootWindow()
        }) {
            
        }
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func gotoDados(_ sender: Any) {
        self.navigationController?.pushViewController(ProfileViewController(), animated: true)
    }
    
    @IBAction func gotoSenha(_ sender: Any) {
        let vc = UpdateUserInfoViewController()
        vc.viewType = .password
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func gotoFavoritos(_ sender: Any) {
        self.navigationController?.pushViewController(FavoritesViewController(), animated: true)
    }
    
    @IBAction func gotoPreferencias(_ sender: Any) {
        let vc = PreferencesViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func gotoContatos(_ sender: Any) {
        self.navigationController?.pushViewController(ContactViewController(), animated: true)
    }
    
    @IBAction func gotoHelp(_ sender: Any) {
        showLoader()
        getLink(forLocation: .PerfilMeajude, success: { link in
            self.hideLoader()
            if let url = URL(string: link ?? "") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else{
                self.showErrorAlert(message: "Erro ao carregar URL")
            }
        }) {msg in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    @IBAction func gotoTermos(_ sender: Any) {
        showLoader()
        getLink(forLocation: .TermoPerfil, success: { link in
            self.hideLoader()
            if let url = URL(string: link ?? "") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else{
                self.showErrorAlert(message: "Erro ao carregar URL")
            }
        }) {msg in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    @IBAction func gotoLinks(_ sender: Any) {
        let vc = LinksViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func gotoExcluir(_ sender: Any) {
        let vc = DeleteUserVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func sair(_ sender: Any) {
        self.showModalGeneric(titulo: "Atenção", desc: "Tem certeza que deseja sair?", acaoConfirmar: {
            UserDefaults.standard.set(nil, forKey: KEY_USER_TOKEN)
            UserDefaults.standard.set(nil, forKey: KEY_USER_INFOS)
            UserSingleton.shared.invalidateUser()
            AppDelegate.setupRootWindow()
        }, acaoCancelar: {})
    }
    
}
