//
//  HomeCell.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 12/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class HomeCell: UICollectionViewCell {

    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    
    @IBOutlet weak var imgWidth: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(model: homeCellModel){
        self.img.image = UIImage(named: model.imagemNome ?? "")
        self.lbl.text = model.titulo
        
        
        //gambiarra para icone de beneficios ficar do mesmo tamanho dos outros
        if model.tipoProduto == .beneficios{
            imgWidth.constant = 75
        }
        else{
            imgWidth.constant = 55
        }
    }

}

class homeCellModel{
    var tipoProduto: produtoTipo?
    var titulo: String?
    var imagemNome: String?
    
    init(tipoProduto: produtoTipo, titulo: String, imagemNome: String) {
        self.tipoProduto = tipoProduto
        self.titulo = titulo
        self.imagemNome = imagemNome
    }
}
