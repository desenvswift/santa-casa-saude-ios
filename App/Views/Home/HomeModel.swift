//
//  HomeModel.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 12/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import Foundation


class HomeModel{
    
    
    var imgs = [homeCellModel(tipoProduto: .carteirinha, titulo: "Carteirinha", imagemNome: "Card"),
                homeCellModel(tipoProduto: .financeiro, titulo: "Financeiro", imagemNome: "Money"),
                homeCellModel(tipoProduto: .autorizacao, titulo: "Autorização de Guia", imagemNome: "Hand"),
                homeCellModel(tipoProduto: .guiaMedico, titulo: "Guia Médico", imagemNome: "Doctor"),
                homeCellModel(tipoProduto: .agendamento, titulo: "Agendamento \nRede Saúde Santa Casa", imagemNome: "Calendar"),
                homeCellModel(tipoProduto: .telemedicina, titulo: "Acessar Telemedicina", imagemNome: "Video"),
                homeCellModel(tipoProduto: .chat, titulo: "Fale Conosco", imagemNome: "Chat"),
                homeCellModel(tipoProduto: .beneficios, titulo: "Meus Benefícios", imagemNome: "Benefits")]
    
    var notificacoes: HomeResponse?
    
    func getNotificacoes(success: @escaping()->(), error: @escaping(String)->()) {
        SCSManager.getHome(success: { (response) in
            self.notificacoes = response
            HomeResponse.notificacoes = self.notificacoes?.painelNotificacoes
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func fetchHome(success: @escaping ()->(), error: @escaping (String)->()) {
        SCSManager.verifyAccess(success: {
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func verifyTicketAccess() -> Bool{
        return (UserSingleton.shared.user?.beneficiario?.opcao_boleto?.boolValue) ?? false
    }
}
