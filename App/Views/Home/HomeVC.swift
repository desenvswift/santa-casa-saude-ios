//
//  HomeVC.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 11/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

enum produtoTipo{
    case carteirinha
    case financeiro
    case autorizacao
    case guiaMedico
    case agendamento
    case telemedicina
    case chat
    case beneficios
}

class HomeVC: UIViewController {

    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var btnNotificacao: UIButton!
    @IBOutlet weak var lblNome: UILabel!
    @IBOutlet weak var lblPlano: UILabel!
    
    private let model = HomeModel()
    var gotoNotification = false
    
    
    let gradientLayer = CAGradientLayer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //btnNotificacao.putBadge(count: 3)
        verificarBackgroud()
        setupCollection()
        setupLabel()
        if gotoNotification { self.abrirTelaSeLogado(tela: NotificacaoVC()) }
        getNotificacoes()
    }
    
    func getNotificacoes(){
        self.showLoader()
        model.getNotificacoes(success: {
            self.showModalNotification(notficacao: HomeResponse.verifyNotificationFor(location: .home))
            self.hideLoader()
        }, error: { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        })
    }
    
    func fetchData(){
        self.showLoader()
        model.fetchHome(success: {
            self.hideLoader()
        }, error: { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        })
    }
    
    func setupLabel(){
        let nome = UserSingleton.shared.user?.beneficiario?.nome
        let primeiroNome = nome?.split(separator: " ")[0]
//        self.lblPlano.text = UserSingleton.shared.user?.beneficiario?.descricaoPlano
        self.lblNome.text = "Olá, \(primeiroNome ?? "")"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchData()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setupCollection(){
        collection.delegate = self
        collection.dataSource = self
        self.collection.registerNib(named: "HomeCell")
    }
    
    func verificarBackgroud(){
        if #available(iOS 12.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                self.view.backgroundColor = .black
                if self.view.layer.sublayers?[0] == gradientLayer{
                    self.view.layer.sublayers?[0].removeFromSuperlayer()
                }
            } else {
                setGradientBackground()
            }
        } else {
            setGradientBackground()
        }
    }


    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        verificarBackgroud()
    }
    
    func setGradientBackground() {
        let colorTop = UIColor(red: 0, green: 0.686, blue: 0.796, alpha: 1).cgColor
        let colorBottom = UIColor(red: 0.137, green: 0.376, blue: 0.659, alpha: 1).cgColor
                    
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width + 30, height: 1000)

        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
                
        self.view.layer.insertSublayer(gradientLayer, at:0)
    }

    @IBAction func openPerfil(_ sender: Any) {
        self.abrirTelaSeLogado(tela: MoreInformationsViewController())
    }
    
    @IBAction func changePlan(_ sender: Any) {
    }
    
    @IBAction func openNotificacao(_ sender: Any) {
        self.abrirTelaSeLogado(tela: NotificacaoVC())
//        self.openVC(tela: MainTabBarViewController())
    }
}


extension HomeVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        flowayout?.minimumInteritemSpacing = 10
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let size:CGFloat = (collection.frame.size.width - space - 10) / 2.0
        return CGSize(width: size, height: size * 0.9)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.model.imgs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath) as? HomeCell{
            cell.setup(model: self.model.imgs[indexPath.row])
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func didTaptalkWithUsView() {
        if isUserLogged() {
            gotoLink(forLocation: .MnFaleconosco)
        } else {
            if let url = URL(string: "https://www.santacasasaude-planos.com/") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    func didTapAgendar(){
        if isUserLogged(){
            gotoLink(forLocation: .MnAgendamento)
        }
        else{
            self.showModalGeneric(titulo: "Usuário não logado", desc: "Faça login no app para acessar essa e outras funcionalidades ;)", exibirConfirmar: true, exibirCancelar: false, acaoConfirmar: {
                AppDelegate.setupRootWindow()
            }, acaoCancelar: {})
        }
    }
    
    func gotoLink(forLocation: WebViewCodes){
        showLoader()
        getLink(forLocation: forLocation, success: { link in
            self.hideLoader()
            if let url = URL(string: link ?? "") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else{
                self.showErrorAlert(message: "Erro ao carregar URL")
            }
        }) {msg in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let id = self.model.imgs[indexPath.row].tipoProduto
        switch id{
        case .carteirinha:
            self.abrirTelaSeLogado(tela: HealthInsuranceViewController())
            break
        case .financeiro:
            self.abrirTelaSeLogado(tela: FinanceiroVC())
            break
        case .autorizacao:
            self.abrirTelaSeLogado(tela: AuthorizationGuideOptionsVC())
            break
        case .guiaMedico:
            self.openVC(tela: MedicalGuideFilterViewController())
            break
        case .agendamento:
            self.didTapAgendar()
            break
        case .telemedicina:
            self.abrirTelaSeLogado(tela: TelemedicinaVC())
            break
        case .chat:
            self.didTaptalkWithUsView()
        case .beneficios:
            self.openVC(tela: BeneficiosVC())
            break
        default:
            break
        }
        
    }
    
    func abrirTelaSeLogado(tela: UIViewController){
        if isUserLogged(){
            self.openVC(tela: tela)
        }
        else{
            self.showModalGeneric(titulo: "Usuário não logado", desc: "Faça login no app para acessar essa e outras funcionalidades ;)", exibirConfirmar: true, exibirCancelar: false, acaoConfirmar: {
                AppDelegate.setupRootWindow()
            }, acaoCancelar: {})
        }
    }
    
    func abrirFinanceiro(){
        if isUserLogged(){
            if model.verifyTicketAccess(){
                self.openVC(tela: FinanceiroVC())
            }
            else{
                self.showModalGeneric(titulo: "Financeiro", desc: "Você não tem acesso a essa funcionalidade", exibirConfirmar: true, exibirCancelar: false, acaoConfirmar: {
                    AppDelegate.setupRootWindow()
                }, acaoCancelar: {})
            }
        }
        else{
            self.showModalGeneric(titulo: "Usuário não logado", desc: "Faça login no app para acessar essa e outras funcionalidades ;)", exibirConfirmar: true, exibirCancelar: false, acaoConfirmar: {
                AppDelegate.setupRootWindow()
            }, acaoCancelar: {})
        }
    }
    
    func openVC(tela: UIViewController){
        let nav = UINavigationController(rootViewController: tela)
        nav.modalPresentationStyle = .fullScreen
        self.navigationController?.present(nav, animated: true)
    }

}
