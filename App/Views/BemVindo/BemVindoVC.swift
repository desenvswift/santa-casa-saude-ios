//
//  BemVindoVC.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 09/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class BemVindoVC: UIViewController {

    @IBOutlet weak var lblEntrar: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(login))
        lblEntrar.isUserInteractionEnabled = true
        lblEntrar.addGestureRecognizer(tap)
    }

    @objc func login(sender:UITapGestureRecognizer) {
        let vc = LoginViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }

    @IBAction func goToLogin(_ sender: Any) {
        let vc = LoginViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func gotoCadatro(_ sender: Any) {
        let vc = Step1RegisterViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
