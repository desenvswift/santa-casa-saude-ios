//
//  RegisterDataViewController.swift
//  Project
//
//  Created by Victor Catão on 04/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class RegisterDataViewController: UIViewController {
    
    // MARK: - init
    convenience required init(request: SignUpSACRequest){
        self.init()
        self.viewModel.request = request
    }
    
    // MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var viewNome: CustomEditText!
    @IBOutlet weak var viewCpf: CustomEditText!
    @IBOutlet weak var viewEmail: CustomEditText!
    @IBOutlet weak var viewMae: CustomEditText!
    @IBOutlet weak var viewNascimento: CustomEditText!
    @IBOutlet weak var viewTelefone: CustomEditText!
    @IBOutlet weak var viewCodigo: CustomEditText!
    
    
    // MARK: - Variables
    private let viewModel = RegisterDataViewModel()
    
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    // MARK: - Setups
    private func setup() {
        setNavBar()
        setupAutoScrollWhenKeyboardShowsUp()
        setupTextFields()
    }
    
    func setupTextFields() {
        self.viewNome.configure(textFieldType: .NAME)
        self.viewCpf.configure(textFieldType: .CPF)
        self.viewEmail.configure(textFieldType: .EMAIL)
        self.viewMae.configure(textFieldType: .NAME)
        self.viewNascimento.configure(textFieldType: .BIRTHDAY)
        self.viewTelefone.configure(textFieldType: .PHONE)
        self.viewCodigo.validateBlock = {
            return (self.viewCodigo.getTexto()?.count ?? 0) > 10
        }
        self.viewCodigo.editText.keyboardType = .numberPad
        
        self.viewCpf.texto = self.viewModel.request.cpf ?? ""
        self.viewNascimento.texto = self.viewModel.request.dtaNascimento ?? ""
    }
    
    // MARK: - ScrollView
    override func setScrollViewContentInset(_ inset: UIEdgeInsets) {
        self.scrollView.contentInset = inset
    }
    
    // MARK: - Actions
    @IBAction func didTapSend(_ sender: Any) {
        let textFieldsToValidate = [viewNome, viewCpf, viewNascimento, viewEmail, viewMae, viewTelefone, viewCodigo]
        let invalid: Bool = (textFieldsToValidate.first(where: {$0?.isValid() == false }) != nil)
        // make request
        if invalid {
            textFieldsToValidate.forEach({$0?.validate()})
            self.showErrorAlert(message: "Preencha os campos corretamente")
        } else {
            
            self.viewModel.request.nome = self.viewNome.getTexto()
            self.viewModel.request.cpf = self.viewCpf.getTexto()
            self.viewModel.request.email = self.viewEmail.getTexto()
            self.viewModel.request.nomeMae = self.viewMae.getTexto()
            self.viewModel.request.dtaNascimento = getDate(stringBRFormat: self.viewNascimento.getTexto() ?? "")
            self.viewModel.request.ddd = self.viewTelefone.getDDD()
            self.viewModel.request.celular = self.viewTelefone.getPhone()
            self.viewModel.request.numCarteirinha = self.viewCodigo.getTexto()
            
            self.showLoader()
            self.viewModel.signUpSAC(success: { response in
                self.hideLoader()
                let status = response.getStatus()
                let msg = response.getMsgExterna()
                
                if status == 1 {
                    self.showSuccessAlert(message: msg ?? "Obrigado! O seu cadastro será validado por nossa equipe. Entraremos em contato em breve.", okBlock: {
                        self.navigationController?.popToRootViewController(animated: true)
                    }, cancelBlock: nil)
                } else {
                    self.showErrorAlert(message: msg ?? ERROR_SOMETHING_WENT_WRONG)
                }
            }) { (msg) in
                self.hideLoader()
                self.showErrorAlert(message: msg)
            }
        }
    }
    
}
