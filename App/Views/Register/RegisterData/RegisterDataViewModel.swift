//
//  RegisterDataViewModel.swift
//  Project
//
//  Created by Victor Catão on 04/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit
import FirebaseMessaging

class RegisterDataViewModel {
    
    var request = SignUpSACRequest()
    
    func signUpSAC(success: @escaping (CataoModel)->(), error: @escaping (String)->()) {
        request.idDispositivo = Messaging.messaging().fcmToken ?? UIDevice.current.identifierForVendor?.uuidString
        SCSManager.signUpSAC(request: request, success: { response in
            success(response)
        }) { (msg, status) in
            error(msg)
        }
    }
    
}
