//
//  Step2RegisterViewModel.swift
//  Project
//
//  Created by Victor Catão on 04/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class Step2RegisterViewModel {
    
    var request: SignUpRequest?
    
    func signUp(success: @escaping (UserResponse)->(), error: @escaping (String)->()) {
        SCSManager.signUp(request: request!, success: { (user) in
            success(user)
        }) { (msg, status) in
            error(msg)
        }
    }
}
