//
//  Step2RegisterViewController.swift
//  Project
//
//  Created by Victor Catão on 04/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class Step2RegisterViewController: UIViewController {
    
    convenience required init(request: SignUpRequest) {
        self.init()
        self.viewModel.request = request
    }
    
    // MARK: - Outlet
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewNome: CustomEditText!
    @IBOutlet weak var viewTelefone: CustomEditText!
    @IBOutlet weak var viewEmai: CustomEditText!
    @IBOutlet weak var viewConfirmEmail: CustomEditText!
    
    
    // MARK: - Variables
    private let viewModel = Step2RegisterViewModel()
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    // MARK: - Setups
    private func setup() {
        setNavBar()
        setupTextFields()
        setupAutoScrollWhenKeyboardShowsUp()
    }
    
    func setupTextFields() {
        self.viewNome.configure(textFieldType: .NAME)
        self.viewTelefone.configure(textFieldType: .PHONE)
        self.viewEmai.configure(textFieldType: .EMAIL)
        self.viewConfirmEmail.configure(textFieldType: .EMAIL)
        
        self.viewNome.texto = self.viewModel.request?.nome ?? ""
        self.viewNome.editText.isUserInteractionEnabled = false
    }
    
    // MARK: - ScrollView
    override func setScrollViewContentInset(_ inset: UIEdgeInsets) {
        self.scrollView.contentInset = inset
    }
    
    // MARK: - Actions
    @IBAction func didTapNext(_ sender: Any) {
        let textFieldsToValidate = [viewNome, viewTelefone, viewEmai, viewConfirmEmail]
        let invalid: Bool = (textFieldsToValidate.first(where: {$0?.isValid() == false }) != nil) && viewEmai.getTexto() == viewConfirmEmail.getTexto()
        
        if invalid {
            textFieldsToValidate.forEach({$0?.validate()})
            self.showErrorAlert(message: "Preencha os campos corretamente")
        } else {
            self.viewModel.request?.etapa = NSNumber(value: 2)
            self.viewModel.request?.nome = self.viewNome.getTexto()
            self.viewModel.request?.ddd = self.viewTelefone.getDDD()
            self.viewModel.request?.celular = self.viewTelefone.getPhone()
            self.viewModel.request?.email = self.viewEmai.getTexto()
            self.viewModel.request?.emailConf = self.viewConfirmEmail.getTexto()
            
            self.viewModel.signUp(success: { response in
                let status = response.getStatus()
                let msg = response.getMsgExterna()
                
                if status == 1 {
                    self.navigationController?.pushViewController(Step3RegisterViewController(request: self.viewModel.request!), animated: true)
                }
                else {
                    self.showErrorAlert(message: msg ?? "Ocorreu algum erro. Tente novamente.")
                }
            }) { (msg) in
                self.showErrorAlert(message: msg)
            }
            
        }
    }
    
}
