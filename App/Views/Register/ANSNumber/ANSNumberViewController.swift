//
//  ANSNumberViewController.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 01/06/20.
//  Copyright © 2020 Mobile2You. All rights reserved.
//

import UIKit

class ANSNumberViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var viewAns: CustomEditText!
    private var request: SignUpSACRequest?
    
    // MARK: - init
    convenience required init(request: SignUpSACRequest){
        self.init()
        self.request = request
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
//        setupView()
        setNavBar()
        setupTextField()
        setupAutoScrollWhenKeyboardShowsUp()
    }
    
    // MARK: - Setups
    
    func setupTextField() {
        viewAns.configure(textFieldType: .NUMERIC)
        viewAns.maxLength = 6
    }
    
    func setupView() {
        contentView.isUserInteractionEnabled = true
        contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapContentView)))
    }
    
    // MARK: - Actions
    @IBAction func didTapNext(_ sender: Any) {
        if viewAns.getTexto() == nil || viewAns.getTexto()?.isEmpty == true {
            
            self.showErrorAlert(message: "Por favor, preencha o número ANS.")
        
        } else if viewAns.getTexto() != "419249" {
            
            self.showErrorAlert(message: "Seu Plano de Saúde não é o Plano Santa Casa Saúde de São José dos Campos. Obrigado!",
                                okBlock: {
                                    self.navigationController?.popToRootViewController(animated: true)
            }, cancelBlock: nil)
            
        } else {
            // ok
            self.navigationController?.pushViewController(RegisterDataViewController(request: request!), animated: true)
        }
    }
    
    @objc func didTapContentView() {
        self.view.endEditing(true)
    }
    
}
