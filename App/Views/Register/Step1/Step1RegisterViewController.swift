//
//  Step1RegisterViewController.swift
//  Project
//
//  Created by Victor Catão on 04/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class Step1RegisterViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var ciewCpf: CustomEditText!
    @IBOutlet weak var viewNascimento: CustomEditText!
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var checkboxButton: CheckboxButton!
    
    // MARK: - Variables
    private let viewModel = Step1RegisterViewModel()
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    // MARK: - Setups
    private func setup() {
        setNavBar()
        setupTermsLabel()
        setupTextFields()
        closeKeyboardOnTouch()
//        setupAutoScrollWhenKeyboardShowsUp()
    }
    
    func setupTermsLabel() {
        termsLabel.isUserInteractionEnabled = true
//        termsLabel.colorString(text: "Declaro que li e aceito os termos de uso.", coloredText: "termos de uso.", isColoredTextBold: true, color: Colors.MAIN_COLOR, colorBase: .darkGray)
        termsLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapTerms)))
    }
    
    func setupTextFields() {
        self.ciewCpf.configure(textFieldType: .CPF)
        self.viewNascimento.configure(textFieldType: .BIRTHDAY)
    }
    
    // MARK: - ScrollView
    override func setScrollViewContentInset(_ inset: UIEdgeInsets) {
        self.scrollView.contentInset = inset
    }
    
    // MARK: - Actions
    
    @objc func didTapTerms() {
        showLoader()
        getLink(forLocation: .TermoCadastro, success: { link in
            self.hideLoader()
            if let url = URL(string: link ?? "") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else{
                self.showErrorAlert(message: "Erro ao carregar URL")
            }
        }) {msg in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    @IBAction func didTapNext(_ sender: Any) {
        
        if(checkboxButton.isSelected == false) {
            self.showErrorAlert(message: "É necessário aceitar os Termos para prosseguir")
            return
        }
        
        let textFieldsToValidate = [ciewCpf, viewNascimento]
        let invalid: Bool = (textFieldsToValidate.first(where: {$0?.isValid() == false }) != nil)
        
        if invalid {
            textFieldsToValidate.forEach({$0?.validate()})
            self.showErrorAlert(message: "Preencha os campos corretamente")
        } else {
            let request = SignUpRequest()
            request.etapa = NSNumber(value: 1)
            request.aceiteTermo = NSNumber(value: 1)
            request.cpf = self.ciewCpf.editText.text
            request.dtaNascimento = getDate(stringBRFormat: self.viewNascimento.editText.text ?? "")
            
            self.showLoader()
            self.viewModel.signUp(request: request, success: { response in
                self.hideLoader()
                let msg = response.msgExterna
                let status = response.codAcao?.intValue ?? 0
                
                if(status == 1) {
                    request.nome = response.infRet?.nome
                    request.dtaNascimento = getDate(stringBRFormat: self.viewNascimento.editText.text ?? "")
                    self.navigationController?.pushViewController(Step2RegisterViewController(request: request), animated: true)
                }
                else if (status == 8 || status == 9) {
                    self.showModalGeneric(titulo: "Ops!", desc:  msg ?? "Não o encontramos nos nossos registros. Precisamos que nos informe alguns dados.", acaoConfirmar: {
                        let req = SignUpSACRequest()
                        req.cpf = self.ciewCpf.editText.text
                        req.dtaNascimento = self.viewNascimento.editText.text
                        
                        self.navigationController?.pushViewController(ANSNumberViewController(request: req), animated: true)
                    }, acaoCancelar: {})
                }
                
            }) { (error, status) in
                self.hideLoader()
                let msg = error
                if (status == 8 || status == 9) {
                    self.showModalGeneric(titulo: "Ops!", desc:  msg ?? "Não o encontramos nos nossos registros. Precisamos que nos informe alguns dados.", acaoConfirmar: {
                        let req = SignUpSACRequest()
                        req.cpf = self.ciewCpf.editText.text
                        req.dtaNascimento = self.viewNascimento.editText.text
                        
                        self.navigationController?.pushViewController(ANSNumberViewController(request: req), animated: true)
                    }, acaoCancelar: {})
                } else {
                    self.showErrorAlert(message: msg ?? "")
                }
            }
        }
    }
    
}

func getDate(stringBRFormat: String) -> String? {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    dateFormatter.timeZone = TimeZone(identifier: "UTC")
    if let date = dateFormatter.date(from: stringBRFormat) {
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    return nil
}
