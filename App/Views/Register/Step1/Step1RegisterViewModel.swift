//
//  Step1RegisterViewModel.swift
//  Project
//
//  Created by Victor Catão on 04/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class Step1RegisterViewModel {
    
    func signUp(request: SignUpRequest, success: @escaping (UserResponse)->(), error: @escaping (String, Int)->()) {
        SCSManager.signUp(request: request, success: { (user) in
            success(user)
        }) { (msg, status) in
            error(msg, status)
        }
    }
    
    
}
