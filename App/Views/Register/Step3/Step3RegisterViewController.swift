//
//  Step3RegisterViewController.swift
//  Project
//
//  Created by Victor Catão on 04/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class Step3RegisterViewController: UIViewController {
    
    // MARK: - Init
    convenience required init(request: SignUpRequest) {
        self.init()
        self.viewModel.request = request
    }
    
    // MARK: - Outlets
    @IBOutlet weak var viewSenha: CustomEditText!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewConfirmarSenha: CustomEditText!
    // MARK: - Variables
    private let viewModel = Step3RegisterViewModel()
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    // MARK: - Setups
    private func setup() {
        setNavBar()
        setupAutoScrollWhenKeyboardShowsUp()
    }
    
    // MARK: - ScrollView
    override func setScrollViewContentInset(_ inset: UIEdgeInsets) {
        self.scrollView.contentInset = inset
    }
    
    // MARK: - Actions
    @IBAction func didTapNext(_ sender: Any) {
        let textFieldsToValidate = [viewSenha, viewConfirmarSenha]
        let invalid: Bool = (textFieldsToValidate.first(where: {isValidPassword($0?.getTexto()) == false }) != nil)

        if invalid || viewConfirmarSenha.getTexto() != viewSenha.getTexto() {
            self.showErrorAlert(message: "Preencha os campos corretamente")
        } else {
            self.viewModel.request?.etapa = NSNumber(value: 3)
            self.viewModel.request?.senha = self.viewSenha.getTexto()
            self.viewModel.request?.senhaConf = self.viewConfirmarSenha.getTexto()

            self.showLoader()
            self.viewModel.signUp(success: { response in
                self.hideLoader()
                let status = response.getStatus()
                let msg = response.getMsgExterna()

                if status == 1 {
                    self.showAlert(title: "Sucesso", message: msg  ?? "Cadastro efetuado com sucesso", okBlock: {
                        AppDelegate.setupRootWindow()
                    }, cancelBlock: nil)
                } else {
                    self.showErrorAlert(message: msg ?? ERROR_SOMETHING_WENT_WRONG)
                }

            }) { (msg) in
                self.hideLoader()
                self.showErrorAlert(message: msg)
            }
        }
    }
    
    func isValidPassword(_ text: String?) -> Bool {
        return (text != nil) && text!.count >= 6
    }
    
}
