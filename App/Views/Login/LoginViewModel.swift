//
//  LoginViewModel.swift
//  Catao
//
//  Created by Catao on 19/12/2017.
//  Copyright © 2017 Catao. All rights reserved.
//

import UIKit
import FirebaseMessaging
import Firebase

class LoginViewModel {
    
    var user: UserResponse?
    var plans: [Plano] = []
    
    func isValidLogin(textFields: [CataoTextFieldClass]) -> Bool {
        for tf in textFields {
            tf.validate()
            if tf.isValid() == false {
                return false
            }
        }
        return true
    }
    
    func loginRequest(cpf: String?, password: String?, success: @escaping (SignInResponse)->(), error: @escaping (String)->()) {
        guard let cpf = cpf else { return error("CPF Inválido") }
        guard let password = password else { return error("Senha inválida")}
        
        if cpf.isValidCPF() == false {
            return error("CPF Inválido")
        }
        
        if password.count < 4 {
            return error("Senha inválida")
        }
        
        let req = LoginRequest()
        req.login = cpf
        req.senha = password
        InstanceID.instanceID().getID(handler: { (result, err) in
            req.IdDispositivo = Messaging.messaging().fcmToken ?? UIDevice.current.identifierForVendor?.uuidString
            SCSManager.signIn(request: req, success: { (user) in
                success(user)
            }) { (msg, status) in
                error(msg)
            }
        })
    }
    
    func fetchPlans(success: @escaping (PlansResponse)->(), error: @escaping (String)->()) {
        SCSManager.getPlans(success: { (response) in
            self.plans = response.planos ?? []
            success(response)
        }) { (msg, status) in
            error(msg)
        }
    }
    
    
    func selectPlan(success: @escaping (CataoModel)->(), error: @escaping (String)->()) {
        let req = UpdateUserPlanRequest()
        req.codPlano = plans.first?.PlanoId
        SCSManager.updateUserPlan(request: req, success: { (response) in
            success(response)
        }) { (msg, _) in
            error(msg)
        }
    }
    
    func savePlan() {
        UserDefaults.standard.set(plans.first?.PlanoDsc, forKey: KEY_USER_PLAN)
    }
    
}
