//
//  LoginViewController.swift
//  Catao
//
//  Created by Catao on 18/12/2017.
//  Copyright © 2017 Catao. All rights reserved.
//

import UIKit


class LoginViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var viewLogin: CustomEditText!
    @IBOutlet weak var viewSenha: CustomEditText!
    
    
    let viewModel = LoginViewModel()
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
        self.closeKeyboardOnTouch()
        self.setNavBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
//        setupBackgroundNavigation()
    }
    
    // MARK: - Setups
    func setup() {
        setupBack()
        closeKeyboardOnTouch()
        setupTextFields()
    }
    
    func setupTextFields() {
        viewLogin.configure(textFieldType: .CPF, erroMessage: "CPF Inválido")
        viewSenha.configure(textFieldType: .PASSWORD)
    }
    
    
    // MARK:- Taps
    @IBAction func didTapForgotPassword(_ sender: Any) {
        let vc = ForgotPasswordViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapDontHavePassword(_ sender: Any) {
        self.openHome()
//        let vc = Step1RegisterViewController()
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapLoginButton(_ sender: Any) {
        viewLogin.validate()
        self.showLoader()
        self.viewModel.loginRequest(cpf: viewLogin.editText.text, password: viewSenha.editText.text, success: { response in
            self.hideLoader()
            
            let status = response.getStatus()
            let msg = response.getMsgExterna()
            
            if status == 1 {
                self.getPlans()
            } else {
                self.showErrorAlert(message: msg ?? ERROR_SOMETHING_WENT_WRONG)
            }
            
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    @IBAction func didTapNotCustomer(_ sender: Any) {
        openHome()
    }
    
    @objc func didTapTerms() {
        showLoader()
        getLink(forLocation: .TermoCadastro, success: { link in
            self.hideLoader()
            if let url = URL(string: link ?? "") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else{
                self.showErrorAlert(message: "Erro ao carregar URL")
            }
        }) {msg in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    // MARK: - Helpers
    private func openHome() {
        let home = AppDelegate.getHomeViewController()
        UIApplication.shared.windows.first?.rootViewController = home
    }
    
    private func getPlans() {
        self.viewModel.fetchPlans(success: { (plans) in
            if plans.planos?.count == 1 {
                self.viewModel.savePlan()
                self.openHome()
            } else {
                self.openSelectPlan()
            }
        }) { (msg) in
            self.showErrorAlert(message: "Erro ao obter os planos")
        }
    }
    
    private func openSelectPlan() {
        let vc = SelectPlanViewController(plans: self.viewModel.plans)
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
}

extension LoginViewController: SelectPlanViewControllerDelegate {
    func didSelectPlan() {
        openHome()
    }
}

