//
//  IrViewModel.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 16/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import Foundation

class IrModel{
    
    var lista = [IrItem]()
    
    
    func fetchLista(success: @escaping()->(), error: @escaping(String)->()){
        SCSManager.getListaIR(success: { (boletos) in
            self.lista = boletos
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func getItem(at: IndexPath) -> IrItem{
        return self.lista[at.row]
    }
}
