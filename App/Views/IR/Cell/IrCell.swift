//
//  IrCell.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 16/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class IrCell: UITableViewCell {

    @IBOutlet weak var lblAno: UILabel!
    @IBOutlet weak var viewArredondar: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewArredondar.layer.cornerRadius = 8
    }
    
}
