//
//  ImpostoRendaVC.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 16/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class ImpostoRendaVC: UIViewController {
    
    let CELL_IDENTIFIER = "IrCell"
    let viewModel = IrModel()
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    func setup(){
        setNavBar()
        setupTableView()
        downloadData()
    }
    
    private func setupTableView() {
        self.tableView.registerNib(named: CELL_IDENTIFIER)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = UITableViewAutomaticDimension
        self.tableView.separatorColor = .clear

    }
    
    private func downloadData() {
        self.showLoader()
        self.viewModel.fetchLista(success: {
            self.hideLoader()
            self.tableView.reloadData()
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    func open(at index: IndexPath) {
//        let vc = IRDetailVC()
//        vc.link = self.viewModel.getItem(at: index).link ?? ""
//        vc.ano = self.viewModel.getItem(at: index).descricao ?? ""
//        self.navigationController?.pushViewController(vc, animated: true)
        
        if let link = self.viewModel.getItem(at: index).link, let url = URL(string: link) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            self.showErrorAlert(message: "Não foi possível abrir")
        }
    }

}

// MARK: - TableView
extension ImpostoRendaVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.lista.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as! IrCell
        cell.lblAno.text =  self.viewModel.getItem(at: indexPath).descricao
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.open(at: indexPath)
    }
    
}
