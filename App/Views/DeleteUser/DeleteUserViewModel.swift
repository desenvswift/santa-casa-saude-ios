//
//  DeleteUserViewModel.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 07/08/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import Foundation

class DeleteUserViewModel{
    
    func delete(success: @escaping()->(), error: @escaping(String)->()) {
        SCSManager.getHome(success: { (response) in
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
}
