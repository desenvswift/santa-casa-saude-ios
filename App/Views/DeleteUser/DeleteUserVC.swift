//
//  DeleteUserVC.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 07/08/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class DeleteUserVC: UIViewController {

    let model = DeleteUserViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavBar()
    }

    private func delete(){
        self.showLoader()
        model.delete(success: {
            self.hideLoader()
            UserDefaults.standard.set(nil, forKey: KEY_USER_TOKEN)
            UserDefaults.standard.set(nil, forKey: KEY_USER_INFOS)
            UserSingleton.shared.invalidateUser()
            AppDelegate.setupRootWindow()
        }, error: { msg in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        })
    }

    @IBAction func deleteUser(_ sender: Any) {
        self.showModalGeneric(titulo: "Confirmar Exclusão", desc: "Essa ação excluirá o seu cadastro no App, Deseja continuar?", acaoConfirmar: {
            self.delete()
        }, acaoCancelar: {})
    }

}
