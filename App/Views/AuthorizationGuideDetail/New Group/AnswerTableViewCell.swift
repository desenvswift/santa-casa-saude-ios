//
//  AnswerTableViewCell.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 29/06/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import UIKit

class AnswerTableViewCell: UITableViewCell {
    @IBOutlet weak var answerLabel: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setup(answer: String?){
//        self.answerLabel.set(html: answer ?? "")
        self.answerLabel.attributedText = answer?.normalizeHtmlText()
        
        answerLabel.isEditable = false

        answerLabel.dataDetectorTypes = .link
        answerLabel.isScrollEnabled = false
        let htmlData = NSString(string: answer ?? "").data(using: String.Encoding.unicode.rawValue)
        let attributedString = try? NSAttributedString(data: htmlData!, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        answerLabel.attributedText = attributedString
    }
    
}
