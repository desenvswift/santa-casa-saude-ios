//
//  AuthorizationGuideDetailViewController.swift
//  Project
//
//  Created by Victor Catão on 14/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class AuthorizationGuideDetailViewController: UIViewController {
    
    convenience required init(detail: AuthorizationGuideDetailResponse, request: AuthorizationGuide?) {
        self.init()
        self.viewModel.detail = detail
        self.viewModel.requisicao = request
    }
    
    let CELL_IDENTIFIER = "AuthorizationGuideTableViewCell"
    let CELL_ANSWER_IDENTIFIER = "AnswerTableViewCell"
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var lblNome: UILabel!
    @IBOutlet weak var lblProcedimento: UILabel!
    @IBOutlet weak var lblProtocolo: UILabel!
    
    // MARK: - Variables
    private let viewModel = AuthorizationGuideDetailViewModel()
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    // MARK: - Setups
    private func setup() {
        setupView()
        setupTableView()
        setupBack()
    }
    
    private func setupView() {
        setNavBar()
        
        self.messageView.isHidden = !self.viewModel.hasToShowMessageView()
        self.messageLabel.text = self.viewModel.getMessage()
        
        self.lblNome.text = viewModel.getBeneficiario()
        self.lblProtocolo.text = viewModel.getProtocolo()
        self.lblProcedimento.text = viewModel.getProcedimento()
        
    }
    
    private func setupTableView() {
        self.tableView.registerNib(named: CELL_IDENTIFIER)
        self.tableView.registerNib(named: CELL_ANSWER_IDENTIFIER)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
}


// MARK: - TableView
extension AuthorizationGuideDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ANSWER_IDENTIFIER) as! AnswerTableViewCell
            cell.selectionStyle = .none
            cell.setup(answer: self.viewModel.getAnswer())
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as! AuthorizationGuideTableViewCell
        cell.selectionStyle = .none
        cell.setup(auth: self.viewModel.getDetail(at: indexPath.row-1))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            return
        }
        let auth = self.viewModel.getDetail(at: indexPath.row-1)
        guard let link = auth.autorizadoLink else { return }
        guard let url = URL(string: link) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
