//
//  AuthGuideDetailVC.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 31/10/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class AuthGuideDetailVC: UIViewController {

    @IBOutlet weak var lblTransacao: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblEmissao: UILabel!
    @IBOutlet weak var lblGuia: UILabel!
    @IBOutlet weak var lblProtocolo: UILabel!
    @IBOutlet weak var lblMatricula: UILabel!
    @IBOutlet weak var lblProcedimento: UILabel!
    @IBOutlet weak var lblAtendimento: UILabel!
    @IBOutlet weak var lblQtd: UILabel!
    @IBOutlet weak var lblNome: UILabel!
    
    var item: AuthorizationGuideStatusResponse?
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavBar(close: false)
        preencherInfos()
    }
    
    func preencherInfos(){
        if let guia = item{
            lblTransacao.text = guia.NR_TRANSACAO
            lblStatus.text = guia.STATUS_GUIA
            lblEmissao.text = guia.DT_EMISSAO
            lblGuia.text = guia.NR_GUIA
            lblProtocolo.text = guia.NR_PROTOCOLO
            lblMatricula.text = guia.CD_MATRICULA
            lblProcedimento.text = guia.DS_PROCEDIMENTO
            lblAtendimento.text = guia.DS_TIPO_ATENDIMENTO
            lblQtd.text = guia.qtdGuiasFilhas
            lblNome.text = guia.NM_PRESTADOR
        }
    }

}
