//
//  AuthorizationGuideDetailViewModel.swift
//  Project
//
//  Created by Victor Catão on 14/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class AuthorizationGuideDetailViewModel {
    
    var detail: AuthorizationGuideDetailResponse?
    var requisicao: AuthorizationGuide?
    
    func getAnswer() -> String? {
        return detail?.resposta
    }
    
    func getBeneficiario() ->String?{
        return detail?.nomeBeneficiario
    }
    
    func getProtocolo() -> String? {
        return requisicao?.numeroProtocolo
    }
    
    func getProcedimento() ->String?{
        return requisicao?.titulo
    }
    
    func getNumberOfRows() -> Int {
        return (detail?.detalhe?.count ?? 0) + 1
    }
    
    func getDetail(at index: Int) -> AuthDetailResponse {
        return detail?.detalhe?[index] ?? AuthDetailResponse()
    }
    
    func hasToShowMessageView() -> Bool {
        return detail?.mensagemBeneficiario != nil && detail?.mensagemBeneficiario != ""
    }
    
    func getMessage() -> String? {
        return detail?.mensagemBeneficiario
    }
    
}
