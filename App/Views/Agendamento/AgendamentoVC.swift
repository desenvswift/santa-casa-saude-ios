//
//  AgendamentoVC.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 15/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class AgendamentoVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavBar(close: true)
        self.showModalNotification(notficacao: HomeResponse.verifyNotificationFor(location: .agendamento))
    }


    @IBAction func open(_ sender: Any) {
        if let url = URL(string: "https://app-1-18.agenda.globalhealth.mv/agendar/?key=scsjc"){
            UIApplication.shared.open(url)
        }

//        if UIApplication.shared.canOpenURL(other app scheme)
//        {
//             UIApplication.shared.open(other app scheme)
//         }else {
//        }
    }

}
