//
//  SelectTableViewCell.swift
//  Base
//
//  Created by Victor Catão on 05/05/19.
//  Copyright © 2019 Catao. All rights reserved.
//

import UIKit

class SelectTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var checkboxButton: CheckboxButton!
    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var textSearchLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.checkboxButton.isUserInteractionEnabled = false
    }
    
    func setup(model: SearchModel) {
        self.pictureImageView.isHidden = model.picture == nil
        self.checkboxButton.isSelected = model.isSelected
        self.textSearchLabel.text = model.text
    }
    
}
