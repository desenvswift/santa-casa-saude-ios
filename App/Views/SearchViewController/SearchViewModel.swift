//
//  SearchViewModel.swift
//  Project
//
//  Created by Victor Catão on 05/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class SearchViewModel {
    // MARK: - Variables
    var options = [SearchModel]()
    var isMultiSelection = false
    
    // MARK: - TableView helpers
    func getNumberOfRows() -> Int {
        return options.count
    }
    
    func getItem(forIndex index: Int) -> SearchModel {
        return options[index]
    }
    
    func select(atIndex: Int) {
        self.options[atIndex].isSelected = !self.options[atIndex].isSelected
    }
    
}
