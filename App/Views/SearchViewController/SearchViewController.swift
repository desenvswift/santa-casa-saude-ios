//
//  SearchViewController.swift
//  Project
//
//  Created by Victor Catão on 05/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

protocol SearchViewControllerDelegate: class {
    func didSelect(items: [SearchModel])
}

class SearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - init
    convenience required init(options: [SearchModel], multiSelection: Bool) {
        self.init()
        self.viewModel.options = options
        self.viewModel.isMultiSelection = multiSelection
    }
    
    // MARK: - Cell identifier
    let CELL_IDENTIFIER = "SelectTableViewCell"
    
    // MARK: - Variables
    private let viewModel = SearchViewModel()
    weak var delegate: SearchViewControllerDelegate?
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    // MARK: - Setup
    private func setup() {
        title = "Selecionar"
        self.setupNibs()
    }
    
    private func setupNibs() {
        self.tableView.registerNib(named: CELL_IDENTIFIER)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 70))
    }
    
    // MARK: - Taps
    @IBAction func didTapSelect(_ sender: Any) {
        self.delegate?.didSelect(items: self.viewModel.options)
        self.navigationController?.popViewController(animated: true)
    }
    
}

// MARK: - UITableViewDelegate

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.getNumberOfRows()
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER, for: indexPath) as! SelectTableViewCell
        cell.selectionStyle = .none
        cell.setup(model: self.viewModel.getItem(forIndex: indexPath.row))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.select(atIndex: indexPath.row)
        tableView.reloadData()
    }
    
}
