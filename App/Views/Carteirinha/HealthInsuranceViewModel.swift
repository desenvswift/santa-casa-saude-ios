//
//  HealthInsuranceViewModel.swift
//  Project
//
//  Created by Victor Catão on 13/01/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class HealthInsuranceViewModel {
    
    var carteirinhas = HealthInsuranceCardResponse(json:UserDefaults.standard.string(forKey: KEY_USER_CARTEIRINHAS))
    
    func getNumberOfItems() -> Int {
//        return (carteirinhas.listaVerso == nil) ? 0 : carteirinhas.listaVerso!.count + 1
        return (carteirinhas.listaVerso == nil) ? 0 : carteirinhas.listaVerso!.count
    }
    
    func getPicture(index: Int) -> String? {
//        if index == 0 {
//            return carteirinhas.imgFrente
//        }
//        return self.carteirinhas.listaVerso?[index-1].imgVerso
        return self.carteirinhas.listaVerso?[index].imgVerso
    }
    
    func fetchCarteirinhas(success: @escaping()->(), error: @escaping(String)->()) {
        SCSManager.getCarteirinhas(success: { (response) in
            self.carteirinhas = response
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
}
