//
//  HealthInsuranceViewController.swift
//  Project
//
//  Created by Victor Catão on 13/01/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class HealthInsuranceViewController: UIViewController {
    
    let CELL_IDENTIFIER = "ImageCollectionViewCell"
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var offlineView: UIView!
    
    // MARK: - Variables
    private let viewModel = HealthInsuranceViewModel()
    
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
//        setupTabBarCollor()
        setNavBar(close: true)
        self.downloadData()
        self.showModalNotification(notficacao: HomeResponse.verifyNotificationFor(location: .carteirinha))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: - Setups
    private func setup() {
//        self.title = "Carteirinha"
        self.offlineView.isHidden = isUserLogged()
        self.setupCollectionView()
        self.setupNav()
    }
    
    func setupCollectionView() {
        self.collectionView.registerNib(named: CELL_IDENTIFIER)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    func setupNav() {
        let sidemenuBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        sidemenuBtn.setImage(UIImage(named: "Share")?.resize(maxWidthHeight: 20), for: .normal)
        sidemenuBtn.addTarget(self, action: #selector(self.didTapShare), for: .touchUpInside)
        sidemenuBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let sideMenuBarBtn = UIBarButtonItem(customView: sidemenuBtn)

        self.navigationItem.setRightBarButtonItems([sideMenuBarBtn], animated: false)
    }
    
    func downloadData() {
        
        if(APIManager.isConnectedToInternet == false) {
            self.showAlert(title: "Aviso", message: "Você não está conectado à internet.", okBlock: nil, cancelBlock: nil)
        }
        self.showLoader()
        self.viewModel.fetchCarteirinhas(success: {
            self.hideLoader()
            self.reloadViewData()
        }) { (msg) in
            self.hideLoader()
            self.reloadViewData()
        }
    }
    
    func reloadViewData() {
        self.collectionView.reloadData()
        self.pageControl.numberOfPages = self.viewModel.getNumberOfItems()
        self.pageControl.currentPage = 0
        self.pageControl.hidesForSinglePage = true
    }
    
    @objc func didTapShare() {
        if let cell = collectionView.visibleCells.first as? ImageCollectionViewCell, let image: UIImage = cell.pictureImageView.image {
            let shareVC: UIActivityViewController = UIActivityViewController(activityItems: [(image)], applicationActivities: nil)
            self.present(shareVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func didTapLogin(_ sender: Any) {
        UIApplication.shared.windows.first?.rootViewController = LoginViewController()
    }
}

extension HealthInsuranceViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.getNumberOfItems()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CELL_IDENTIFIER, for: indexPath) as! ImageCollectionViewCell
        cell.setup(img: self.viewModel.getPicture(index: indexPath.item), rotate: true)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.item
    }
    
    
}
