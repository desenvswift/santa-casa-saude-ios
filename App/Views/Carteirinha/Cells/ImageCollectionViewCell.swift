//
//  ImageCollectionViewCell.swift
//  Policlin
//
//  Created by Victor Catão on 08/01/19.
//  Copyright © 2019 Victor Catao. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var pictureImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setup(img: String?, rotate: Bool = false) {
//        if (rotate) {
            self.pictureImageView.setBlobImage(string: img)
//            self.pictureImageView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
//        } else {
//            self.pictureImageView.setBlobImage(string: img)
//        }
//        self.pictureImageView.layoutIfNeeded()
//        self.layoutIfNeeded()
    }

}

extension UIImage {
    
    func rotate(_ degree: CGFloat) -> UIImage {
        let radians = CGFloat(degree * .pi) / 180.0 as CGFloat
        let rotatedSize = self.size
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(rotatedSize, false, scale)
        let bitmap = UIGraphicsGetCurrentContext()
        bitmap?.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
        bitmap?.rotate(by: radians)
        bitmap?.scaleBy(x: 1.0, y: -1.0)
        bitmap?.draw(
            self.cgImage!,
            in: CGRect.init(x: -self.size.width / 2, y: -self.size.height / 2 , width: self.size.width, height: self.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext() // this is needed
        return newImage!
    }
}
