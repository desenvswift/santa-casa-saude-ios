//
//  QualificationTableViewCell.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 29/05/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import UIKit

class QualificationTableViewCell: UITableViewCell {

    @IBOutlet weak var checkButton: CheckboxButton!
    @IBOutlet weak var qualificationImageView: UIImageView!
    @IBOutlet weak var qualificationNameLabel: UILabel!
    
    func setup(image: String?, name: String?, hideSelection: Bool, isSelected: Bool = false) {
        
        self.qualificationImageView.setBlobImage(string: image)
        self.qualificationNameLabel.text = name
        
        self.checkButton.isHidden = hideSelection
        self.checkButton.isSelected = isSelected
    }
    
}
