//
//  MedicalGuideFilterViewModel.swift
//  Project
//
//  Created by Victor Catão on 04/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit
import RxSwift

class MedicalGuideFilterViewModel {
    
    var request = MedicalGuideRequest()
    let bag = DisposeBag()
    
    let selectedPlan = Variable<CodDescTypeModel?>(nil)
    let selectedCity = Variable<CodDescTypeModel?>(nil)
    let selectedSpecialtyOrService = Variable<[CodDescTypeModel]?>(nil)
    let isAdvancedFiltersHidden = Variable<Bool>(true)
    let qualifications = Variable<[QualificationModel]>([])
    let selectedBairro = Variable<KeyValueModel?>(nil)
    let selectedTipo = Variable<KeyValueModel?>(nil)
    
    private var plans: [CodDescTypeModel] = []
    private var cities: [CodDescTypeModel] = []
    private var specialtiesServices: [CodDescTypeModel] = []
    private var bairros: [KeyValueModel] = []
    private var tiposEstabelecimento: [KeyValueModel] = []
    
    func setUserLatLng(lat: NSNumber, lng: NSNumber) {
        if UserDefaults.standard.bool(forKey: KEY_PREFERENCE_LOCATION) == false {
            return
        }
        self.request.latitude = lat
        self.request.longitude = lng
    }
    
    func needsToShowLatLngAlert(orderByNearest: Bool) -> Bool {
        if orderByNearest == false { return false }
        return (UserDefaults.standard.bool(forKey: KEY_PREFERENCE_LOCATION) == false) || (UserDefaults.standard.bool(forKey: KEY_PREFERENCE_LOCATION) == true && self.request.latitude == nil)
    }
    
    func getPlans(success: @escaping ([String])->(), error: @escaping (String)->()) {
        if self.plans.count > 0 {
            success(self.plans.map({$0.descricao ?? ""}))
            return
        }
        request.filtro = "P"
        SCSManager.getMedicalGuideFilter(request: request, success: { (response) in
            self.plans = response.lista ?? []
            success(self.plans.map({$0.descricao ?? ""}))
        }) { (msg, status) in
            error(msg)
        }
    }
    
    
    func getCities(success: @escaping ([String])->(), error: @escaping (String)->()) {
        if self.cities.count > 0 {
            success(self.cities.map({$0.descricao ?? ""}))
            return
        }
        request.filtro = "C"
        SCSManager.getMedicalGuideFilter(request: request, success: { (response) in
            self.cities = response.lista ?? []
            success(self.cities.map({$0.descricao ?? ""}))
        }) { (msg, status) in
            error(msg)
        }
    }
    
    
    func getBairros(success: @escaping ([String])->(), error: @escaping (String)->()) {
        if self.bairros.count > 0 {
            success(self.bairros.map({$0.valor ?? ""}))
            return
        }
        request.filtro = "BR"
        SCSManager.getMedicalGuideBairro(request: request, success: { (response) in
            self.bairros = response
            success(self.bairros.map({$0.valor ?? ""}))
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func getTipoEstabelecimento(success: @escaping ([String])->(), error: @escaping (String)->()) {
        if self.tiposEstabelecimento.count > 0 {
            success(self.tiposEstabelecimento.map({$0.valor ?? ""}))
            return
        }
        request.filtro = "TE"
        SCSManager.getMedicalGuideTipoEstabeliecimento(request: request, success: { (response) in
            self.tiposEstabelecimento = response
            success(self.tiposEstabelecimento.map({$0.valor ?? ""}))
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func getSpecialtyServices(success: @escaping ([String])->(), error: @escaping (String)->()) {
        if self.specialtiesServices.count > 0 {
            success(self.specialtiesServices.map({$0.descricao ?? ""}))
            return
        }
        request.filtro = "ES"
        SCSManager.getMedicalGuideFilter(request: request, success: { (response) in
            self.specialtiesServices = response.lista ?? []
            success(self.specialtiesServices.map({$0.descricao ?? ""}))
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func canSelectCity() -> Bool {
        return self.selectedPlan.value != nil
    }
    
    func canSelectSpecialtyServices() -> Bool {
        return self.selectedCity.value != nil
    }
    
    func canSelectBairro() -> Bool {
        return self.selectedCity.value != nil
    }
    
    func canSelectTipoEstabelecimento() -> Bool {
        return self.selectedPlan.value != nil
    }
    
    func selectPlan(index: Int, string: String){
        self.selectedPlan.value = plans[index]
        self.request.planoCod = plans[index].codigo
        self.request.CODIGOPLANO = plans[index].codigo
        
        // reset
        self.cities.removeAll()
        self.selectedCity.value = nil
        self.specialtiesServices.removeAll()
        self.selectedSpecialtyOrService.value = nil
        
        self.request.cidadeCod = nil
        self.request.espServCod = nil
        self.request.espServTip = nil
        self.request.CodCidade = nil
    }
    
    func selectCity(index: Int, string: String){
        self.selectedCity.value = cities[index]
        self.request.cidadeCod = cities[index].codigo
        self.request.CodCidade = cities[index].codigo
        
        self.specialtiesServices.removeAll()
        self.selectedSpecialtyOrService.value = nil
        
        self.request.espServCod = nil
        self.request.espServTip = nil
    }
    
    func selectBairro(index: Int, string: String){
        self.selectedBairro.value = self.bairros[index]
        self.request.bairro = bairros[index].valor
    }
    
    func selectTipo(index: Int, string: String){
        self.selectedTipo.value = self.tiposEstabelecimento[index]
        self.request.tpEstabelecimento = tiposEstabelecimento[index].valor
    }
    
    func selectSpecialtyServices(strings: [String]) {
        var selecteds:[CodDescTypeModel] = []
        for str in strings {
            if let model = specialtiesServices.first(where: {$0.descricao == str}) {
                selecteds.append(model)
            }
        }
        self.selectedSpecialtyOrService.value = selecteds
        self.request.espServCod = selecteds.map({$0.codigo ?? ""}).joined(separator: ";")
        self.request.espServTip = selecteds.map({$0.tipo ?? ""}).joined(separator: ";")
        
        //        self.selectedSpecialtyOrService.value = specialtiesServices[index]
        //        self.request.espServCod = specialtiesServices[index].codigo
        //        self.request.espServTip = specialtiesServices[index].tipo
    }
    
    func fetchAdvancedFilter(error: @escaping (String)->()) {
        if(self.qualifications.value == []) {
            SCSManager.getMedicalGuideQualifications(success: { (response) in
                self.qualifications.value = response.qualificacoes ?? []
            }) { (msg, status) in
                error(msg)
            }
        }
    }
    
    func fetchPlaces(orderByNearest: Bool, success: @escaping ([PlaceResponse])->(), error: @escaping (String)->()) {
        
        if UserDefaults.standard.bool(forKey: KEY_PREFERENCE_LOCATION) == false {
            request.latitude = nil
            request.longitude = nil
        }
        
        request.filtro = nil
        request.qualificacoes = self.qualifications.value.filter({$0.selected}).map({$0.quaDsc ?? ""}).joined(separator: ";")
        
        request.planName = self.selectedPlan.value?.descricao
        request.cityName = self.selectedCity.value?.descricao
        request.specialtyOrServiceName = self.selectedSpecialtyOrService.value?.map({$0.descricao ?? ""}).joined(separator: ", ")
        request.qualifications = qualifications.value.filter({$0.selected})
        
        if !orderByNearest {
            request.latitude = nil
            request.longitude = nil
        }
        
        SCSManager.getMedicalGuide(request: self.request, success: { (response) in
            success(response)
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func setUserLoggedData() {
        let model = CodDescTypeModel()
        model.codigo = UserSingleton.shared.user?.beneficiario?.codigoPlano
        model.descricao = UserSingleton.shared.user?.beneficiario?.descricaoPlano
        self.selectedPlan.value = model
        self.request.planoCod = model.codigo
        self.request.CODIGOPLANO = model.codigo
    }
    
    func clean() {
        if isUserLogged() == false {
            self.selectedPlan.value = nil
        }
        self.selectedCity.value = nil
        self.selectedSpecialtyOrService.value = nil
        self.selectedTipo.value = nil
        self.selectedBairro.value = nil
        self.qualifications.value.forEach({$0.selected = false})
        self.isAdvancedFiltersHidden.value = true
        
        self.request = MedicalGuideRequest()
        
        if isUserLogged() {
            setUserLoggedData()
        }
    }
    
}
