//
//  MedicalGuideFilterViewController.swift
//  Project
//
//  Created by Victor Catão on 04/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import CoreLocation

class MedicalGuideFilterViewController: UIViewController {
    
    let CELL_IDENTIFIER = "QualificationTableViewCell"
    let CELL_QUALIFICATION_HEIGHT: CGFloat = 40
    
    // MARK: - Outlets
    @IBOutlet weak var viewPlano: CustomButtomCombo!
    @IBOutlet weak var viewCidade: CustomButtomCombo!
    @IBOutlet weak var viewEspecialidade: CustomButtomCombo!
    @IBOutlet weak var lblMostrarMais: UILabel!
    
    @IBOutlet weak var stackMaisFiltros: UIStackView!
    @IBOutlet weak var viewQualificacoes: UIView!
    @IBOutlet weak var qualificationsTableView: UITableView!
    @IBOutlet weak var alturaLista: NSLayoutConstraint!
    
    @IBOutlet weak var viewNome: CustomEditText!
    @IBOutlet weak var viewCRM: CustomEditText!
    @IBOutlet weak var viewEstabelecimento: CustomButtomCombo!
    
    @IBOutlet weak var viewBairro: CustomButtomCombo!
    // MARK: - Variables
    private let viewModel = MedicalGuideFilterViewModel()
    var locationManager = CLLocationManager()
    var didAcceptEmptyPlanQuery = false
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        setupNav()
        setNavBar(close: true)
        self.showModalNotification(notficacao: HomeResponse.verifyNotificationFor(location: .guia))
    }
    
    func setupNav() {
        let sidemenuBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        sidemenuBtn.setImage(UIImage(named: "Erase")?.resize(maxWidthHeight: 20), for: .normal)
        sidemenuBtn.addTarget(self, action: #selector(self.didTapClean), for: .touchUpInside)
        sidemenuBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let sideMenuBarBtn = UIBarButtonItem(customView: sidemenuBtn)

        self.navigationItem.setRightBarButtonItems([sideMenuBarBtn], animated: false)
    }
    
    @objc func didTapClean() {
        self.viewCRM.texto = ""
        self.viewNome.texto = ""
        self.viewModel.clean()
    }
    
    // MARK: - Setups
    private func setup() {
        setupSubscribers()
        setupTaps()
        setupTableView()
        setupLocationManager()
        setupView()
        setupEditText()
    }
    
    func setupView() {
        if isUserLogged() {
            self.viewModel.setUserLoggedData()
        }
        
        self.setupAutoScrollWhenKeyboardShowsUp()
    }
    
    func setupTableView() {
        self.qualificationsTableView.registerNib(named: CELL_IDENTIFIER)
        self.qualificationsTableView.delegate = self
        self.qualificationsTableView.dataSource = self
    }
    
    func setupTaps() {
        self.viewPlano.action = didTapPlan
        self.viewCidade.action = didTapCity
        self.viewEspecialidade.action = didTapSpecialtyService
        self.viewBairro.action = didTapBairro
        self.viewEstabelecimento.action = didTapTipo
    }
    
    func setupEditText(){
        viewCRM.editText.keyboardType = .numberPad
        viewCRM.editText.autocapitalizationType = .words
    }
    
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func setupSubscribers() {
        
        self.viewModel.selectedPlan.asObservable().subscribe(onNext: { value in
            self.viewPlano.texto = value?.descricao ?? "Selecione seu plano"
        }).disposed(by: self.viewModel.bag)
        
        self.viewModel.selectedCity.asObservable().subscribe(onNext: { value in
            self.viewCidade.texto = value?.descricao ?? "Selecione a cidade"
        }).disposed(by: self.viewModel.bag)
        
        self.viewModel.selectedBairro.asObservable().subscribe(onNext: { value in
            self.viewBairro.texto = value?.valor ?? "Selecione o bairro"
        }).disposed(by: self.viewModel.bag)
        
        self.viewModel.selectedTipo.asObservable().subscribe(onNext: { value in
            self.viewEstabelecimento.texto = value?.chave ?? "Selecione o tipo"
        }).disposed(by: self.viewModel.bag)
        
        self.viewModel.selectedSpecialtyOrService.asObservable().subscribe(onNext: { value in
            self.viewEspecialidade.texto = value?.map({$0.descricao ?? ""}).joined(separator: ", ") ?? "Selecione a especialidade"
        }).disposed(by: self.viewModel.bag)
        
        self.viewModel.isAdvancedFiltersHidden.asObservable().subscribe(onNext: { value in
//            self.advancedFilterViews.forEach({$0.isHidden = value})
            self.stackMaisFiltros.isHidden = value
            self.viewQualificacoes.isHidden = value
            self.lblMostrarMais.text = value ? "Mostrar" : "Ocultar"
            self.qualificationsTableView.reloadData()
            self.viewModel.fetchAdvancedFilter(error: { (msg) in
                self.showErrorAlert(message: msg)
            })
        }).disposed(by: self.viewModel.bag)
        
        self.viewModel.qualifications.asObservable().subscribe(onNext: { value in
            self.alturaLista.constant =  CGFloat(value.count) * self.CELL_QUALIFICATION_HEIGHT
            self.qualificationsTableView.reloadData()
        }).disposed(by: self.viewModel.bag)
//
//        self.checkCloserButton.addTarget(self, action: #selector(didTapCheckCloserButton), for: .touchUpInside)
    }
    
    // MARK: - Actions
    
    @objc func didTapCheckCloserButton() {
        self.locationManager.startUpdatingLocation()
    }
    
    @IBAction func didTapSearch(_ sender: Any) {
        if self.viewModel.selectedPlan.value == nil {
            self.showErrorAlert(message: "Selecione o Plano")
            return
        }
        
        if (self.viewModel.selectedCity.value == nil || self.viewModel.selectedSpecialtyOrService.value == nil ) && didAcceptEmptyPlanQuery == false {
            self.showAlert(title: "Aviso", message: "Poucos filtros preenchidos! O resultado da pesquisa pode consumir MUITO do plano de dados.", okBlock: {
                self.didAcceptEmptyPlanQuery = true
                self.didTapSearch(UIButton())
            }) {
                // do nothing
            }
            return
        }
        
        
        fetchPlaces()
        
    }
    
    private func fetchPlaces() {
        self.showLoader()
//        self.viewModel.fetchPlaces(orderByNearest: checkCloserButton.isSelected,success: { (response) in
        self.viewModel.request.crm = self.viewCRM.getTexto()
        self.viewModel.request.nomPrestador = self.viewNome.getTexto()
        self.viewModel.fetchPlaces(orderByNearest: true,success: { (response) in
            self.hideLoader()
            let vc = MedicalGuideListViewController(places: response, request: self.viewModel.request)
            self.navigationController?.pushViewController(vc, animated: true)
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    func didTapPlan() -> Void {
        if isUserLogged() {
            return
        }
        self.showLoader()
        self.viewModel.getPlans(success: { strings in
            
            ActionSheetStringPicker.show(withTitle: "Selecione o Plano", rows: strings, initialSelection: 0, doneBlock: { (picker, index, str) in
                self.viewModel.selectPlan(index: index, string: str as! String)
            }, cancel: { (picker) in
                
            }, origin: self.view)
            
            self.hideLoader()
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    @objc func didTapCity() -> Void {
        
        if(self.viewModel.canSelectCity()) {
            self.showLoader()
            self.viewModel.getCities(success: { strings in
                
                ActionSheetStringPicker.show(withTitle: "Selecione a Cidade", rows: strings, initialSelection: 0, doneBlock: { (picker, index, str) in
                    self.viewModel.selectCity(index: index, string: str as! String)
                }, cancel: { (picker) in
                    
                }, origin: self.view)
                
                self.hideLoader()
            }) { (msg) in
                self.hideLoader()
                self.showErrorAlert(message: msg)
            }
        } else {
            self.showErrorAlert(message: "Por favor, selecione o plano")
        }
    }
    
    @objc func didTapSpecialtyService() -> Void {
        if(self.viewModel.canSelectSpecialtyServices()) {
            self.showLoader()
            self.viewModel.getSpecialtyServices(success: { strings in
                
                var selectedSpecialties: [String] = []
                if let specialties = self.viewModel.selectedSpecialtyOrService.value {
                    selectedSpecialties = specialties.map({$0.descricao ?? ""})
                }
                
                var arr: [SearchModel] = []
                strings.forEach({ str in
                    arr.append(SearchModel(picture: nil, text: str, isSelected: selectedSpecialties.contains(str)))
                })
                let vc = SearchViewController(options: arr, multiSelection: true)
                vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)

                self.hideLoader()
            }) { (msg) in
                self.hideLoader()
                self.showErrorAlert(message: msg)
            }
        } else {
            self.showErrorAlert(message: "Por favor, selecione a cidade")
        }
    }
    
    @objc func didTapBairro() -> Void {
        if(self.viewModel.canSelectBairro()) {
            self.showLoader()
            self.viewModel.getBairros(success: { strings in
                
                ActionSheetStringPicker.show(withTitle: "Selecione o Bairro", rows: strings, initialSelection: 0, doneBlock: { (picker, index, str) in
                    self.viewModel.selectBairro(index: index, string: str as! String)
                }, cancel: { (picker) in
                    
                }, origin: self.view)
                
                self.hideLoader()
            }) { (msg) in
                self.hideLoader()
                self.showErrorAlert(message: msg)
            }
        } else {
            self.showErrorAlert(message: "Por favor, selecione a Cidade")
        }
    }
    
    @objc func didTapTipo() -> Void {
        if(self.viewModel.canSelectTipoEstabelecimento()) {
            self.showLoader()
            self.viewModel.getTipoEstabelecimento(success: { strings in
                
                ActionSheetStringPicker.show(withTitle: "Selecione o Tipo", rows: strings, initialSelection: 0, doneBlock: { (picker, index, str) in
                    self.viewModel.selectTipo(index: index, string: str as! String)
                }, cancel: { (picker) in
                    
                }, origin: self.view)
                
                self.hideLoader()
            }) { (msg) in
                self.hideLoader()
                self.showErrorAlert(message: msg)
            }
        } else {
            self.showErrorAlert(message: "Por favor, selecione o plano")
        }
    }
    
    
    @IBAction func mostrarMais(_ sender: Any) {
        self.viewModel.isAdvancedFiltersHidden.value = !self.viewModel.isAdvancedFiltersHidden.value
    }
    
}

// MARK: - TableView
extension MedicalGuideFilterViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CELL_QUALIFICATION_HEIGHT
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.qualifications.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as! QualificationTableViewCell
        let qualification = self.viewModel.qualifications.value[indexPath.row]
        cell.setup(image: qualification.quaImg, name: qualification.quaDsc, hideSelection: false, isSelected: qualification.selected)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let qualification = self.viewModel.qualifications.value[indexPath.row]
        qualification.selected = !qualification.selected
        tableView.reloadData()
    }
    
}

// MARK: - SearchViewControllerDelegate
extension MedicalGuideFilterViewController: SearchViewControllerDelegate {
    func didSelect(items: [SearchModel]) {
        self.viewModel.selectSpecialtyServices(strings: items.filter({$0.isSelected == true}).map({$0.text}))
    }
}

// MARK: - CLLocationManagerDelegate
extension MedicalGuideFilterViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let lat = NSNumber(value: locations.last?.coordinate.latitude ?? 0)
        let lng = NSNumber(value: locations.last?.coordinate.longitude ?? 0)
        if UserDefaults.standard.bool(forKey: KEY_PREFERENCE_LOCATION) == true {
            self.viewModel.setUserLatLng(lat: lat, lng: lng)
        }
        manager.stopUpdatingLocation()
    }
}
