//
//  FinanceiroVC.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 15/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class FinanceiroVC: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavBar(close: true)
        
        self.showModalNotification(notficacao: HomeResponse.verifyNotificationFor(location: .financeiro))
    }

    @IBAction func gotoBoleto(_ sender: Any) {
        self.abrir(tela: BoletoViewController())
//        self.navigationController?.pushViewController(BoletoViewController(), animated: true)
    }
    
    @IBAction func gotoIR(_ sender: Any) {
        self.abrir(tela: ImpostoRendaVC())
//        self.navigationController?.pushViewController(ImpostoRendaVC(), animated: true)
    }
    
    func abrir(tela: UIViewController){
        if self.verifyTicketAccess(){
            self.navigationController?.pushViewController(tela, animated: true)
        }
        else{
            self.showModalGeneric(titulo: "Financeiro", desc: "Você não tem acesso a essa funcionalidade", exibirConfirmar: true, exibirCancelar: false, acaoConfirmar: {}, acaoCancelar: {})
        }
    }
    
    
    func verifyTicketAccess() -> Bool{
        return (UserSingleton.shared.user?.beneficiario?.opcao_boleto?.boolValue) ?? false
    }
    
}
