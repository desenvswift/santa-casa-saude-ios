//
//  IRDetailVC.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 16/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit
import PDFKit
import WebKit

class IRDetailVC: UIViewController {

    @IBOutlet weak var viewTitulo: CustomViewTituloGrande!
    @IBOutlet weak var viewPdf: UIView!
    var link = ""
    var ano = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavBar()
        DispatchQueue.main.async {
            self.setupPdf()
        }
        viewTitulo.subTitulo = "Aqui está o seu informe de pagamentos referente ao ano de \(ano)"
    }
    
    func setupPdf(){
        self.showLoader()
        let pdfView = PDFView()

        pdfView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pdfView)

        pdfView.leadingAnchor.constraint(equalTo: viewPdf.leadingAnchor).isActive = true
        pdfView.trailingAnchor.constraint(equalTo: viewPdf.trailingAnchor).isActive = true
        pdfView.topAnchor.constraint(equalTo: viewPdf.topAnchor).isActive = true
        pdfView.bottomAnchor.constraint(equalTo: viewPdf.bottomAnchor).isActive = true
        
        
        DispatchQueue.main.async {
            if let resourceUrl = URL(string: self.link) {
                pdfView.document = PDFDocument(url: resourceUrl)
            }
            self.hideLoader()
        }
        
    }


    @IBAction func compartilhar(_ sender: Any) {
        if let resourceUrl = URL(string: self.link) {
            if let document = PDFDocument(url: resourceUrl){
                sharePDF(document)
            }
        }
    }
    
    func sharePDF(_ filePDF: PDFDocument) {
        if let pdfData = filePDF.dataRepresentation() {
            let objectsToShare = [pdfData]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//            activityVC.popoverPresentationController?.sourceView = self.button

            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
}
