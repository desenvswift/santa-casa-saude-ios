//
//  ReportsViewController.swift
//  Project
//
//  Created by Victor Catão on 25/02/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class ReportsViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userHeaderView: UserHeader!
    
    // MARK: - Variables
    private let viewModel = ReportsViewModel()
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        setupTabBarCollor()
        self.downloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.userHeaderView.setup()
        self.userHeaderView.showHomeItems()
    }
    
    // MARK: - Setups
    private func setup() {
        self.viewModel.delegate = self
        setupView()
        setupBack()
    }
    
    func setupView() {
        self.userHeaderView.delegate = self
        if isUserLogged(), let name = UserSingleton.shared.user?.beneficiario?.nome {
            let firstName = name.split(separator: " ").first ?? ""
            self.navigationItem.title = "Bem-vindo, \(firstName)"
        } else {
            self.navigationItem.title = "Bem-vindo"
        }
    }
    
    func setupTableView() {
        self.tableView.registerNib(named: self.viewModel.CELL_LIST_REPORT_IDENTIFIER)
        self.tableView.registerNib(named: self.viewModel.CELL_NOTIFICATION_IDENTIFIER)
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    func downloadData() {
        self.showLoader()
        self.viewModel.fetchHome(success: {
            self.setupTableView()
            self.tableView.reloadData()
            self.hideLoader()
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
}

// MARK: - UITableViewDelegate
extension ReportsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.viewModel.getHeaderViewForSection(index: section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.viewModel.getHeightForSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.viewModel.getHeightForRow(section: indexPath.section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.getNumberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.getNumberOfRows(at: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.viewModel.getCellForRow(section: indexPath.section, row: indexPath.row, tableView: tableView)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 { // notificacoes
            if let link = self.viewModel.homeResponse?.painelNotificacoes?[indexPath.row].link, let url = URL(string: link) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
}


// MARK: - ReportsViewModelDelegate
extension ReportsViewController: ReportsViewModelDelegate {
    func openViewController(_ vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showError(message: String) {
        self.showErrorAlert(message: message)
    }
    
    func displayLoader(show: Bool) {
        if show {
            self.showLoader()
        } else {
            self.hideLoader()
        }
    }
    
    func reloadTableView() {
        self.tableView.reloadData()
    }
}

extension ReportsViewController: UserHeaderDelegate {
    func didTapUserHeader() {
        if isUserLogged() {
            self.showLoader()
            SCSManager.getPlans(success: { (response) in
                self.hideLoader()
                if let planos = response.planos, planos.count > 1 {
                    let vc = SelectPlanViewController(plans: planos)
                    vc.delegate = self
                    self.present(vc, animated: true, completion: nil)
                }
            }) { (msg, status) in
                self.hideLoader()
                self.showErrorAlert(message: msg)
            }
        } else {
            AppDelegate.setupRootWindow()
        }
    }
}

// MARK: - SelectPlanViewControllerDelegate
extension ReportsViewController: SelectPlanViewControllerDelegate {
    func didSelectPlan() {
        AppDelegate.setupRootWindow()
    }
}

extension UIView {
    func dropShadow(color: UIColor? = UIColor.black,
                    height: CGFloat? = CGFloat(1),
                    opacity: Float? = 0.15,
                    radius: CGFloat? = 10) {
        layer.masksToBounds = false
        layer.shadowColor = color?.cgColor ?? UIColor.black.cgColor
        layer.shadowOpacity = opacity ?? 0.15
        layer.shadowOffset = CGSize(width: 0, height: height ?? 1.0)
        layer.shadowRadius = radius ?? 10
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        //        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    }
}
