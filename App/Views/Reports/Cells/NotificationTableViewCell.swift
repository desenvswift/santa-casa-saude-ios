//
//  NotificationTableViewCell.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 29/06/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: CataoUIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var pictureImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(item: PainelBoleto?) {
        self.titleLabel.text = item?.titulo
        self.messageLabel.text = item?.corpo
        self.pictureImageView.setBlobImage(string: item?.imagem)
        self.pictureImageView.isHidden = item?.imagem == nil || item?.imagem == ""
        
        let values: [String] = NotificationName.allCases.map { $0.rawValue }
        titleLabel.isHidden = values.contains(item?.titulo ?? "")
    }
    
}
