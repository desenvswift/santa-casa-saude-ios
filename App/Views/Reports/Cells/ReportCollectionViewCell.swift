//
//  ReportCollectionViewCell.swift
//  Base
//
//  Created by Victor Catão on 25/02/19.
//  Copyright © 2019 Catao. All rights reserved.
//

import UIKit

protocol ReportCollectionViewCellDelegate: class {
    func didTapOk(item: PainelBoleto)
    func didTapSee(item: PainelBoleto)
}

class ReportCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var descricaoLabel: UILabel!
    @IBOutlet weak var seeGuideButton: UIButton!
    
    weak var delegate: ReportCollectionViewCellDelegate?
    var item: PainelBoleto!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(item: PainelBoleto) {
        self.dropShadow()
        self.item = item
        
        self.descricaoLabel.text = item.corpo
        
        self.iconImageView.image = item.idGuia == nil ? UIImage(named: "ic_home_alert") : UIImage(named: "ic_home_guia")
        self.seeGuideButton.setTitle(item.idGuia == nil ? "VER BOLETO" : "VER GUIAS", for: .normal)
    }

    @IBAction func didTapSeeSomething(_ sender: Any) {
        delegate?.didTapSee(item: self.item)
    }
    
    @IBAction func didTapOk(_ sender: Any) {
        delegate?.didTapOk(item: self.item)
    }
    
}
