//
//  ListReportTableViewCell.swift
//  Base
//
//  Created by Victor Catão on 02/05/19.
//  Copyright © 2019 Catao. All rights reserved.
//

import UIKit

class ListReportTableViewCell: UITableViewCell {
    private let CELL_IDENTIFIER = "ReportCollectionViewCell"
    
    @IBOutlet weak var collectionView: UICollectionView!
    weak var delegate: ReportCollectionViewCellDelegate?
    
    var items: [PainelBoleto] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.registerNib(named: CELL_IDENTIFIER)
    }

    func setup(items: [PainelBoleto]) {
        self.items = items
        self.collectionView.reloadData()
    }
    
}

// MARK: - CollectionViewDelegate and DataSource
extension ListReportTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 30, height: self.collectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CELL_IDENTIFIER, for: indexPath) as! ReportCollectionViewCell
        cell.setup(item: items[indexPath.row])
        cell.delegate = self.delegate
        return cell
    }
    
    
    
}
