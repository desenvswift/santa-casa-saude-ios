//
//  ReportsViewModel.swift
//  Project
//
//  Created by Victor Catão on 25/02/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

protocol ReportsViewModelDelegate: class {
    func openViewController(_ vc: UIViewController)
    func showError(message: String)
    func displayLoader(show: Bool)
    func reloadTableView()
}

class ReportsViewModel {
    
    let CELL_LIST_REPORT_IDENTIFIER = "ListReportTableViewCell"
    let CELL_NOTIFICATION_IDENTIFIER = "NotificationTableViewCell"
    
    let sectionTitles = ["BOLETOS EM ABERTO", "GUIAS EM APROVAÇÃO", "NOTIFICAÇÕES"]
    var homeResponse: HomeResponse?
    weak var delegate: ReportsViewModelDelegate?
    
    func getNumberOfSections() -> Int {
        return sectionTitles.count
    }
    
    func getSectionTitle(index: Int) -> String {
        return sectionTitles[index]
    }
    
    func getHeightForRow(section: Int) -> CGFloat {
        return (section == 0 || section == 1) ? 200 : UITableViewAutomaticDimension
    }
    
    func getHeightForSection(section: Int) -> CGFloat {
        if section == 0 {
            return (self.homeResponse?.painelBoleto?.count ?? 0) > 0 ? 30 : 0
        } else if section == 1 {
            return (self.homeResponse?.painelGuiasAprovacao?.count ?? 0) > 0 ? 30 : 0
        }
        return (self.homeResponse?.painelNotificacoes?.count ?? 0) > 0 ? 30 : 0
    }
    
    func getNumberOfRows(at section: Int) -> Int {
        if section == 0 {
            return (self.homeResponse?.painelBoleto?.count ?? 0) > 0 ? 1 : 0
        } else if section == 1 {
            return (self.homeResponse?.painelGuiasAprovacao?.count ?? 0) > 0 ? 1 : 0
        } else {
            return self.homeResponse?.painelNotificacoes?.count ?? 0
        }
    }
    
    func getHeaderViewForSection(index: Int) -> UIView? {
        let v = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 30))
        v.backgroundColor = .clear
        
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.size.width-10, height: 30))
        label.text = self.getSectionTitle(index: index)
        label.font = UIFont.systemFont(ofSize: 13, weight: .bold)
        label.textColor = .darkGray
        
        v.addSubview(label)
        v.backgroundColor = Colors.hexStringToUIColor(hex: "#EFEFEF")
        
        return v
    }
    
    func getCellForRow(section: Int, row: Int, tableView: UITableView) -> UITableViewCell {
        if section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_LIST_REPORT_IDENTIFIER) as! ListReportTableViewCell
            cell.setup(items: self.homeResponse?.painelBoleto ?? [])
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        } else if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_LIST_REPORT_IDENTIFIER) as! ListReportTableViewCell
            cell.setup(items: self.homeResponse?.painelGuiasAprovacao ?? [])
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_NOTIFICATION_IDENTIFIER) as! NotificationTableViewCell
        cell.setup(item: self.homeResponse?.painelNotificacoes?[row])
        cell.selectionStyle = .none
        return cell
    }
    
    
    func fetchHome(success: @escaping ()->(), error: @escaping (String)->()) {
        SCSManager.getHome(success: { (response) in
            self.homeResponse = response
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    
    
    
}

// MARK: - ReportCollectionViewCellDelegate
extension ReportsViewModel: ReportCollectionViewCellDelegate {
    func didTapOk(item: PainelBoleto) {
        self.delegate?.displayLoader(show: true)
        SCSManager.clickHome(idHome: item.idHome ?? "", success: {
            if item.idBoleto != nil {
                self.homeResponse?.painelBoleto?.removeAll(where: {$0.idHome == item.idHome})
            } else {
                self.homeResponse?.painelGuiasAprovacao?.removeAll(where: {$0.idHome == item.idHome})
            }
            self.delegate?.reloadTableView()
            self.delegate?.displayLoader(show: false)
        }) { (msg, status) in
            self.delegate?.displayLoader(show: false)
            self.delegate?.showError(message: msg)
        }
    }
    
    func didTapSee(item: PainelBoleto) {
        if item.idGuia != nil { // eh guia
            
            self.delegate?.displayLoader(show: true)
            SCSManager.getAuthorizationGuideDetail(id: item.idGuia!, success: { (response) in
                self.delegate?.displayLoader(show: false)
                let vc = AuthorizationGuideDetailViewController(detail: response, request: nil)
                self.delegate?.openViewController(vc)
            }) { (msg, status) in
                self.delegate?.displayLoader(show: false)
                self.delegate?.showError(message: msg)
            }
            
        } else { // boleto
            
            self.delegate?.displayLoader(show: true)
            SCSManager.getboletos(ano: "2023", success: { (boletos) in
                self.delegate?.displayLoader(show: false)
                if let boleto = boletos.first(where: {$0.id == item.idBoleto}) {
                    let vc = BoletoDetailViewController(boleto: boleto)
                    self.delegate?.openViewController(vc)
                }
            }) { (msg, status) in
                self.delegate?.displayLoader(show: false)
                self.delegate?.showError(message: msg)
            }
            
        }
    }
}
