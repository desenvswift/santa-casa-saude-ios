//
//  MedicalDetailViewController.swift
//  Project
//
//  Created by Victor Catão on 05/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit
import GoogleMaps

class MedicalDetailViewController: UIViewController {
    
    convenience required init(place: PlaceResponse) {
        self.init()
        self.viewModel.place = place
    }
    
    let CELL_IDENTIFIER = "QualificationTableViewCell"
    let CELL_QUALIFICATIONS_HEIGHT: CGFloat = 25.0
    
    @IBOutlet weak var mapView: UIView!
    
    @IBOutlet weak var imgFavorito: UIImageView!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var crmLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var lblCidadeEstado: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var razaoSocialLabel: UILabel!
    @IBOutlet weak var cnpjLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    
    
    var qualifications: [QualificationModel] = []
    
    // MARK: - Variables
    private let viewModel = MedicalDetailViewModel()
    
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.setNavBar()
        self.setupNavButtons()
    }
    
    func setupNavButtons() {
        let share = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        share.setImage(UIImage(named: "Share")?.resize(maxWidthHeight: 20), for: .normal)
        share.addTarget(self, action: #selector(self.didTapshareView), for: .touchUpInside)
        share.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let sideMenuBarBtn = UIBarButtonItem(customView: share)
        
        let map = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        map.setImage(UIImage(named: "Map")?.resize(maxWidthHeight: 20), for: .normal)
        map.addTarget(self, action: #selector(self.didTapmapRouteView), for: .touchUpInside)
        map.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let sideMenuBarBtn1 = UIBarButtonItem(customView: map)
        
        let phone = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        phone.setImage(UIImage(named: "Phone")?.resize(maxWidthHeight: 20), for: .normal)
        phone.addTarget(self, action: #selector(self.didTapcallView), for: .touchUpInside)
        phone.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let sideMenuBarBtn2 = UIBarButtonItem(customView: phone)
        
        let whats = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        whats.setImage(UIImage(named: "WhatsApp")?.resize(maxWidthHeight: 20), for: .normal)
        whats.addTarget(self, action: #selector(self.didTapWhatsapp), for: .touchUpInside)
        whats.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let sideMenuBarBtn3 = UIBarButtonItem(customView: whats)

        self.navigationItem.setRightBarButtonItems([sideMenuBarBtn, sideMenuBarBtn1, sideMenuBarBtn2, sideMenuBarBtn3], animated: false)
    }
    
    // MARK: - Setups
    private func setup() {
        setupView()
    }
    
    
    func setupView() {
        let place = self.viewModel.place
        
        // MAP
        let (mainLat, mainLng) = (place?.prestadorLatitude?.replacingOccurrences(of: ",", with: ".").doubleValue ?? 0, place?.prestadorLongitude?.replacingOccurrences(of: ",", with: ".").doubleValue ?? 0)
        let camera = GMSCameraPosition.camera(withLatitude: mainLat, longitude: mainLng, zoom: 16.0)
        let mapViewGMS = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.mapView.frame.width - 20, height: self.mapView.frame.height),
                                        camera: camera)
        self.mapView.addSubview(mapViewGMS)

        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: mainLat,
                                                 longitude: mainLng)
        marker.title = place?.prestadorNome
        marker.map = mapViewGMS
        marker.icon = UIImage(named: "nem_pin_map")
        
        // DOCTOR SPACE
        self.updateFavoriteView()
        
        self.doctorNameLabel.text = place?.prestadorNome
        self.crmLabel.text = "\(place?.especialidadeDsc ?? "") | CRM \(place?.prestadorNumero ?? "")"
        self.addressLabel.text = "\(place?.prestadorLogradouro ?? ""), \(place?.prestadorNumero ?? "") - \(place?.prestadorBairro ?? "")"
        self.lblCidadeEstado.text = "\(place?.cidadeDsc ?? "") - \(place?.prestadorUF ?? "")"
        
        self.distanceLabel.text = String.init(format: "%.2f", place?.prestadorDistancia?.floatValue ?? 0.0)
        
        self.qualifications = place?.prestadorQualificacoes ?? []
        
        // MEDICAL INFOS
        self.razaoSocialLabel.text = place?.prestadorRazaoSocial
        self.cnpjLabel.text = place?.prestadorCNPJ
        self.phoneLabel.text = place?.listaTelefone?.map({$0.prestadorTelefone ?? ""}).joined(separator: "    ")
        self.emailLabel.text = place?.prestadorEmail ?? "-"
    }
    
    func updateFavoriteView() {
        self.imgFavorito.image = self.viewModel.isFavorite() ? UIImage(named: "StarFilled") : UIImage(named: "favorito")
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapfavoriteView))
        imgFavorito.isUserInteractionEnabled = true
        imgFavorito.addGestureRecognizer(tap)
    }
    
    // MARK: - Taps
    
    @objc func didTapfavoriteView(){
        if isUserLogged() {
//            self.showLoader()
            self.viewModel.favoriteOuUnfavorite(success: {
                self.updateFavoriteView()
//                self.hideLoader()
            }) { (msg) in
//                self.hideLoader()
                self.showErrorAlert(message: msg)
            }
        } else {
            self.showErrorAlert(message: "Você precisa fazer login para registrar os seus favoritos")
        }
    }
    
    @objc func didTapcallView(){
        if let phone = self.viewModel.place?.listaTelefone?.first?.prestadorTelefone?
            .replacingOccurrences(of: " ", with: "")
            .replacingOccurrences(of: ")", with: "")
            .replacingOccurrences(of: "(", with: "")
            .replacingOccurrences(of: "-", with: ""), let url = URL(string: "tel://\(phone)") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        
    }
    
    @objc func didTapWhatsapp(){
        if let phone = self.viewModel.place?.listaTelefone?.first?.prestadorTelefone?
            .replacingOccurrences(of: " ", with: "")
            .replacingOccurrences(of: ")", with: "")
            .replacingOccurrences(of: "(", with: "")
            .replacingOccurrences(of: "-", with: ""), let appURL = URL(string: "https://api.whatsapp.com/send?phone=+55\(phone)") {
            if UIApplication.shared.canOpenURL(appURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
                }
                else {
                    UIApplication.shared.openURL(appURL)
                }
            }
        }
    }
    
    @objc func didTapmapRouteView(){
        let (mainLat, mainLng) = (self.viewModel.place?.prestadorLatitude?.replacingOccurrences(of: ",", with: ".").doubleValue ?? 0, self.viewModel.place?.prestadorLongitude?.replacingOccurrences(of: ",", with: ".").doubleValue ?? 0)
        self.route(locationSelected: CLLocationCoordinate2D(latitude: mainLat, longitude: mainLng))
    }
    
    @objc func didTapshareView(){
        let str = "\(self.viewModel.place?.prestadorNome ?? "")\n\n\(self.phoneLabel.text ?? "")\n\n\(self.addressLabel.text ?? "")\nCEP: \(self.viewModel.place?.prestadorCEP ?? "")\nhttps://www.google.com/maps?q=\(self.viewModel.place?.prestadorLatitude?.replacingOccurrences(of: ",", with: ".") ?? ""),\(self.viewModel.place?.prestadorLongitude?.replacingOccurrences(of: ",", with: ".") ?? "")"
        let activityController = UIActivityViewController(activityItems: [str], applicationActivities: nil)
        self.present(activityController, animated: true, completion: nil)
    }
    
    @IBAction func verPlanos(_ sender: Any) {
        self.showLoader()
        self.viewModel.getPlans(success: { (strings) in
            self.hideLoader()
            let modal = PlansModalViewController(plans: strings)
            self.showModal(viewController: modal)
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
}
