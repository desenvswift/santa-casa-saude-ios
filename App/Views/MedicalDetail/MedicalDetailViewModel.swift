//
//  MedicalDetailViewModel.swift
//  Project
//
//  Created by Victor Catão on 05/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class MedicalDetailViewModel {
    
    var place: PlaceResponse?
    
    func favoriteOuUnfavorite(success: @escaping ()->(), error: @escaping (String)->()) {
        let request = FavoriteRequest()
        request.guiMedId = place?.guiaMedicoCod
        request.modo = self.isFavorite() ? "R":"I"
        SCSManager.favorite(request: request, success: { (res) in
            self.place?.favorito = NSNumber(booleanLiteral: !self.isFavorite())
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func getPlans(success: @escaping ([String])->(), error: @escaping (String)->()) {
        SCSManager.getPlanosAtendidos(guiaMedCod: place?.guiaMedicoCod ?? "", success: { (response) in
            success(response.lista?.map({$0.planoDsc ?? ""}) ?? [])
        }) { (msg, statu) in
            error(msg)
        }
    }
    
    func isFavorite() -> Bool {
        return self.place?.favorito?.boolValue == true
    }
    
}
