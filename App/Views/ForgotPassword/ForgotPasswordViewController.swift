//
//  ForgotPasswordViewController.swift
//  Project
//
//  Created by Victor Catão on 03/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var viewCpf: CustomEditText!
    
    // MARK: - Variables
    private let viewModel = ForgotPasswordViewModel()
    
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.setNavBar()
    }
    
    // MARK: - Setups
    private func setup() {
        setupAutoScrollWhenKeyboardShowsUp()
        viewCpf.configure(textFieldType: .CPF)
    }
    
    // MARK: - Actions
    @IBAction func didTapSendToEmail(_ sender: Any) {
        viewCpf.validate()
        if(viewCpf.isValid()){
            self.callBackendRequest(type: "E")
        }
    }
    
    @IBAction func didTapSendToPhone(_ sender: Any) {
        viewCpf.validate()
        if(viewCpf.isValid()){
            self.callBackendRequest(type: "C")
        }
    }
    
    func callBackendRequest(type: String) {
        self.showLoader()
        self.viewModel.recoverPassword(cpf: viewCpf.editText.text!, type: type, success: { (response) in
            self.hideLoader()
            let status = response.getStatus()
            let msg = response.getMsgExterna()
            
            if status == 1 {
                self.showSuccessAlert(message: msg ?? "Verifique seu e-mail/celular para finalizar o processo.")
            } else {
                self.showErrorAlert(message: msg ?? ERROR_SOMETHING_WENT_WRONG)
            }
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
