//
//  ForgotPasswordViewModel.swift
//  Project
//
//  Created by Victor Catão on 03/05/19.
//  Copyright (c) 2019 Catao. All rights reserved.
//

import UIKit

class ForgotPasswordViewModel {
    
    func recoverPassword(cpf: String, type: String, success: @escaping (CataoModel)->(), error: @escaping (String)->()) {
        let req = RecoverPasswordRequest()
        req.cpf = cpf.replacingOccurrences(of: ".", with: "").replacingOccurrences(of: "-", with: "")
        req.modoEnvio = type
        SCSManager.recoverPassword(request: req, success: { (response) in
            success(response)
        }) { (msg, _) in
            error(msg)
        }
    }
    
}
