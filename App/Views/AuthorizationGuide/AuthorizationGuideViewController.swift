//
//  AuthorizationGuideViewController.swift
//  Project
//
//  Created by Victor Catão on 05/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class AuthorizationGuideViewController: UIViewController {
    
    let CELL_IDENTIFIER = "AuthorizationGuideTableViewCell"
    let CELL_IDENTIFIER_HIST = "AuthHistoricoCell"
    @IBOutlet weak var txtFiltro: UITextField!
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewPeriodo: UIView!
    @IBOutlet weak var tableViewHistorico: UITableView!
    @IBOutlet weak var emptyStackView: UIStackView!
    @IBOutlet weak var btnSolicitacoes: CustomButton!
    @IBOutlet weak var btnHistorico: CustomButton!
    @IBOutlet weak var lblPeriodo: UILabel!
    var periodoSelecionado = "90"
    
    // MARK: - Variables
    private let viewModel = AuthorizationGuideViewModel()
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.populatePeriodos()
        self.setup()
        self.downloadData()
        configureSolicitacao()
        closeKeyboardOnTouch()
    }
    
    @IBAction func filter(_ sender: Any) {
        self.showLoader()
        self.viewModel.fetchGuides(filtro: txtFiltro.text ?? "", periodo: periodoSelecionado, success: {
            self.hideLoader()
            self.tableViewHistorico.reloadData()
            self.tableViewHistorico.isHidden = false
            self.emptyStackView.isHidden = true
//            self.emptyStackView.isHidden = self.viewModel.getNumberOfRows() > 0
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    // MARK: - Download
    func downloadData() {
        self.showLoader()
        self.viewModel.fetchAuthorizationGuide(success: {
            self.hideLoader()
            self.getGuides()
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    func getGuides(){
        self.showLoader()
        self.viewModel.fetchGuides(periodo: periodoSelecionado, success: {
            self.hideLoader()
            self.tableView.reloadData()
            self.tableView.isHidden = false
            self.emptyStackView.isHidden = true
            self.emptyStackView.isHidden = self.viewModel.getNumberOfRows() > 0
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    // MARK: - Setups
    private func setup() {
        setNavBar(close: false)
        setupTableView()
    }
    
    func setupNav(exibeFiltro: Bool) {
        if !exibeFiltro{
            self.navigationItem.setRightBarButtonItems([], animated: false)
            return
        }
        let sidemenuBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        sidemenuBtn.setImage(UIImage(named: "Filter")?.resize(maxWidthHeight: 20), for: .normal)
        sidemenuBtn.addTarget(self, action: #selector(self.didTapNewAuthorization), for: .touchUpInside)
        sidemenuBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let sideMenuBarBtn = UIBarButtonItem(customView: sidemenuBtn)

        
        self.navigationItem.setRightBarButtonItems([sideMenuBarBtn], animated: false)
    }
    
    func setupTableView() {
        self.tableView.registerNib(named: CELL_IDENTIFIER)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableViewHistorico.registerNib(named: CELL_IDENTIFIER_HIST)
        self.tableViewHistorico.delegate = self
        self.tableViewHistorico.dataSource = self
    }
    // MARK: - Actions
    @objc func didTapNewAuthorization() {
        txtFiltro.isHidden = !txtFiltro.isHidden
    }
    
    func configureSolicitacao(){
        emptyStackView.isHidden = self.viewModel.getNumberOfRows() > 0
        tableView.isHidden = false
        tableViewHistorico.isHidden = true
        btnSolicitacoes.preenchimento = true
        btnHistorico.preenchimento = false
        
        setupNav(exibeFiltro: false)
        tableView.reloadData()
    }
    
    @IBAction func setupSolicitacoes(_ sender: Any) {
        configureSolicitacao()
        viewPeriodo.isHidden = true
    }
    
    @IBAction func openPeriodos(_ sender: Any) {
        viewModel.getPeriodos(success: { arr in
            ActionSheetStringPicker.show(withTitle: "Selecione um Período", rows: arr , initialSelection: 0, doneBlock: { (picker, index, strings) in
                self.periodoSelecionado = self.viewModel.getPeriodoSelecionado(index: index)
                self.filter(picker as Any)
                self.lblPeriodo.text = "Peíodo: \(strings ?? "")"
            }, cancel: { (picker) in
                // do nothing
            }, origin: self.view)
        })
    }
    
    @IBAction func setupHistorico(_ sender: Any) {
        viewPeriodo.isHidden = false
        emptyStackView.isHidden = true
        tableView.isHidden = true
        tableViewHistorico.isHidden = false
        btnSolicitacoes.preenchimento = false
        btnHistorico.preenchimento = true
        
        setupNav(exibeFiltro: true)
        tableViewHistorico.reloadData()
    }
}

// MARK: - TableView
extension AuthorizationGuideViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView{
            return self.viewModel.getNumberOfRows()
        }
        return self.viewModel.getNumberOfRowsHist()
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as! AuthorizationGuideTableViewCell
            cell.selectionStyle = .none
            cell.setup(auth: self.viewModel.getAuthorization(at: indexPath.row))
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER_HIST) as! AuthHistoricoCell
        cell.selectionStyle = .none
        cell.delegate = self
        cell.index = indexPath.row
        cell.listaDelegate = self
        cell.setup(auth: self.viewModel.getAuthorizationHist(at: indexPath.row))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableView{
            self.openAuthorizationDetail(indexPath)
            return
        }
        let vc = AuthGuideDetailVC()
        vc.item = viewModel.getGuide(at: indexPath.row)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    fileprivate func openAuthorizationDetail(_ indexPath: IndexPath) {
        self.showLoader()
        self.viewModel.fetchDetails(at: indexPath.row, success: { response in
            self.hideLoader()
            let vc = AuthorizationGuideDetailViewController(detail: response, request: self.viewModel.getAuthorization(at: indexPath.row))
            self.navigationController?.pushViewController(vc, animated: true)
        }) { (msg) in
            self.showErrorAlert(message: msg)
        }
    }
    
}

extension AuthorizationGuideViewController: refreshCellListDelegate{
    func refresh(index: Int) {
        viewModel.toogleChildVisible(at: index)
        tableViewHistorico.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
    }
}

extension AuthorizationGuideViewController: clickAssociatedDetail{
    func openDetail(item: AuthorizationGuideStatusResponse) {
        if item.PRAZO_ATRASO == "0"{
            let vc = AuthGuideDetailVC()
            vc.item = item
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let vc = LateAuthDetailVC()
            vc.msg = item.MSG_ATRASO ?? ""
            vc.telefone = item.TELEFONE_CONTATO ?? ""
            vc.wpp = item.WHATS_CONTATO ?? ""
            vc.link = item.LINK_CONTATO ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
