//
//  AuthHistoricoCell.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 30/10/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class AuthHistoricoCell: UITableViewCell {
    @IBOutlet weak var bgView: CataoUIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var statusButtonView: CataoUIView!
    @IBOutlet weak var statusButtonLabel: UILabel!
    @IBOutlet weak var lblData: UILabel!
    @IBOutlet weak var viewFilhos: UIView!
    @IBOutlet weak var imgArrow: UIImageView!
    //    @IBOutlet weak var stackFilhos: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var alturaLista: NSLayoutConstraint!
    var listaDelegate: refreshCellListDelegate?
    var index = 0
    
    var delegate: clickAssociatedDetail?
    var filhos: [AuthorizationGuideStatusResponse]?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bgView.dropShadow()
//
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.toggleFilhos (_:)))
        viewFilhos.addGestureRecognizer(gesture)
    }
    
    @objc func toggleFilhos(_ sender:UITapGestureRecognizer){
        listaDelegate?.refresh(index: index)
        layoutIfNeeded()
    }
    
    func setup(auth: AuthorizationGuideStatusResponse) {
        self.titleLabel.text = "N° de transação \(auth.NR_TRANSACAO ?? "")"
        imgArrow.image = UIImage(named: "ic_arrow_down")
        
        self.statusButtonView.backgroundColor = Colors.hexStringToUIColor(hex: auth.statusCor ?? "")
        self.statusButtonLabel.text = auth.STATUS_GUIA
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        if let date = dateFormatter.date(from: auth.DT_EMISSAO ?? "") {
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
            let date = dateFormatter.string(from: date)
            self.lblData.text = "Aberto em \(date)"
        }
        
        if !auth.isVisible {
            tableView.isHidden = true
            return
        }
        
        imgArrow.image = UIImage(named: "ic_arrow_down")?.rotate(180)
        tableView.isHidden = false
        filhos = auth.guiasPai
        viewFilhos.isHidden = true
        if filhos != nil && !(filhos!.isEmpty){
            viewFilhos.isHidden = false
        }
        alturaLista.constant = CGFloat((filhos?.count ?? 0) * 130)
        self.tableView.registerNib(named: "FilhoCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
    }
    
}


// MARK: - TableView
extension AuthHistoricoCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filhos?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilhoCell") as! FilhoCell
        cell.selectionStyle = .none
        let item = filhos![indexPath.row]
        cell.index = indexPath.row
        cell.listaDelegate = self
        cell.delegate = delegate
        cell.setup(auth: item, last: indexPath.row == (filhos!.count - 1))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let item = filhos?[indexPath.row]{
            delegate?.openDetail(item: item)
        }
    }
    
}

extension AuthHistoricoCell: refreshCellListDelegate{
    func refresh(index: Int) {
        tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        listaDelegate?.refresh(index: index)
        layoutIfNeeded()
    }
}

protocol clickAssociatedDetail{
    func openDetail(item: AuthorizationGuideStatusResponse)
}

