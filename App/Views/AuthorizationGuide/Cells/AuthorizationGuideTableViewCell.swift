//
//  AuthorizationGuideTableViewCell.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 05/06/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import UIKit

class AuthorizationGuideTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bgView: CataoUIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var protocolNumberLabel: UILabel!
    @IBOutlet weak var forecastLabel: UILabel!
    
    @IBOutlet weak var statusButtonView: CataoUIView!
    @IBOutlet weak var statusButtonLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bgView.dropShadow()
    }
    
    func setup(auth: AuthorizationGuide) {
        self.titleLabel.text = auth.titulo
        self.protocolNumberLabel.text = "Número do protocolo: \(auth.numeroProtocolo ?? "")"
        self.forecastLabel.text = "Previsão: \(auth.previsao ?? "")"
        
        self.statusButtonView.isHidden = false
        
        self.statusButtonView.backgroundColor = Colors.hexStringToUIColor(hex: auth.statusCor ?? "")
        self.statusButtonLabel.textColor = Colors.hexStringToUIColor(hex: auth.statusCorTexto ?? "")
        self.statusButtonLabel.text = auth.status
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        if let date = dateFormatter.date(from: auth.datahora ?? "") {
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
            let date = dateFormatter.string(from: date)
            let split = date.split(separator: " ")
            self.dateLabel.text = "\(split.first ?? "") às \(split.last ?? "")"
        }
    }
    
    func setup(auth: AuthDetailResponse) {
        self.titleLabel.text = auth.titulo
        self.protocolNumberLabel.text = "Número do protocolo: \(auth.numeroProtocolo ?? "")"
        self.forecastLabel.isHidden = true
        
        self.statusButtonView.isHidden = (auth.autorizadoLink == nil || auth.autorizadoLink == "")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        if let date = auth.datahora {
            let split = date.split(separator: " ")
            self.dateLabel.text = "\(split.first ?? "") às \(split.last ?? "")"
        } else {
            self.dateLabel.text = ""
        }
    }
    
    
}
