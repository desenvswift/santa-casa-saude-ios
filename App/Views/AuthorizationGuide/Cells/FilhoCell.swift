//
//  FilhoCell.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 30/10/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class FilhoCell: UITableViewCell {

    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var lblData: UILabel!
    @IBOutlet weak var viewStatus: CataoUIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var lblAtendiemtno: UILabel!
    @IBOutlet weak var lblProcedimento: UILabel!
    @IBOutlet weak var viewFilhos: UIView!
    @IBOutlet weak var stackFilhos: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var alturaLista: NSLayoutConstraint!
    @IBOutlet weak var imgArrow: UIImageView!
    var delegate: clickAssociatedDetail?
    var listaDelegate: refreshCellListDelegate?
    var index = 0
    var filhos: [AuthorizationGuideStatusResponse]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.toggleFilhos (_:)))
        viewFilhos.addGestureRecognizer(gesture)
    }
    
    @objc func toggleFilhos(_ sender:UITapGestureRecognizer){
        listaDelegate?.refresh(index: index)
        layoutIfNeeded()
    }
    
    func setup(auth: AuthorizationGuideStatusResponse, last: Bool) {
        self.lblTitulo.text = "N° de transação \(auth.NR_TRANSACAO ?? "")"
        imgArrow.image = UIImage(named: "ic_arrow_down")
        
        self.viewStatus.backgroundColor = Colors.hexStringToUIColor(hex: auth.statusCor ?? "")
        self.lblStatus.text = auth.STATUS_GUIA
        
        lblAtendiemtno.attributedText = decorateText(txt1: "Atendimento: ", txt2: auth.DS_TIPO_ATENDIMENTO ?? "")
        lblProcedimento.attributedText = decorateText(txt1: "Procedimento: ", txt2: auth.DS_PROCEDIMENTO ?? "")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        if let date = dateFormatter.date(from: auth.DT_EMISSAO ?? "") {
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
            let date = dateFormatter.string(from: date)
            self.lblData.text = "Aberto em \(date)"
        }
        else{
            self.lblData.text = "Aberto em \(auth.DT_EMISSAO ?? "")"
        }
        viewSeparator.isHidden = last
        
        if !auth.isVisible {
            tableView.isHidden = true
            return
        }
        
        imgArrow.image = UIImage(named: "ic_arrow_down")?.rotate(180)
        tableView.isHidden = false
        filhos = auth.filhos
        viewFilhos.isHidden = true
        if filhos != nil && !(filhos!.isEmpty){
            viewFilhos.isHidden = false
        }
        alturaLista.constant = CGFloat((filhos?.count ?? 0) * 130)
        self.tableView.registerNib(named: "FilhoCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
    }
    
    
    func decorateText(txt1:String, txt2:String)->NSAttributedString{
        let textAttributesOne = [NSAttributedStringKey.foregroundColor: Colors.MAIN_COLOR, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15)] as [NSAttributedStringKey : Any]
        let textAttributesTwo = [NSAttributedStringKey.foregroundColor: Colors.DarkGray ?? .black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15)] as [NSAttributedStringKey : Any]

        let textPartOne = NSMutableAttributedString(string: txt1, attributes: textAttributesOne)
        let textPartTwo = NSMutableAttributedString(string: txt2, attributes: textAttributesTwo)

        let textCombination = NSMutableAttributedString()
        textCombination.append(textPartOne)
        textCombination.append(textPartTwo)
        return textCombination
    }
}

// MARK: - TableView
extension FilhoCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filhos?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilhoCell") as! FilhoCell
        cell.selectionStyle = .none
        let item = filhos![indexPath.row]
        cell.index = indexPath.row
        cell.delegate = self.delegate
        cell.listaDelegate = self
        cell.setup(auth: item, last: indexPath.row == (filhos!.count - 1))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let item = filhos?[indexPath.row]{
            delegate?.openDetail(item: item)
        }
    }
    
}

extension FilhoCell: refreshCellListDelegate{
    func refresh(index: Int) {
        filhos?[index].isVisible = !(filhos?[index].isVisible ?? false)
        tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
    }
}

protocol refreshCellListDelegate{
    func refresh(index: Int)
}

