//
//  LateAuthDetailVC.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 29/11/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class LateAuthDetailVC: UIViewController {

    @IBOutlet weak var btnTell: CustomButton!
    @IBOutlet weak var viewTitulo: CustomViewTituloGrande!
    @IBOutlet weak var btnWpp: CustomButton!
    @IBOutlet weak var btnLink: CustomButton!
    
    var telefone = ""
    var wpp = ""
    var link = ""
    var msg = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewTitulo.subTitulo = msg
        btnTell.isHidden = telefone.isEmpty
        btnWpp.isHidden = wpp.isEmpty
        btnLink.isHidden = link.isEmpty
    }
    
    @IBAction func ligar(_ sender: Any) {
        let phone = telefone
            .replacingOccurrences(of: " ", with: "")
            .replacingOccurrences(of: ")", with: "")
            .replacingOccurrences(of: "(", with: "")
            .replacingOccurrences(of: "-", with: "")
        if let url = URL(string: "tel://\(phone)"){
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func gotoWpp(_ sender: Any) {
        let phone = wpp
            .replacingOccurrences(of: " ", with: "")
            .replacingOccurrences(of: ")", with: "")
            .replacingOccurrences(of: "(", with: "")
            .replacingOccurrences(of: "-", with: "")
        if let appURL = URL(string: "https://api.whatsapp.com/send?phone=+55\(phone)") {
            if UIApplication.shared.canOpenURL(appURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
                }
                else {
                    UIApplication.shared.openURL(appURL)
                }
            }
        }
    }
    
    @IBAction func gotoChat(_ sender: Any) {
        guard let url = URL(string: link) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
