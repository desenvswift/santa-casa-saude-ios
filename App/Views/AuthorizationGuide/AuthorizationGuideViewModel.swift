//
//  AuthorizationGuideViewModel.swift
//  Project
//
//  Created by Victor Catão on 05/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class AuthorizationGuideViewModel {
    
    private var authorizations: [AuthorizationGuide] = []
    private var guides: [AuthorizationGuideStatusResponse] = []
    var periodos: [KeyValueModel] = []
    
    func fetchAuthorizationGuide(success: @escaping ()->(), error: @escaping (String)->()){
        SCSManager.getAuthorizationGuide(success: { (list) in
            self.authorizations = list
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func populatePeriodos(){
        let p1 = KeyValueModel()
        p1.chave = "30"
        p1.valor = "1 Mês"
        let p2 = KeyValueModel()
        p2.chave = "90"
        p2.valor = "3 Meses"
        let p3 = KeyValueModel()
        p3.chave = "180"
        p3.valor = "6 Meses"
        let p4 = KeyValueModel()
        p4.chave = "360"
        p4.valor = "1 Ano"
        let p5 = KeyValueModel()
        p5.chave = "0"
        p5.valor = "Tudo"
        periodos = [p1, p2, p3, p4, p5]
    }
    
    func fetchGuides(filtro: String = "", periodo: String = "", success: @escaping ()->(), error: @escaping (String)->()){
        SCSManager.getAuthorizationGuideHistory(filtro: filtro, periodo: periodo, success: { (list) in
            self.guides = list
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func getPeriodos(success: @escaping ([String])->()) {
        if self.periodos.count > 0 {
            success(self.periodos.map({$0.valor ?? ""}))
            return
        }
    }
    
    func getPeriodoSelecionado(index: Int) -> String{
        return periodos[index].chave ?? "0"
    }
    
    func getNumberOfRows() -> Int {
        return self.authorizations.count
    }
    
    func getGuide(at: Int) -> AuthorizationGuideStatusResponse{
        return guides[at]
    }
    
    func getAuthorization(at index: Int) -> AuthorizationGuide {
        return self.authorizations[index]
    }
    
    func getNumberOfRowsHist() -> Int {
        return self.guides.count
    }
    
    func getAuthorizationHist(at index: Int) -> AuthorizationGuideStatusResponse {
        return self.guides[index]
    }
    
    func toogleChildVisible(at index: Int){
        self.guides[index].isVisible = !guides[index].isVisible
    }
    
    func fetchDetails(at index: Int, success: @escaping (AuthorizationGuideDetailResponse)->(), error: @escaping (String)->()) {
        SCSManager.getAuthorizationGuideDetail(id: self.authorizations[index].id ?? "", success: { (response) in
            success(response)
        }) { (msg, status) in
            error(msg)
        }
    }
    
}
