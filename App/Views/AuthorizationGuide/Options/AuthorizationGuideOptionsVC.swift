//
//  AuthorizationGuideOptionsVC.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 30/10/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class AuthorizationGuideOptionsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavBar(close: true)
        self.showModalNotification(notficacao: HomeResponse.verifyNotificationFor(location: .guias))
    }

    @IBAction func novoAutorizador(_ sender: Any) {
        let vc = AuthorizationGuideRequestViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func consultarStatus(_ sender: Any) {
        let vc = AuthorizationGuideViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func consultarProcedimentos(_ sender: Any) {
        showLoader()
        getLink(forLocation: .ConsultarProcedimentos, success: { link in
            self.hideLoader()
            if let url = URL(string: link ?? "") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else{
                self.showErrorAlert(message: "Erro ao carregar URL")
            }
        }) {msg in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
}
