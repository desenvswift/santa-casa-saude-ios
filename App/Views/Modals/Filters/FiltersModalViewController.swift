//
//  FiltersModalViewController.swift
//  Project
//
//  Created by Victor Catão on 25/05/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class FiltersModalViewController: UIViewController {
    
    let CELL_IDENTIFIER = "ImageCollectionViewCell"
    
    // MARK: - Init
    convenience required init(request: MedicalGuideRequest) {
        self.init()
        self.viewModel.request = request
    }
    
    // MARK: - Outlets
    @IBOutlet weak var planLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var specialtyServiceLabel: UILabel!
    @IBOutlet weak var qualificationsView: UICollectionView!
    
    // MARK: - Variables
    private let viewModel = FiltersModalViewModel()
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    // MARK: - Setups
    private func setup() {
        setupView()
        setupCollectionView()
    }
    
    func setupCollectionView() {
        self.qualificationsView.registerNib(named: CELL_IDENTIFIER)
        self.qualificationsView.delegate = self
        self.qualificationsView.dataSource = self
    }
    
    func setupView() {
        self.planLabel.text = self.viewModel.request.planName ?? "Não selecionado"
        self.cityLabel.text = self.viewModel.request.cityName ?? "Não selecionado"
        self.specialtyServiceLabel.text = self.viewModel.request.specialtyOrServiceName ?? "Não selecionado"
    }
    
    // MARK: - Actions
    @IBAction func didTapClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FiltersModalViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 20, height: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.request.qualifications?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CELL_IDENTIFIER, for: indexPath) as! ImageCollectionViewCell
        cell.setup(img: self.viewModel.request.qualifications?[indexPath.item].quaImg)
        return cell
    }
}
