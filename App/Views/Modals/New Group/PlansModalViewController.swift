//
//  PlansModalViewController.swift
//  Project
//
//  Created by Victor Catão on 01/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class PlansModalViewController: UIViewController {
    
    convenience required init(plans: [String]) {
        self.init()
        self.plans = plans
    }
    
    let CELL_HEIGHT: CGFloat = 50.0
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightTableViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    
    // MARK: - Variables
    private let viewModel = PlansModalViewModel()
    var plans: [String] = []
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    // MARK: - Setups
    private func setup() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.bgView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapBgView)))
    }
    
    @objc func didTapBgView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension PlansModalViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CELL_HEIGHT
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.heightTableViewConstraint.constant = min(CGFloat(self.plans.count) * CELL_HEIGHT, UIScreen.main.bounds.height-100)
        return self.plans.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.selectionStyle = .none
        cell.textLabel?.numberOfLines = 2
        cell.textLabel?.text = self.plans[indexPath.row]
        cell.textLabel?.font = UIFont.systemFont(ofSize: 15.0)
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        return cell
    }
}
