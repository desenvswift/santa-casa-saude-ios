//
//  GenericModalVC.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 11/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class GenericModalVC: UIViewController {

    @IBOutlet weak var viewArredondar: UIView!
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var btnFechar: UIButton!
    @IBOutlet weak var lblDescricao: UILabel!
    @IBOutlet weak var btnCancelar: CustomButton!
    @IBOutlet weak var btnConfirmar: CustomButton!
    
    var actionCancel: (() -> Void)?
    var actionConfirm: (() -> Void)?
    var titulo: String?
    var desc: String?
    var titleCancelar = "Cancelar"
    var titleConfirmar = "Confirmar"
    var showCancelar = true
    var showConfirmar = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    
    func setup(){
        if let titulo = self.titulo{
            lblTitulo.text = titulo
        }
        else{
            lblTitulo.isHidden = true
        }
        if let desc = self.desc{
            lblDescricao.text = desc
        }
        else{
            lblDescricao.isHidden = true
        }
        
        btnCancelar.isHidden = !showCancelar
        btnCancelar.setTitle(titleCancelar, for: .normal)
        btnConfirmar.isHidden = !showConfirmar
        btnConfirmar.setTitle(titleConfirmar, for: .normal)
    }
    

    @IBAction func fechar(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func cancelar(_ sender: Any) {
        if let cancelar = actionCancel{
            cancelar()
        }
        self.dismiss(animated: false)
    }
    
    @IBAction func confirmar(_ sender: Any) {
        if let confirm = actionConfirm{
            confirm()
        }
        self.dismiss(animated: false)
    }
}
