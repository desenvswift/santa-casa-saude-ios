//
//  NotificationModal.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 06/02/24.
//  Copyright © 2024 Mobile2You. All rights reserved.
//

import UIKit

class NotificationModal: UIViewController {

    @IBOutlet weak var viewTitulo: UIView!
    @IBOutlet weak var viewArredondar: UIView!
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var btnFechar: UIButton!
    @IBOutlet weak var lblDescricao: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var btnConfirmar: CustomButton!
    @IBOutlet weak var viewBtn: UIView!
    
    var notficacao: PainelBoleto?
    var titleConfirmar = "Confirmar"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.close (_:)))
        self.view.addGestureRecognizer(gesture)

    }
    
    func setup(){
        lblTitulo.text = "Aviso"
        
        if !(notficacao?.corpo?.isEmpty ?? false){
            lblDescricao.text = notficacao?.corpo
        }
        else{
            viewTitulo.isHidden = true
            viewBtn.isHidden = true
        }
        if !(notficacao?.imagem?.isEmpty ?? false){
            img.setBlobImage(string: notficacao?.imagem)
        }
        else{
            img.isHidden = true
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(openLink))
        lblDescricao.isUserInteractionEnabled = true
        lblDescricao.addGestureRecognizer(tap)
        img.isUserInteractionEnabled = true
        img.addGestureRecognizer(tap)
        
        btnConfirmar.setTitle(titleConfirmar, for: .normal)
    }
    
    @objc func openLink(){
        if let link = notficacao?.link{
            self.dismiss(animated: false)
            guard let url = URL(string: link) else { return }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

    @IBAction func fechar(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @objc func close(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true)
    }
    
    @IBAction func confirmar(_ sender: Any) {
        if let notificacao = notficacao{
            HomeResponse.markAsRead(notification: notificacao)
            SCSManager.clickHome(idHome: notificacao.idHome ?? "", success: { })
            { (msg, status) in
                print(msg)
            }
        }
        self.dismiss(animated: false)
    }

}
