//
//  CoparticipacaoModel.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 16/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import Foundation


class coparticiapacaoModel{
    
    var lista = [CopartList]()
    
    
    func fetchLista(success: @escaping()->(), error: @escaping(String)->()){
        if let list = createLista().lista{
            self.lista = list
            success()
            return
        }
        error("Ocorreu um erro")
                
    }
    
    func getItem(at: IndexPath) -> CopartList{
        return self.lista[at.row]
    }
    
    
    func createLista() -> CopartResponse{
        
        var proc = CopartProcedimentos()
        proc.procedimento = "Procedimento"
        proc.codigo = "0123456"
        proc.valor = "R$ 50,00"
        
        var item = CopartList()
        item.data = "16/05/2023"
        item.nome = "José Carlos da Silva"
        item.procedimento = [proc, proc]
        
        var retorno = CopartResponse()
        retorno.lista = [item, item, item, item, item, item]
        
        return retorno
        
    }
}
