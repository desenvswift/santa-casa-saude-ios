//
//  ProcedimentoView.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 16/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class ProcedimentoView: UIView {

    @IBOutlet weak var lblProcedimento: UILabel!
    @IBOutlet weak var lblCodigo: UILabel!
    @IBOutlet weak var lblValor: UILabel!
    
}
