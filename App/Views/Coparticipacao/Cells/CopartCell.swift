//
//  CopartCell.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 16/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class CopartCell: UITableViewCell {

    @IBOutlet weak var viewArredondar: UIView!
    @IBOutlet weak var stackItens: UIStackView!
    
    @IBOutlet weak var imageArrow: UIImageView!
    @IBOutlet weak var lblData: UILabel!
    @IBOutlet weak var lblNome: UILabel!
    
    var item: CopartList?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewArredondar.layer.cornerRadius = 8
        // Initialization code
        
    }
    
    func setup(item: CopartList){
        self.item = item
        for view in self.stackItens.subviews {
            view.removeFromSuperview()
        }
        
        lblNome.text = item.nome
        lblData.text = item.data
        addItemToStack()
    }
    
    func addItemToStack(){
        
        if let itens = item?.procedimento{
            for item in itens{
                if let view = Bundle.main.loadNibNamed("ProcedimentoView", owner: self, options: nil)?.first as? ProcedimentoView  {
                    view.lblProcedimento.text = "Procedimento: \(item.procedimento ?? "-")"
                    view.lblCodigo.text = "Código TUSS: \(item.codigo ?? "-")"
                    view.lblValor.text = item.valor
                    
                    let separator = UIView()
                    separator.backgroundColor = Colors.Gray
                    separator.heightAnchor.constraint(equalToConstant: 1).isActive = true

                    
                    self.stackItens.addArrangedSubview(separator)
                    self.stackItens.addArrangedSubview(view)
                }
            }
        }
    }
    
    @IBAction func expandir(_ sender: Any) {
        if let tela = self.parentViewController as? CoparticipacaoVC{
            tela.tableView.beginUpdates()
        }
        stackItens.isHidden = !stackItens.isHidden
        self.viewArredondar.layoutIfNeeded()
        
        if let tela = self.parentViewController as? CoparticipacaoVC{
            tela.tableView.endUpdates()
        }
        
        
        self.imageArrow.image = stackItens.isHidden ? UIImage(named: "Down") : UIImage(named: "Up")
    }
}
