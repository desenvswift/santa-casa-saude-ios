//
//  CoparticipacaoVC.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 16/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import UIKit

class CoparticipacaoVC: UIViewController {
    
    let CELL_IDENTIFIER = "CopartCell"
    let viewModel = coparticiapacaoModel()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()

    }
    
    func setup(){
        setNavBar()
        setupTableView()
        downloadData()
    }
    
    private func setupTableView() {
        self.tableView.registerNib(named: CELL_IDENTIFIER)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = UITableViewAutomaticDimension
        self.tableView.separatorColor = .clear

    }
    
    private func downloadData() {
        self.showLoader()
        self.viewModel.fetchLista(success: {
            self.hideLoader()
            self.tableView.reloadData()
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }

}

// MARK: - TableView
extension CoparticipacaoVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.lista.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as! CopartCell
        cell.setup(item: self.viewModel.getItem(at: indexPath))
        return cell
    }
    
}
