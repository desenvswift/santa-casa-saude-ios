//
//  FavoritesViewController.swift
//  Project
//
//  Created by Victor Catão on 05/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController {
    
    let CELL_IDENTIFIER = "FavoriteTableViewCell"
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variables
    private let viewModel = FavoritesViewModel()
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.downloadData()
    }
    
    // MARK: - Setups
    private func setup() {
        setNavBar()
        setupTableView()
    }
    
    func setupTableView() {
        self.tableView.registerNib(named: CELL_IDENTIFIER)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func downloadData() {
        self.showLoader()
        self.viewModel.fetchFavorites(success: {
            self.hideLoader()
            self.tableView.reloadData()
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
}

// MARK: - TableView
extension FavoritesViewController: UITableViewDelegate, UITableViewDataSource {
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let v = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 40))
//        v.backgroundColor = Colors.UIColorFromRGB(rgbValue: 0xDDDDDD)
//
//        let lbl = UILabel(frame: CGRect(x: 16, y: 0, width: self.tableView.frame.width-16, height: 40))
//        lbl.text = self.viewModel.getTitleForSection(section: section)
//        lbl.textColor = Colors.UIColorFromRGB(rgbValue: 0x666666)
//
//        v.addSubview(lbl)
//        return v
//    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 40
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
//        return self.viewModel.getNumberOfSections()
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as! FavoriteTableViewCell
        cell.selectionStyle = .none
        cell.setup(place: self.viewModel.getPlace(index: indexPath.row))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = MedicalDetailViewController(place: self.viewModel.getPlace(section: indexPath.section, index: indexPath.row))
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
