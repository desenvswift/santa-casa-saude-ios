//
//  FavoritesViewModel.swift
//  Project
//
//  Created by Victor Catão on 05/06/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class FavoritesViewModel {
    
    var favorites: [PlaceResponse] = []
    var dict: Dictionary<String?, [PlaceResponse]> = [:]
    
    
    func fetchFavorites(success: @escaping ()->(), error: @escaping (String)->()) {
        SCSManager.getFavorites(success: { (response) in
            self.favorites = response
            self.dict = Dictionary(grouping: self.favorites, by: {$0.especialidadeDsc})
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func getTitleForSection(section: Int) -> String? {
        return Array(self.dict.keys)[section]
    }
    
    func getNumberOfSections() -> Int {
        return self.dict.keys.count
    }
    
    func getNumberOfRows(section: Int) -> Int {
        let keys = Array(self.dict.keys)
        return self.dict[keys[section]]?.count ?? 0
    }
    
    
    func getNumberOfRows() -> Int {
        return self.favorites.count
    }
    
    func getPlace(index: Int) -> PlaceResponse {
        return self.favorites[index]
    }
    
    func getPlace(section: Int, index: Int) -> PlaceResponse {
        let key = Array(self.dict.keys)[section]
        return self.favorites[index] ?? PlaceResponse()
    }
    
}
