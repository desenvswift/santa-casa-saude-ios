//
//  FavoriteTableViewCell.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 05/06/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import UIKit

class FavoriteTableViewCell: UITableViewCell {

    @IBOutlet weak var viewArredondar: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var lblEspcialidade: UILabel!
    
    func setup(place: PlaceResponse) {
        self.viewArredondar.layer.cornerRadius = 8
        self.nameLabel.text = place.prestadorNome
        self.lblEspcialidade.text = "\(place.especialidadeDsc ?? "") | CRM \(place.prestadorNumero ?? "")"
    }
    
}
