//
//  MainTabBarViewController.swift
//  Base
//
//  Created by Victor Catão on 24/02/19.
//  Copyright © 2019 Catao. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabs()
         
        setNavBar(close: true)
    }
    
    fileprivate func setupTabs() {
        
        let firstViewController = createTabViewController(rootViewController: BoletoViewController(),
                                                          image: UIImage(named: "ic_boleto_azul"),
                                                          selectedImage: UIImage(named: "ic_boleto_cinza"),
                                                          title: "Boleto")
        
        let secondViewController = createTabViewController(rootViewController: MedicalGuideFilterViewController(),
                                                          image: UIImage(named: "ic_gm_azul"),
                                                          selectedImage: UIImage(named: "ic_gm_cinza"),
                                                          title: "Guia Médico")
        
        let thirdViewController = createTabViewController(rootViewController: ReportsViewController(),
                                                           image: UIImage(named: "ic_home_azul"),
                                                           selectedImage: UIImage(named: "ic_home_cinza"),
                                                           title: "Home")
        
        let fourthViewController = createTabViewController(rootViewController: HealthInsuranceViewController(),
                                                           image: UIImage(named: "ic_carteirinha_azul"),
                                                           selectedImage: UIImage(named: "ic_carteirinha_cinza"),
                                                           title: "Carteirinha")
        
        let fifthViewController = createTabViewController(rootViewController: MoreInformationsViewController(),
                                                           image: UIImage(named: "ic_mais_azul"),
                                                           selectedImage: UIImage(named: "ic_mais_cinza"),
                                                           title: "Mais")
        
        if isUserLogged() && UserSingleton.shared.user?.beneficiario?.opcao_boleto?.boolValue == true {
            viewControllers = [firstViewController, secondViewController, thirdViewController, fourthViewController, fifthViewController]
            self.selectedIndex = 2
        } else {
            viewControllers = [secondViewController, thirdViewController, fourthViewController, fifthViewController]
            self.selectedIndex = 1
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        setupBackgroundNavigation()
    }
    
    func createTabViewController(rootViewController: UIViewController, image: UIImage?, selectedImage: UIImage?, title: String) -> UIViewController {
        let navViewController = UINavigationController(rootViewController: rootViewController)
        navViewController.tabBarItem.image = image
        navViewController.tabBarItem.selectedImage = selectedImage
        navViewController.tabBarItem.title = title
        return navViewController
    }

}

extension UIViewController {
    func setupTabBarCollor() {
        if #available(iOS 15.0, *) {
            let appearance = UITabBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = .white
            tabBarController?.tabBar.standardAppearance = appearance
            tabBarController?.tabBar.scrollEdgeAppearance = tabBarController?.tabBar.standardAppearance
        }
    }
}
