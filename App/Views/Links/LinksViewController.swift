//
//  LinksViewController.swift
//  Project
//
//  Created by Victor Catão on 19/05/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class LinksViewController: UIViewController {
    
    let CELL_IDENTIFIER = "LinkTableViewCell"
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variables
    private let viewModel = LinksViewModel()
    
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.downloadData()
    }
    
    func downloadData() {
        self.showLoader()
        self.viewModel.fetchLinks(success: {
            self.hideLoader()
            self.tableView.reloadData()
        }) { (msg) in
            self.hideLoader()
            self.showErrorAlert(message: msg)
        }
    }
    
    // MARK: - Setups
    fileprivate func setupTableView() {
        self.tableView.registerNib(named: CELL_IDENTIFIER)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    private func setup() {
        setNavBar()
        setupTableView()
    }
    
}

// MARK: - TableView
extension LinksViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as! LinkTableViewCell
        cell.setup(link: self.viewModel.getLink(at: indexPath.row))
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let link = self.viewModel.getLink(at: indexPath.row).valor else { return }
        guard let url = URL(string: link) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
}
