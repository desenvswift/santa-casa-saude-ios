//
//  LinkTableViewCell.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 19/05/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import UIKit

class LinkTableViewCell: UITableViewCell {
    
    @IBOutlet weak var linkLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setup(link: NameValueModel) {
        self.linkLabel.text = link.nome
    }
    
}
