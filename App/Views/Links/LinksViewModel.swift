//
//  LinksViewModel.swift
//  Project
//
//  Created by Victor Catão on 19/05/19.
//  Copyright (c) 2019 Mobile2You. All rights reserved.
//

import UIKit

class LinksViewModel {
    
    var links: [NameValueModel] = []
    
    func fetchLinks(success: @escaping ()->(), error: @escaping (String)->()) {
        SCSManager.getLinks(success: { (response) in
            self.links = response.links ?? []
            success()
        }) { (msg, status) in
            error(msg)
        }
    }
    
    func getNumberOfRows() -> Int {
        return links.count
    }
    
    func getLink(at index: Int) -> NameValueModel {
        return links[index]
    }
}
