//
//  RecoverPasswordRequest.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 08/05/19.
//  Copyright © 2019 Catao. All rights reserved.
//

import Foundation

class RecoverPasswordRequest: CataoModel {
    var cpf: String?
    var modoEnvio: String?
}
