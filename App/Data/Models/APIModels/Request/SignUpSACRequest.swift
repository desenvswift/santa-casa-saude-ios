//
//  SignUpSACRequest.swift
//  Base
//
//  Created by Victor Catão on 06/05/19.
//  Copyright © 2019 Catao. All rights reserved.
//

import Foundation

class SignUpSACRequest: CataoModel {
    var nome: String?
    var nomeMae: String?
    var cpf: String?
    var email: String?
    var dtaNascimento: String?
    var ddd: String?
    var celular: String?
    var numCarteirinha: String?
    var idDispositivo: String?
}
