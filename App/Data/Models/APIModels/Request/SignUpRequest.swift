//
//  SignUpRequest.swift
//  Base
//
//  Created by Victor Catão on 04/05/19.
//  Copyright © 2019 Catao. All rights reserved.
//

import Foundation

class SignUpRequest: CataoModel {
    var etapa: NSNumber?
    var cpf: String?
    var dtaNascimento: String?
    var aceiteTermo: NSNumber?
    var nome: String?
    var email: String?
    var emailConf: String?
    var ddd: String?
    var celular: String?
    var senha: String?
    var senhaConf: String?
    var idDispositivo: String?
}
