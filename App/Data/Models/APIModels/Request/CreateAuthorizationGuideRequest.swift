//
//  CreateAuthorizationGuideRequest.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 22/06/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import Foundation

class CreateAuthorizationGuideRequest: CataoModel{
    var matBen: String?
    var tipSol: String?
    var locExame: String? = ""
    var dtaExame: String? = ""
    var fotoGuiaMedico: String? = "a"
    var fotoGuiaMedico2: String? = ""
    var fotoGuiaMedico3: String? = ""
}
