//
//  TalkWithUsRequest.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 04/06/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import Foundation

class TalkWithUsRequest: CataoModel {
    var nome: String?
    var cidade: String?
    var email: String?
    var telefone: String?
    var celular: String?
    var numeroCarterinha: String?
    var cpf: String?
    var codAssunto: String?
    var msg: String?
    var idDispositivo: String?
}
