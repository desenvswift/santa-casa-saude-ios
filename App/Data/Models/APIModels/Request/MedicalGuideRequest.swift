//
//  MedicalGuideRequest.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 20/05/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import Foundation

class MedicalGuideRequest: CataoModel {
    var guiMedCod: String?
    var filtro: String? // P = Plano / C = cidade / ES = EspecialidadeServico
    var planoCod: String? // 74
    var cidadeCod: String? // 2342
    var espServCod: String? // 42
    var espServTip: String? // E/S
    var calcDist: NSNumber? // bool
    var latitude: NSNumber?
    var longitude: NSNumber?
    var nomPrestador: String?
    var qualificacoes: String?
    var crm: String?
    var tpEstabelecimento: String?
    var bairro: String?
    
    
    // isso eh pra usar no modal
    var planName: String?
    var cityName: String?
    var specialtyOrServiceName: String?
    var qualifications: [QualificationModel]?
    
    //usado no filtro avencado
    var CODIGOPLANO: String?
    var UF: String? = "SP"
    var CodCidade: String?
}
