//
//  LinksResponse.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 29/06/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import Foundation

class LinksResponse: CataoModel {
    var links: [NameValueModel]?
}

class NameValueModel: CataoModel {
    var nome: String?
    var valor: String?
}
