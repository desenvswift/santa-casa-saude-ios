//
//  SubjectsListResponse.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 05/06/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import Foundation

class SubjectsListResponse: CataoModel {
    var comboAssunto: [SubjectResponse]?
}

class SubjectResponse: CataoModel {
    var valor: String?
    var descricao: String?
}
