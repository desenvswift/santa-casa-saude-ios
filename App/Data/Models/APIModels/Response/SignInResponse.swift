//
//  SignInResponse.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 07/05/19.
//  Copyright © 2019 Catao. All rights reserved.
//

import Foundation

class SignInResponse: CataoModel {
    var beneficiario: Beneficiario?
    var beneficiariosPlanos: BeneficiariosPlanos?
    var carteirinhas: Carteirinhas?
}

class Beneficiario: CataoModel {
    var nome, cpfBeneficiario, dtaNasc, ddd: String?
    var celular, email, codigoPlano, descricaoPlano: String?
    var cpfTitular, matTitular, matBeneficiario: String?
    var opcao_boleto: NSNumber?
    var data_inclusao: String?
}

class BeneficiariosPlanos: CataoModel {
    var planos: [Plano]?
}

class Plano: CataoModel {
    var PlanoId, PlanoDsc, MatBeneficiario, MatTitular: String?
    var CPFBeneficiario, CPFTitular: String?
}

class Carteirinhas: CataoModel {
    var imgFrente: String?
    var listaVerso: [ListaVerso]?
}

class ListaVerso: CataoModel {
    var imgVerso: String?
}

class UserResponseBack: CataoModel {
    var nome: String?
    var cpf_beneficiario: String?
    var dtaNasc: String?
    var ddd: String?
    var celular: String?
    var email: String?
    var codigoPlano: String?
    var descricaoPlano: String?
    var cpf_titular: String?
    var mat_titular: String?
    var mat_beneficiario: String?
}
