//
//  GetPlansResponse.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 01/06/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import Foundation

class GetPlansResponse: CataoModel {
    var lista: [PlanDscCod]?
}

class PlanDscCod: CataoModel {
    var planoCod: String?
    var planoDsc: String?
}
