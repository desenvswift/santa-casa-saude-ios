//
//  PlaceResponse.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 25/05/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import Foundation

class PlaceResponse: CataoModel {
    var guiaMedicoCod: String?
    var favorito: NSNumber?
    var prestadorNome: String?
    var prestadorCRM: String?
    var prestadorLocalAtendimento: String?
    var prestadorLogradouro: String?
    var prestadorNumero: String?
    var prestadorComplemento: String?
    var prestadorBairro: String?
    var cidadeDsc: String?
    var prestadorUF: String?
    var prestadorCEP: String?
    var prestadorCNPJ: String?
    var prestadorRazaoSocial: String?
    var listaTelefone: [Phone]?
    var prestadorEmail: String?
    var especialidadeDsc: String?
    var planoDsc: String?
    var prestadorLongitude: String?
    var prestadorLatitude: String?
    var prestadorDistancia: NSNumber?
    var prestadorQualificacoes: [QualificationModel]?
}

class QualificationModel: CataoModel {
    var quaCod: String?
    var quaImg: String?
    var quaDsc: String?
    var selected = false
}

class Phone: CataoModel {
    var prestadorTelefone: String?
}
