//
//  GetFilterAdvancedResponse.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 30/05/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import Foundation

class GetFilterAdvancedResponse: CataoModel {
    var qualificacoes: [QualificationModel]?
    var tipoEstabelecimento: [KeyValueModel]?
    var uf: [KeyValueModel]?
    var bairro: [KeyValueModel]?
}

class KeyValueModel: CataoModel{
    var chave: String?
    var valor: String?
}


