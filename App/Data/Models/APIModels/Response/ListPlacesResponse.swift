//
//  ListPlacesResponse.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 25/05/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import Foundation

class ListPlacesResponse: CataoModel {
    var lista: [PlaceResponse]?
}
