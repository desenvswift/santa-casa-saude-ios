//
//  IrResponse.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 16/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import Foundation

class IrResponse: CataoModel{
    var impostoRenda: [IrItem]?
}

class IrItem: CataoModel{
    var id: Int?
    var descricao: String?
    var link: String?
}
