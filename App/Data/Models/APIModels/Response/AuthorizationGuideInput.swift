//
//  AuthorizationGuideInput.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 22/06/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import Foundation

class AuthorizationGuideInput: CataoModel {
    var comboCarterinha: [SubjectResponse]?
    var combotipoSolicitacao: [SubjectResponse]?
}
