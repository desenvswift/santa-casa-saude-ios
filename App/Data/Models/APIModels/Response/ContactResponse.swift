//
//  ContactResponse.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 04/07/20.
//  Copyright © 2020 Mobile2You. All rights reserved.
//

import Foundation

class ContactResponse: CataoModel {
    var lista: [ContactList]?
}

class ContactList: CataoModel {
    var nome: String?
    var subgrupo: [SubGroup]?
}

class SubGroup: CataoModel {
    var cod: String?
    var titulo: String?
    var valor: String?
}
