//
//  GetBoletosResponse.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 28/06/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import Foundation

class GetBoletosResponse: CataoModel {
    var boletos: [BoletoModel]?
}

class BoletoModel: CataoModel {
    var id: String?
    var mes: String?
    var descricao: String?
    var valor: String?
    var dataVencimento: String?
    var linhaDigitavel: String?
    var linkBoleto: String?
    var estaPago: NSNumber?
    var linkCoparticipacao: String?
    var mensagemBeneficiario: String?
    var existeBoleto: NSNumber?
    var existeCoparticipacao: NSNumber?
    var competencia: String?
    var status: String?
    
}

class GetBoletoFiltrosResponse: CataoModel {
    var lista: [BoletoFiltroModel]?
}

class BoletoFiltroModel: CataoModel{
    var chave: String?
    var valor: Int?
}
