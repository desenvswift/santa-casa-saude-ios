//
//  CoparticipacaoResponse.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 16/05/23.
//  Copyright © 2023 Mobile2You. All rights reserved.
//

import Foundation

class CopartResponse: CataoModel {
    var lista: [CopartList]?
}

class CopartList: CataoModel {
    var nome: String?
    var data: String?
    var procedimento: [CopartProcedimentos]?
}

class CopartProcedimentos: CataoModel {
    var procedimento: String?
    var codigo: String?
    var valor: String?
}
