//
//  ListCodDescTypeModel.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 20/05/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import Foundation

class ListCodDescTypeModel: CataoModel {
    var lista: [CodDescTypeModel]?
}

class CodDescTypeModel: CataoModel {
    var codigo: String?
    var descricao: String?
    var tipo: String?
}
