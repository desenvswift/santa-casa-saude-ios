//
//  File.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 25/06/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import Foundation

class AuthorizationGuideDetailResponse: CataoModel {
    var resposta: String?
    var nomeBeneficiario: String?
    var mensagemBeneficiario: String?
    var detalhe: [AuthDetailResponse]?
}

class AuthDetailResponse: CataoModel {
    var idDetalhe: String?
    var titulo: String?
    var datahora: String?
    var numeroProtocolo: String?
    var status: String?
    var statusCorTexto: String?
    var statusCodigo: String?
    var autorizado: String?
    var autorizadoLink: String?
}
