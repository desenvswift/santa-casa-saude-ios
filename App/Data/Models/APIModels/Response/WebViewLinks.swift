//
//  WebViewLinks.swift
//  SantaCasaSaude
//
//  Created by Luan Rocha Damato on 06/02/24.
//  Copyright © 2024 Mobile2You. All rights reserved.
//

import Foundation

class WebviewLinkRequest: CataoModel{
    var WebCod: String?
}

class WebViewLinkResponse: CataoModel{
    var lista: [WebViewLinkItem]?
}

class WebViewLinkItem: CataoModel{
    var WebvId: String?
    var WebvDescr: String?
    var WebvLink: String?
}

enum WebViewCodes: String{
    case ConsultarProcedimentos = "CdWep01"
    case TermoCadastro = "CdWep02"
    case TermoPerfil = "CdWep03"
    case PerfilLinkOuvidoria = "CdWep04"
    case PerfilLinkANS = "CdWep05"
    case PerfilMeajude = "CdWep06"
    case MnAgendamento = "CdWep07"
    case MnTelemedicinaAgenda = "CdWep08"
    case MnTelemedicinaPAdigi = "CdWep09"
    case MnTelemedicinaTutorial = "CdWep10"
    case MnFaleconosco = "CdWep11"
}
