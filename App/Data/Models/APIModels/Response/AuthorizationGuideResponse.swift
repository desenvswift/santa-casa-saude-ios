//
//  AuthorizationGuideResponse.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 22/06/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import Foundation

class AuthorizationGuideResponse: CataoModel {
    var lista: [AuthorizationGuide]?
}

class AuthorizationGuide: CataoModel {
    var id: String?
    var titulo: String?
    var nomeBeneficiario: String?
    var datahora: String?
    var numeroProtocolo: String?
    var status: String?
    var previsao: String?
    
    var statusCodigo: String?
    var statusCorTexto: String?
    var statusCor: String?
}
class AuthorizationGuideStatusListResponse : CataoModel {
    var lista: [AuthorizationGuideStatusResponse]?
}

class AuthorizationGuideStatusResponse : CataoModel {
    var NR_PROTOCOLO: String?
    var NR_TRANSACAO: String?
    var NR_GUIA: String?
    var STATUS_GUIA: String?
    var PRAZO: String?
    var DT_EMISSAO: String?
    var DT_VENCIMENTO: String?
    var CD_PRESTADOR_EXECUTOR: String?
    var CD_MATRICULA: String?
    var NM_PRESTADOR: String?
    var CD_PROCEDIMENTO: String?
    var DS_PROCEDIMENTO: String?
    var CD_TIPO_ATENDIMENTO: String?
    var DS_TIPO_ATENDIMENTO: String?
    var qtdGuiasFilhas: String?
    var statusCor: String?
    var GUIA_PAI: String?
    var QTD_TR: String?
    var PRAZO_ATRASO: String?
    var MSG_ATRASO: String?
    var TELEFONE_CONTATO: String?
    var WHATS_CONTATO: String?
    var LINK_CONTATO: String?
    var filhos: [AuthorizationGuideStatusResponse]?
    var guiasPai: [AuthorizationGuideStatusResponse]?
    var isVisible = true
}
