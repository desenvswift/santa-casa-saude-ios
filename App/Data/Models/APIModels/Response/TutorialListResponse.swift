//
//  TutorialResponse.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 19/05/19.
//  Copyright © 2019 Catao. All rights reserved.
//

import Foundation

class TutorialListResponse: CataoModel {
    var lista: [TutorialResponse]?
}

class TutorialResponse: CataoModel {
    var id, imagem, texto: String?
}

class BeneficiosListResponse: CataoModel {
    var lista: [BeneficiosResponse]?
}

class BeneficiosResponse: CataoModel {
    var id, titulo, link, imagemBase64: String?
}
