//
//  HomeResponse.swift
//  SantaCasaSaude
//
//  Created by Victor Catão on 29/06/19.
//  Copyright © 2019 Mobile2You. All rights reserved.
//

import Foundation

class HomeResponse: CataoModel {
    var painelBoleto: [PainelBoleto]?
    var painelGuiasAprovacao: [PainelBoleto]?
    var painelNotificacoes: [PainelBoleto]?
    
    static var notificacoes: [PainelBoleto]?
    
    static func verifyNotificationFor(location: NotificationName) -> PainelBoleto? {
        return notificacoes?.filter{ $0.titulo == location.rawValue}.first
    }
    
    static func markAsRead(notification: PainelBoleto){
        if let index = notificacoes?.index(of: notification){
            notificacoes?.remove(at: index)
        }
    }
}

class PainelBoleto: CataoModel {
    var idHome: String?
    var idPush: String?
    var idGuia: String?
    var idBoleto: String?
    var titulo: String?
    var corpo: String?
    var link: String?
    var imagem: String?
}

enum NotificationName: String{
    case home = "notifHome"
    case carteirinha = "notifCarteirinha"
    case financeiro = "notifFinanceiro"
    case guia = "notifGuiamedico"
    case agendamento = "notifAgendamento"
    case guias = "notifAutGuias"
    case telemedicina = "notifTelemedicina"
}
extension NotificationName: CaseIterable {}
