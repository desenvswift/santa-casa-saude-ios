//
//  SearchModel.swift
//  Base
//
//  Created by Victor Catão on 05/05/19.
//  Copyright © 2019 Catao. All rights reserved.
//

import Foundation

struct SearchModel {
    var picture: String?
    var text: String
    var isSelected: Bool
}
