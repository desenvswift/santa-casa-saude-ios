//
//  MAPPManager.swift
//  Catao
//
//  Created by Victor Catão on 30/12/18.
//  Copyright © 2018 Catao. All rights reserved.
//

import Foundation

class SCSManager: APIManager {
    
    static func signIn(request: LoginRequest, success: ((SignInResponse) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: POST_LOGIN, params: (request.toDictionary() as? [String : Any]), success: { (response) in
            let user = SignInResponse(json: response)
            UserDefaults.standard.set(user.token, forKey: KEY_USER_TOKEN)
            UserDefaults.standard.set(response, forKey: KEY_USER_INFOS)
            UserSingleton.shared.loadUser()
            
            success?(user)
            
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func signUp(request: SignUpRequest, success: ((UserResponse) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: POST_SIGNUP, params: (request.toDictionary() as? [String : Any]), success: { (response) in
            let user = UserResponse(json: response)
            success?(user)
            
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func signUpSAC(request: SignUpSACRequest, success: ((CataoModel) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: POST_SIGNUP_SAC, params: (request.toDictionary() as? [String : Any]), success: { (response) in
            success?(CataoModel(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func getUser(success: ((UserResponseBack) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: GET_USER, params: (CataoModel().toDictionary() as? [String : Any]), success: { (response) in
            let user = UserResponseBack(json: response)
            UserSingleton.shared.user?.beneficiario?.nome = user.nome
            UserSingleton.shared.user?.beneficiario?.cpfBeneficiario = user.cpf_beneficiario
            UserSingleton.shared.user?.beneficiario?.dtaNasc = user.dtaNasc
            UserSingleton.shared.user?.beneficiario?.ddd = user.ddd
            UserSingleton.shared.user?.beneficiario?.celular = user.celular
            UserSingleton.shared.user?.beneficiario?.email = user.email
            UserSingleton.shared.user?.beneficiario?.codigoPlano = user.codigoPlano
            UserSingleton.shared.user?.beneficiario?.descricaoPlano = user.descricaoPlano
            UserSingleton.shared.user?.beneficiario?.cpfTitular = user.cpf_titular
            UserSingleton.shared.user?.beneficiario?.matTitular = user.mat_titular
            UserSingleton.shared.user?.beneficiario?.matBeneficiario = user.mat_beneficiario
            success?(user)
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func getPlans(success: ((PlansResponse) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: GET_PLANS, params: (CataoModel().toDictionary() as? [String : Any]), success: { (response) in
            success?(PlansResponse(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func updateUserPlan(request: UpdateUserPlanRequest, success: ((CataoModel) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: UPDATE_USER_PLANS, params: (request.toDictionary() as? [String : Any]), success: { (response) in
            UserDefaults.standard.set(response, forKey: KEY_USER_INFOS)
            UserSingleton.shared.loadUser()
            success?(CataoModel(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func recoverPassword(request: RecoverPasswordRequest, success: ((CataoModel) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: RECOVER_PASSWORD, params: (request.toDictionary() as? [String : Any]), success: { (response) in
            success?(CataoModel(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    
    static func getTutorial(success: ((TutorialListResponse) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: GET_TUTORIAL, params: nil, success: { (response) in
            success?(TutorialListResponse(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func getBeneficios(success: ((BeneficiosListResponse) -> Void)?, error: ((String, Int) -> Void)? ){
        getToAPIService(service: GET_BENEFICIOS, params: nil, success: { (response) in
            success?(BeneficiosListResponse(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    
    
    static func getMedicalGuideFilter(request: MedicalGuideRequest, success: ((ListCodDescTypeModel) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: GET_MEDICAL_GUIDE_FILTER, params: (request.toDictionary() as? [String : Any]), success: { (response) in
            success?(ListCodDescTypeModel(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func getMedicalGuideQualifications(success: ((GetFilterAdvancedResponse) -> Void)?, error: ((String, Int) -> Void)? ){
        let p: [String: Any] = ["filtro" : "QL"]
        postToAPIService(service: GET_MEDICAL_GUIDE_FILTER_ADVANCED, params: p, success: { (response) in
            success?(GetFilterAdvancedResponse(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    static func getMedicalGuideBairro(request: MedicalGuideRequest, success: (([KeyValueModel]) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: GET_MEDICAL_GUIDE_FILTER_ADVANCED, params: (request.toDictionary() as? [String : Any]), success: { (response) in
            success?(GetFilterAdvancedResponse(json: response).bairro ?? [])
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    static func getMedicalGuideTipoEstabeliecimento(request: MedicalGuideRequest, success: (([KeyValueModel]) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: GET_MEDICAL_GUIDE_FILTER_ADVANCED, params: (request.toDictionary() as? [String : Any]), success: { (response) in
            success?(GetFilterAdvancedResponse(json: response).tipoEstabelecimento ?? [])
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func getMedicalGuide(request: MedicalGuideRequest, success: (([PlaceResponse]) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: GET_MEDICAL_GUIDE, params: (request.toDictionary() as? [String : Any]), success: { (response) in
            success?(ListPlacesResponse(json: response).lista ?? [])
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    
    static func favorite(request: FavoriteRequest, success: ((CataoModel) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: POST_FAVORITE, params: (request.toDictionary() as? [String : Any]), success: { (response) in
            success?(CataoModel(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func getPlanosAtendidos(guiaMedCod: String, success: ((GetPlansResponse) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: GET_PLANS_ATENDIDOS, params: (["guiaMedCod": guiaMedCod]), success: { (response) in
            success?(GetPlansResponse(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func sendMessage(request: TalkWithUsRequest, success: (() -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: POST_TALK_WITH_US, params: (request.toDictionary() as? [String : Any]), success: { (response) in
            success?()
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    
    static func getSubsjects(success: ((SubjectsListResponse) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: POST_TALK_WITH_US_RESOURCES, params: nil, success: { (response) in
            success?(SubjectsListResponse(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func getFavorites(success: (([PlaceResponse]) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: GET_FAVORITES, params: nil, success: { (response) in
            success?(ListPlacesResponse(json: response).lista ?? [])
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func getHome(success: ((HomeResponse) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: GET_HOME, params: nil, success: { (response) in
            success?(HomeResponse(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    
    static func verifyAccess(success: (() -> Void)?, error: ((String, Int) -> Void)? ){
        getToAPIService(service: VERIFY_ACCESS, params: nil, success: { (response) in
            success?()
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    // MARK: - Profile
    static func updateMail(email: String, success: (() -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: UPDATE_MAIL, params: ["email": email], success: { (response) in
            success?()
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func updatePhone(ddd: String, phone: String, success: (() -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: UPDATE_PHONE, params: ["ddd": ddd, "celular": phone], success: { (response) in
            success?()
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func updatePassword(password: String, success: (() -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: UPDATE_PASSWORD, params: ["senha": password], success: { (response) in
            success?()
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }

    
    // MARK: - Autorizacao de Guia
    static func getAuthorizationGuide(success: (([AuthorizationGuide]) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: GET_AUTHORIZATION_GUIDES, params: nil, success: { (response) in
            success?(AuthorizationGuideResponse(json: response).lista ?? [])
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    static func getAuthorizationGuideHistory(filtro: String, periodo: String, success: (([AuthorizationGuideStatusResponse]) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(baseURL: API_URL_V3, service: GET_AUTHORIZATION_HISTORY, params: ["transacao": filtro, "periodo": periodo], success: { (response) in
            success?(AuthorizationGuideStatusListResponse(json: response).lista ?? [])
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func getAuthorizationGuideInput(success: ((AuthorizationGuideInput) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: GET_AUTHORIZATION_GUIDE_INSUMO, params: nil, success: { (response) in
            success?(AuthorizationGuideInput(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func createAuthorizationGuide(request:CreateAuthorizationGuideRequest, success: (() -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: CREATE_AUTHORIZATION_GUIDE, params: (request.toDictionary() as? [String : Any]), success: { (response) in
            success?()
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }

    
    static func getAuthorizationGuideDetail(id: String, success: ((AuthorizationGuideDetailResponse) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: GET_AUTHORIZATION_GUIDE_DETAIL, params: ["id": id], success: { (response) in
            success?(AuthorizationGuideDetailResponse(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    

    static func getboletos(ano: String, success: (([BoletoModel]) -> Void)?, error: ((String, Int) -> Void)? ){
        let params: [String: Any] = ["servico": "BOLETO", "filtroKey": "ano", "filtroValue": ano]
        postToAPIService(service: GET_FINANCEIRO, params: params, success: { (response) in
            success?(GetBoletosResponse(json: response).boletos ?? [])
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    
    
    static func getBoletosFiltros(success: (([BoletoFiltroModel]) -> Void)?, error: ((String, Int) -> Void)? ){
        let params: [String: Any] = ["servico": "BOLETO", "filtroKey": "ano"]
        postToAPIService(service: GET_FINANCEIRO_INSUMO, params: params, success: { (response) in
            success?(GetBoletoFiltrosResponse(json: response).lista ?? [])
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func clickHome(idHome: String, success: (() -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: CLICK_HOME, params: ["idHome": idHome], success: { (response) in
            success?()
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func getListaIR(success: (([IrItem]) -> Void)?, error: ((String, Int) -> Void)? ){
        let params: [String: Any] = ["servico": "IRPF"]
        postToAPIService(service: GET_FINANCEIRO, params: params, success: { (response) in
            success?(IrResponse(json: response).impostoRenda ?? [])
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    
    static func getLinks(success: ((LinksResponse) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: GET_LINKS, params: nil, success: { (response) in
            success?(LinksResponse(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }

    
    static func getCarteirinhas(success: ((HealthInsuranceCardResponse) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: GET_CARTEIRINHAS, params: nil, success: { (response) in
            UserDefaults.standard.set(response, forKey: KEY_USER_CARTEIRINHAS)
            success?(HealthInsuranceCardResponse(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }

    static func getContact(success: ((ContactResponse) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: GET_CONTACT, params: nil, success: { (response) in
            success?(ContactResponse(json: response))
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    static func deleteUser(success: (() -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: DELETE_USER, params: nil, success: { (response) in
            success?()
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
    
    static func getLink(location: String, success: ((String?) -> Void)?, error: ((String, Int) -> Void)? ){
        postToAPIService(service: WEB_LINKS, params: ["WebCod": location], success: { (response) in
            success?(WebViewLinkResponse(json: response).lista?.first?.WebvLink)
        }) { (err, statusCode) in
            let erro = ErrorResponse(json: err)
            error?(erro.msgExterna ?? "Verifique a sua conexão", erro.codAcao?.intValue ?? -1)
        }
    }
    
}

